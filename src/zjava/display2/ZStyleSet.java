package zjava.display2;

import java.util.Iterator;

import zjava.ZObject;
import zjava.lang.types.collections.ZList;

public class ZStyleSet extends ZObject implements Iterable<ZRestyle<?>>
{
	private final ZList<ZRestyle<?>>
		styles;
	
	public ZStyleSet(ZStyleSet... sets)
	{
		this.styles = new ZList<>(sets);
	}
	public ZStyleSet(ZRestyle<?>... styles)
	{
		this.styles = new ZList<>(styles);
	}
	public int getStyleAmount()
	{
		return this.styles.getSize();
	}
	@Override
	public Iterator<ZRestyle<?>> iterator()
	{
		return styles.iterator();
	}
	
}
