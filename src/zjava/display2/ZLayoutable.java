package zjava.display2;

public interface ZLayoutable 
{
	public void style(ZState state, ZStyleSet... styles);
	
	public default void style(ZStyleSet... styles)
	{
		this.style(ZState.NONE, styles);
	}
	public default void hover(ZStyleSet... styles)
	{
		this.style(ZState.HOVER, styles);
	}
	public default void click(ZStyleSet... styles)
	{
		this.style(ZState.CLICK, styles);
	}
	public default void select(ZStyleSet... styles)
	{
		this.style(ZState.SELECT, styles);
	}
	public void reset(ZStyle<?> style);
	public default void reset(ZStyle<?>[] styles)
	{
		for(ZStyle<?> style : styles)
		{
			this.reset(style);
		}
	}
}
