package zjava.display2;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.font.FontRenderContext;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.Map;

import zjava.ZObject;
import zjava.auto.annotations.Attribute;
import zjava.display2.color.ZColor;
import zjava.lang.tools.Copyable;
import zjava.lang.utils.ZString;

public class ZFont extends ZObject implements Copyable<ZFont>
{	
	public final static class Default
	{
		private Default() {}
		
		public final static String
			FAMILY = "Sans-Serif",
			NAME = "Arial";
	
		public final static int
			SIZE = 12;
		
		public final static boolean
			ITALIC = false,
			STRIKETHROUGH = false;
		
		public final static ZUnderline
			UNDERLINE = ZUnderline.NONE;
		
		public final static Float
			WEIGHT = 1F;
		
		public final static int
			SUPERSCRIPT = 0;
		
		public final static double
			TRACKING = 0;
		
		public final static ZColor
			BG_COLOR = ZColor.INVISIBLE;
		
		public final static boolean
			KERNING = false;
		
		public final static boolean
			COLOR_SWAPPING = false;
		
		public final static int
			BIDI_EMBEDDING = 0;
	}
	
	public static enum ZUnderline
	{
		NONE, FULL_1, FULL_2, DOT, DASH;
	}
	
	@Attribute
	private final String
		family,
		name;
	
	@Attribute
	private final int 
		size;
	
	@Attribute
	private final boolean
		isItalic,
		strikethrough;
	
	@Attribute
	private final ZUnderline
		underline;
	
	@Attribute
	private final float
		weight;
	
	@Attribute
	private final int
		superScript;
	
	@Attribute
	private final double
		tracking;
	
	@Attribute
	private final ZColor
		bgColor;
	
	@Attribute
	private final boolean
		kerning;
	
	@Attribute
	private final boolean
		isColorSwapping;
	
	@Attribute
	private final int
		bidiEmbedding;
	
	public ZFont()
	{
		this(ZFont.Default.NAME, ZFont.Default.SIZE);
	}
	public ZFont(java.awt.Font font)
	{
		this.family = font.getFamily();
		this.name = font.getName();
		this.size = font.getSize();
		if (font.getStyle() == java.awt.Font.ITALIC)
		{
			this.isItalic = true;
		}
		else if (font.getStyle() == java.awt.Font.BOLD + java.awt.Font.ITALIC)
		{
			this.isItalic = true;
		}
		else
		{
			this.isItalic = ZFont.Default.ITALIC;
		}
		Map<TextAttribute, ?> map = font.getAttributes();
		if(map.containsKey(TextAttribute.UNDERLINE))
		{
			int underline = (int)map.get(TextAttribute.UNDERLINE);
			if(0 == underline)
			{
				this.underline = ZUnderline.NONE;
			}
			else if(TextAttribute.UNDERLINE_LOW_ONE_PIXEL == underline)
			{
				this.underline = ZUnderline.FULL_1;
			}
			else if(TextAttribute.UNDERLINE_LOW_TWO_PIXEL == underline)
			{
				this.underline = ZUnderline.FULL_2;
			}
			else if(TextAttribute.UNDERLINE_LOW_DASHED == underline)
			{
				this.underline = ZUnderline.DASH;
			}
			else if(TextAttribute.UNDERLINE_LOW_DOTTED == underline)
			{
				this.underline = ZUnderline.DOT;
			}
			else
			{
				this.underline = ZFont.Default.UNDERLINE;
			}
		}
		else
		{
			this.underline = ZFont.Default.UNDERLINE;
		}
		this.weight = this.readAttribute(map, TextAttribute.WEIGHT, ZFont.Default.WEIGHT);
		this.strikethrough = this.readAttribute(map, TextAttribute.STRIKETHROUGH, ZFont.Default.STRIKETHROUGH);
		this.superScript = this.readAttribute(map, TextAttribute.SUPERSCRIPT, ZFont.Default.SUPERSCRIPT);
		this.tracking = this.readAttribute(map, TextAttribute.TRACKING, ZFont.Default.TRACKING);
		Paint p = (Paint) map.get(TextAttribute.BACKGROUND);
		if(p instanceof Color)
		{
			this.bgColor = new ZColor((Color) p);
		}
		else
		{
			this.bgColor = ZFont.Default.BG_COLOR;
		}
		this.kerning = this.readAttribute(map, TextAttribute.KERNING, ZFont.Default.KERNING);
		this.isColorSwapping = this.readAttribute(map, TextAttribute.SWAP_COLORS, ZFont.Default.COLOR_SWAPPING);
		this.bidiEmbedding = this.readAttribute(map, TextAttribute.BIDI_EMBEDDING, ZFont.Default.BIDI_EMBEDDING);
	}
	public ZFont(ZFont font)
	{
		this(font.getFamily(), font.getName(), font.getSize(), font.isItalic(), font.isStrikethrough(), font.getUnderline(), 
				font.getWeight(), font.getSuperScript(), font.getTracking(), font.getBGColor(), font.isKerning(), font.isColorSwapping, font.getBidiEmbedding());
	}
	public ZFont(String name, int size)
	{
		this(new Font(name, Font.PLAIN, size).getFamily(), new Font(name, Font.PLAIN, size).getName(), size, ZFont.Default.ITALIC, ZFont.Default.STRIKETHROUGH, 
				ZFont.Default.UNDERLINE, ZFont.Default.WEIGHT, ZFont.Default.SUPERSCRIPT, ZFont.Default.TRACKING, ZFont.Default.BG_COLOR, 
				ZFont.Default.KERNING, ZFont.Default.COLOR_SWAPPING, ZFont.Default.BIDI_EMBEDDING);
	}
	public ZFont(String family, String name, int size, boolean isItalic, boolean strikethrough,
			ZUnderline underline, float weight, int superScript, double tracking, ZColor bg_color, boolean kerning,
			boolean isColorSwapping, int bidiEmbedding)
	{
		this.family = family;
		this.name = name.charAt(0) + "".toUpperCase() + ZString.removeStart(name.toLowerCase(), 1);;
		this.size = size;
		this.isItalic = isItalic;
		this.strikethrough = strikethrough;
		this.underline = underline;
		this.weight = weight;
		this.superScript = superScript;
		this.tracking = tracking;
		this.bgColor = bg_color;
		this.kerning = kerning;
		this.isColorSwapping = isColorSwapping;
		this.bidiEmbedding = bidiEmbedding;
	}
	@SuppressWarnings("unchecked") 
	private <P> P readAttribute(Map<TextAttribute, ?> map, TextAttribute attribute, P def)
	{
		Object value = map.get(attribute);
		return null == value ? def : ((Class<P>)def.getClass()).cast(value);
	}
	
	public int getHeight()
	{
		return new Canvas().getFontMetrics(this.toAWTFont()).getDescent() + new Canvas().getFontMetrics(this.toAWTFont()).getAscent();
	}
	public int getWidth(String text)
	{
		return (int) this.toAWTFont().getStringBounds(text, new FontRenderContext(this.toAWTFont().getTransform(), false, false)).getBounds().getWidth();
	}
	
	public String getFamily()
	{
		return family;
	}
	public String getName()
	{
		return name;
	}
	public int getSize()
	{
		return size;
	}
	public boolean isItalic()
	{
		return isItalic;
	}
	public boolean isStrikethrough()
	{
		return strikethrough;
	}
	public ZUnderline getUnderline()
	{
		return underline;
	}
	public float getWeight()
	{
		return weight;
	}
	public int getSuperScript()
	{
		return superScript;
	}
	public double getTracking()
	{
		return tracking;
	}
	public ZColor getBGColor()
	{
		return bgColor;
	}
	public boolean isKerning()
	{
		return kerning;
	}
	public boolean isColorSwapping()
	{
		return isColorSwapping;
	}
	public int getBidiEmbedding()
	{
		return bidiEmbedding;
	}
	
	public java.awt.Font toAWTFont()
	{
		Map<TextAttribute, Object> att = new HashMap<TextAttribute, Object>();
		
		att.put(TextAttribute.BACKGROUND, this.getBGColor().toAWTColor());
		att.put(TextAttribute.BIDI_EMBEDDING, this.getBidiEmbedding());
		att.put(TextAttribute.FAMILY, this.getFamily());
		att.put(TextAttribute.KERNING, this.isKerning());
		att.put(TextAttribute.SIZE, this.getSize());
		att.put(TextAttribute.STRIKETHROUGH, this.isStrikethrough());
		att.put(TextAttribute.SUPERSCRIPT, this.getSuperScript());
		att.put(TextAttribute.SWAP_COLORS, this.isColorSwapping());
		att.put(TextAttribute.TRACKING, this.getTracking());
		
		int underline = -1;
		switch(this.getUnderline())
		{
			case DASH:
			{
				underline = TextAttribute.UNDERLINE_LOW_DASHED;
				break;
			}
			case DOT:
			{
				underline = TextAttribute.UNDERLINE_LOW_DOTTED;
				break;
			}
			case FULL_1:
			{
				underline = TextAttribute.UNDERLINE_LOW_ONE_PIXEL;
				break;
			}
			case FULL_2:
			{
				underline = TextAttribute.UNDERLINE_LOW_TWO_PIXEL;
				break;
			}
			case NONE:
			{
				underline = -1;
				break;
			}
		}
		att.put(TextAttribute.UNDERLINE, underline);
		att.put(TextAttribute.WEIGHT, this.getWeight());
		
		return new java.awt.Font(this.name, Font.PLAIN, this.size).deriveFont(att);
	}
	
	@Override
	public ZFont copy()
	{
		return new ZFont(this);
	}
}
