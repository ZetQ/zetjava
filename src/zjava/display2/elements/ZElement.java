package zjava.display2.elements;

import zjava.auto.annotations.Attribute;
import zjava.display2.ZContainer;
import zjava.display2.ZDrawer;
import zjava.display2.ZLayout;
import zjava.display2.ZLayoutable;
import zjava.display2.ZState;
import zjava.display2.ZStyle;
import zjava.display2.ZStyleSet;
import zjava.display2.shape.ZShape;
import zjava.lang.types.ZPoint;
import zjava.lang.types.ZBounds;
import zjava.lang.types.collections.ZList;
import zjava.lang.types.dec.BoundsGetter;

public abstract class ZElement extends ZContainer implements ZLayoutable, BoundsGetter
{
	private ZLayout
		layout = new ZLayout();
	
	private final ZList<ZLayout>
		layouts = new ZList<>();
	
	@Attribute
	private ZBounds
		bounds = new ZBounds();
	
	private ZShape
		shape = null;
	
	
	private final ZState[]
		states = new ZState[] { ZState.CLICK, ZState.HOVER, ZState.NONE };
	
	private boolean
		isHovered,
		isClicked;
	
	public ZElement() {}
	public ZElement(ZStyleSet... styles)
	{
		for(ZStyleSet set : styles)
		{
			this.style(set);
		}
	}
	public ZElement(ZLayout... layouts)
	{
		this.layouts.add(layouts);
	}
	
//	>> PRIVATE SECTION >>
	private <P> P getStyleState(ZState state, ZStyle<P> style)
	{
	/*
		Layout-Priority:
			1. own layout
			2. last added layout
			3. ...
			4. first added layout
	*/
		P value = this.layout.get(style, state);
		if(null == value)
		{
			for(int i = this.layouts.getSize() - 1; i >= 0; --i)
			{
				P inVal = this.layouts.get(i).get(style, state);
				if(null != inVal)
				{
					break;
				}
			}
		}
		return value;
	}
//	<< PRIVATE SECTION <<
	
//	>> PROTECTED SECTION >>
	protected ZState[] getStates()
	{
		return states;
	}
	@Override
	protected void drawInit(ZDrawer canvas, BoundsGetter bounds)
	{	
		this.bounds.setBounds(bounds);
		this.shape = canvas.getShape();
		this.draw(canvas);
		super.drawInit(canvas, bounds);
	}
	protected abstract void draw(ZDrawer canvas);
//	<< PROTECTED SECTION <<
	
//	>> LAYOUT >>
	public <P> P getStyle(ZStyle<P> style)
	{
		for(ZState state : this.getStates())
		{
			if(state.appliesTo(this))
			{
				P value = this.getStyleState(state, style);
				if(null != value) return value;
			}
		}
		return style.getDefault();
	}
	public void addLayout(ZLayout layout)
	{
		this.layouts.add(layout);
	}
	public void removeLayout(ZLayout layout)
	{
		this.layouts.remove(layout);
	}
//	<< LAYOUT <<
	
//	>> INFO >>
	public ZContainer getParent()
	{
		return super.getParent();
	}
	public ZPoint getActualLocation()
	{
		ZContainer containerParent = this.getParent();
		if(null == containerParent || !(containerParent instanceof ZElement))
		{
			return this.getLocation();
		}
		ZPoint location = ((ZElement) containerParent).getActualLocation();
		location.addLocation(this.getX(), this.getY());
		return location;
	}
	public ZShape getShape()
	{
		return this.shape.copy();
	}
//	<< INFO <<
	
//	>> STATE >>
	public boolean isHovered()
	{
		return this.isHovered;
	}
	public boolean isClicked()
	{
		return this.isClicked;
	}
//	<< STATE <<

//	>> IMPLEMENTATIONS >>
	@Override
	public void style(ZState state, ZStyleSet... styles)
	{
		this.layout.style(state, styles);
	}
	@Override
	public void reset(ZStyle<?> style)
	{
		this.layout.reset(style);
	}
	@Override
	public int getX()
	{
		return this.bounds.getX();
	}
	@Override
	public int getY()
	{
		return this.bounds.getY();
	}
	@Override
	public int getWidth()
	{
		return this.bounds.getWidth();
	}
	@Override
	public int getHeight()
	{
		return this.bounds.getHeight();
	}
//	<< IMPLEMENTATIONS <<
}
