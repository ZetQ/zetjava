package zjava.display2.elements;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JTextField;

import zjava.display2.ZContainer;
import zjava.display2.ZDrawer;
import zjava.display2.ZImage;
import zjava.display2.ZStyleSet;
import zjava.display2.attributes.Background;
import zjava.display2.attributes.Border;
import zjava.display2.attributes.Bounds;
import zjava.display2.attributes.Background.ImageAdapt;
import zjava.display2.shape.DynamicShape;
import zjava.display2.shape.ZShape;
import zjava.lang.types.ZBounds;
import zjava.lang.types.ZSize;

public class ZPanel extends ZElement
{
	private int
		border;
	
	private JTextField
		textContainer = new JTextField("");
	
	public ZPanel() {}
	public ZPanel(ZStyleSet... styles)
	{
		super(styles);
		this.textContainer.setBackground(new Color(0, 0, 0, 0));
		this.textContainer.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0, 0)));
	}
	
	@Override
	protected void setParent(ZContainer parent)
	{
		super.setParent(parent);
		if(null != parent)
		{
			this.addComponent(this, this.textContainer);
		}
	}
	
	@Override
	protected void draw(ZDrawer drawer)
	{
		ZDrawer rectDrawer = drawer.subDrawer(ZShape.createRect(this.getWidth(), this.getHeight()), 0, 0, 0);
		int border = this.getStyle(Border.size()).calc(this.getStyle(Border.relation()).calc(this.getWidth(), this.getHeight()));
		this.border = border < 1 ? 1 : border;
		this.drawBackground(rectDrawer);
		this.drawBorder(drawer);
	}
	protected void drawBackground(ZDrawer drawer)
	{
		drawer.fillShape(this.getStyle(Bounds.shape()).create(this.getWidth() - this.border, this.getHeight() - this.border), this.border / 2, this.border / 2, this.getStyle(Background.color()));
		
		ZImage[] images = this.getStyle(Background.image());
		ImageAdapt v = this.getStyle(Background.verAdapt());
		ImageAdapt h = this.getStyle(Background.horAdapt());
		
//		TODO image drawing
		for(ZImage image : images)
		{
			ZBounds bounds = new ZBounds();
			if(v == ImageAdapt.STRETCH && h == ImageAdapt.STRETCH)
			{
				bounds.setBounds(0, 0, this.getWidth(), this.getHeight());
			}
			else if(v == ImageAdapt.POSITION && h == ImageAdapt.POSITION)
			{
				if(image.getWidth() > image.getHeight())
				{
					double ratio = (double) image.getHeight() / image.getWidth();
					bounds.setWidth(this.getWidth());
					bounds.setHeight((int)((double)bounds.getWidth()  * ratio));
				}
				else
				{	
					double ratio = (double) image.getWidth() / image.getHeight();
					bounds.setHeight(this.getHeight());
					bounds.setWidth((int)((double)bounds.getHeight()  * ratio));
				}
			}
			drawer.drawImage(image, bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
		}
	}
	protected void drawInset(ZDrawer drawer, ZSize size) {}
	protected void drawBorder(ZDrawer drawer)
	{
		DynamicShape shape = this.getStyle(Bounds.shape());
		ZShape outline = shape.create(this.getWidth() - this.border, this.getHeight() - this.border);
		double offset = (double)this.border / 2;
		drawer.drawShape(this.getStyle(Bounds.shape()).create(this.getWidth() - this.border, this.getHeight() - this.border), 
				offset, offset, this.getStyle(Background.color()), 
				this.getStyle(Border.size()).calc(this.getStyle(Border.relation()).calc(this.getWidth(), this.getHeight())));
		drawer.drawShape(outline, offset, offset, this.getStyle(Border.color()), 
				this.getStyle(Border.size()).calc(this.getStyle(Border.relation()).calc(this.getWidth(), this.getHeight())));
	
		this.textContainer.setBounds(this.getBounds().toAWTRectangle());
	}
	
}
