package zjava.display2.color;

import java.awt.Paint;

import zjava.ZObject;
import zjava.auto.annotations.Attribute;
import zjava.errors.ArgumentException;
import zjava.lang.tools.Copyable;

public class ZColor extends ZObject implements Copyable<ZColor>, ZPaint
{
	public static final ZColor
		BLACK = new ZColor(0, 0, 0),
		NAVY = new ZColor(0, 0, 128),
		DARKBLUE = new ZColor(0, 0, 139),
		MEDIUMBLUE = new ZColor(0, 0, 205),
		BLUE = new ZColor(0, 0, 255),
		DARKGREEN = new ZColor(0, 100, 0),
		GREEN = new ZColor(0, 128, 0),
		//http://www.w3schools.com/cssref/css_colorsfull.asp
		INVISIBLE = new ZColor(0, 0, 0, 0),
		RED = new ZColor(255, 0, 0),
		WHITE = new ZColor(255, 255, 255),

		LIGHTGRAY = new ZColor(211, 211, 211);
	
	public static boolean isColor(String code)
	{
		return ZColor.isHex(code) || ZColor.isRGBA(code);
	}
	public static boolean isRGBA(String code)
	{
		return code.trim().matches("(\\d?\\d?\\d)\\s*,\\s*(\\d?\\d?\\d)\\s*,\\s*(\\d?\\d?\\d)(\\s*,\\s*(\\d?\\d?\\d))?");
	}
	public static boolean isHex(String code)
	{
		return code.trim().matches( "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$");
	} 
	
	@Attribute 
	private int
		red;
	
	@Attribute
	private int
		green;
		
	@Attribute
	private int
		blue;
	
	@Attribute
	private int
		alpha;
	
	public ZColor()
	{
		this(0, 0, 0, 255);
	}
	public ZColor(int code)
	{
		this.red = (code)&0xFF;
		this.green = (code>>8)&0xFF;
		this.blue = (code>>16)&0xFF;
		this.alpha = (code>>24)&0xFF;
	}
	public ZColor(String color)
	{
		color = color.trim();
		if(ZColor.isRGBA(color))
		{
			String[] rgba = color.split(",");
			this.red = Integer.valueOf(rgba[0].trim());
			this.green = Integer.valueOf(rgba[1].trim());
			this.blue = Integer.valueOf(rgba[2].trim());
			this.alpha = rgba.length == 4 ? Integer.valueOf(rgba[3].trim()) : 255;
		}
		else if(ZColor.isHex(color))
		{
			java.awt.Color c = java.awt.Color.decode(color);
			this.red = c.getRed();
			this.green = c.getGreen();
			this.blue = c.getBlue();
			this.alpha = c.getAlpha();
		}
		else
		{
			throw new ArgumentException("the string \"" + color + "\" cannot be converted into a valid color", ZColor.class, "isColor(String)");
		}
	}
	public ZColor(java.awt.Color color)
	{
		this.red = color.getRed();
		this.green = color.getGreen();
		this.blue = color.getBlue();
		this.alpha = color.getAlpha();
	}
	public ZColor(ZColor color)
	{
		this.red = color.getRed();
		this.green = color.getGreen();
		this.blue = color.getBlue();
		this.alpha = color.getAlpha();
	}
	public ZColor(int red, int green, int blue)
	{
		this(red, green, blue, 255);
	}
	public ZColor(int red, int green, int blue, int alpha)
	{
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;
	}
	
	public int getRed()
	{
		return this.red;
	}
	public int getGreen()
	{
		return this.green;
	}
	public int getBlue()
	{
		return this.blue;
	}
	public int getAlpha()
	{
		return this.alpha;
	}
	
	public int toIntRGB()
	{
		return ((this.red & 0xFF) << 24) |
                ((this.green & 0xFF) << 16) |
                ((this.blue & 0xFF) << 8)  |
                ((this.alpha & 0xFF) << 0);
	}
	public java.awt.Color toAWTColor()
	{
		return new java.awt.Color(this.getRed(), this.getGreen(), this.getBlue(), this.getAlpha());
	}
	@Override
	public Paint toAWTPaint(int width, int height)
	{
		return this.toAWTColor();
	}
	public javafx.scene.paint.Color toFXColor()
	{
		return new javafx.scene.paint.Color(this.red, this.green, this.blue, this.alpha);
	}
	@Override
	public ZColor copy()
	{
		return new ZColor(this);
	}
}
