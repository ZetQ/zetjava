package zjava.display2.color;

import java.awt.GradientPaint;
import java.awt.Paint;

import zjava.ZObject;
import zjava.auto.annotations.Attribute;
import zjava.display2.Unit;
import zjava.display2.ZValue;
import zjava.lang.tools.Copyable;

public class ZGradient extends ZObject implements ZPaint
{
	@Attribute
	private final ZColor
		startColor,
		endColor;
	
	private final ZValue
		startX,
		startY,
		endX,
		endY;
	
	public ZGradient(String startX, String startY, String endX, String endY, ZColor startColor, ZColor endColor)
	{
		this.startColor = startColor;
		this.endColor = endColor;
		
		this.startX = Unit.decode(startX);
		this.startY = Unit.decode(startY);
		this.endX = Unit.decode(endX);
		this.endY = Unit.decode(endY);
	}
	
//	>> COLOR >>
	public ZColor getStartColor()
	{
		return this.startColor;
	}
	public ZColor getEndColor()
	{
		return this.endColor;
	}
//	<< COLOR <<
	
	@Override
	public Paint toAWTPaint(int width, int height)
	{
		return new GradientPaint(this.startX.calc(width), this.startY.calc(height), this.startColor.toAWTColor(), 
								 this.endX.calc(width), this.endY.calc(height), this.endColor.toAWTColor());
	}
}
