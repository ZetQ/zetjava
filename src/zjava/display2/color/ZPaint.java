package zjava.display2.color;

import java.awt.Paint;

public interface ZPaint
{
	Paint toAWTPaint(int width, int height);
}
