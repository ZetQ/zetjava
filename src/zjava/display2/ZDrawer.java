package zjava.display2;

import zjava.ZObject;
import zjava.display2.color.ZPaint;
import zjava.display2.shape.ZShape;
import zjava.lang.tools.Copyable;

public abstract class ZDrawer extends ZObject implements Copyable<ZDrawer>
{	
	public abstract void drawShape(ZShape shape, double x, double y, ZPaint color, float size);
	public abstract void fillShape(ZShape shape, int x, int y, ZPaint color);
	public abstract void drawString(String string, int x, int y, ZFont font, ZPaint color);
	public abstract void drawImage(ZImage image, int x, int y, int width, int height);
	
	public abstract ZDrawer subDrawer(ZShape shape, int x, int y, double degree);
	public abstract ZShape getShape();
	
}
