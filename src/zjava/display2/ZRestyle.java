package zjava.display2;

import zjava.ZObject;
import zjava.auto.annotations.Attribute;

public class ZRestyle<T> extends ZObject
{
	private final ZStyle<T>
		style;
	
	@Attribute
	private final T
		value;
	
	public ZRestyle(ZStyle<T> style, T value)
	{
		this.style = style;
		this.value = value;
	}
	public ZStyle<T> getStyle()
	{
		return this.style;
	}
	public T getValue()
	{
		return this.value;
	}
}
