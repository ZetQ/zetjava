package zjava.display2;

import zjava.ZObject;
import zjava.errors.ArgumentException;
import zjava.errors.UnexpectedException;
import zjava.lang.types.collections.ZMap;
import zjava.system.ZScreen;

public class Unit extends ZObject
{
	private static final ZMap<String, Unit>
		UNITS = new ZMap<>();
	
	public static final Unit
		PIXEL = define("px", (value, ref) -> (int) value),
		MILLIMETRE = define("mm", (value, ref) -> (int)((value * ZScreen.getResolution()) / 25.4)),
		CENTIMETRE = define("cm", (value, ref) -> (int)((value * ZScreen.getResolution()) / 2.54)),
		PERUN = define("i", (value, ref) -> (int) (value * ref)),
		PERDIX = define("x", (value, ref) -> (int)((float)value / 10 * ref)),
		PERCENT = define("%", (value, ref) -> (int)((float)value / 100 * ref)),
		PERMILLE = define("pm", (value, ref) -> (int)((float)value / 1000 * ref));
	
	public static Unit define(String sign, UnitCalc calc)
	{
		Unit unit = new Unit(notNull(sign), notNull(calc));
		UNITS.put(sign, unit);
		return unit;
	}
	public static ZValue decode(String string)
	{
		for(Unit unit : UNITS.itValues())
		{
			if(unit.matches(string))
			{
				String[] val = string.split(unit.getSign());
				if(val.length > 1)
				{
					throw new UnexpectedException("error while trying to cast value");
				}
				double value = Double.valueOf(val[0]);
				return ref -> unit.calc(value, ref);
			}
		}
		throw new ArgumentException("the string \"" + string + "\" cannot be casted to any defined unit");
	}
	
	private final String
		sign;
	
	private final UnitCalc
		calc;
	
	private Unit(String sign, UnitCalc calc)
	{
		this.sign = sign;
		this.calc = calc;
	}
	
//	>> MISC >>
	public String getSign()
	{
		return this.sign;
	}
	public boolean matches(String value)
	{
		return value.trim().toLowerCase().matches("-?\\d+(\\.\\d+)? ?" + this.sign) || value.trim().toLowerCase().matches("\\d." + this.sign);
	}
	public int calc(double value, int ref)
	{
		return this.calc.calc(value, ref);
	}
//	<< MISC <<
	
	@FunctionalInterface
	public interface UnitCalc
	{
		public int calc(double value, int ref);
	}
}
