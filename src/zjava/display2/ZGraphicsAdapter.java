package zjava.display2;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.AffineTransform;

import zjava.display2.color.ZPaint;
import zjava.display2.shape.ZShape;

public class ZGraphicsAdapter extends ZDrawer
{	
	private final Graphics2D
		source;
	
	private final ZShape
		shape;
	
//	>> CONSTRUCTORS >>
	public ZGraphicsAdapter(Graphics2D graphics)
	{
		this.source = graphics;
		Shape shape = graphics.getClip();
		this.shape = new ZShape(shape);
		this.antialiase(true);
	}
//	<< CONSTRUCTORS <<
	
//	>> PRIVATE SECTION >>
	private Graphics2D g()
	{
		return this.source;
	}
	private Graphics2D sub()
	{
		return (Graphics2D) g().create();
	}
	private void antialiase(boolean value)
	{
		g().setRenderingHint(RenderingHints.KEY_ANTIALIASING, value ? RenderingHints.VALUE_ANTIALIAS_ON : RenderingHints.VALUE_ANTIALIAS_OFF);
		g().setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, value ? RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY : RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED);
		g().setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, value ? RenderingHints.VALUE_COLOR_RENDER_QUALITY : RenderingHints.VALUE_COLOR_RENDER_SPEED);
		g().setRenderingHint(RenderingHints.KEY_DITHERING, value ? RenderingHints.VALUE_DITHER_ENABLE : RenderingHints.VALUE_DITHER_DISABLE);
		g().setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, value ? RenderingHints.VALUE_FRACTIONALMETRICS_ON : RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
		g().setRenderingHint(RenderingHints.KEY_INTERPOLATION, value ? RenderingHints.VALUE_INTERPOLATION_BICUBIC : RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
		g().setRenderingHint(RenderingHints.KEY_RENDERING, value ? RenderingHints.VALUE_RENDER_QUALITY : RenderingHints.VALUE_RENDER_SPEED);
		g().setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, value ? RenderingHints.VALUE_STROKE_NORMALIZE : RenderingHints.VALUE_STROKE_PURE);
		g().setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, value ? RenderingHints.VALUE_TEXT_ANTIALIAS_ON : RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
	}
	private void color(ZPaint paint)
	{
		g().setPaint(paint.toAWTPaint((int)shape.getWidth(), (int)shape.getHeight()));
	}
//	<< PRIVATE SECTION <<
	
//	>> DRAWING >>
	@Override
	public void drawShape(ZShape shape, double x, double y, ZPaint color, float size) 
	{
		if(size > 0)
		{
			this.color(color);
			g().setStroke(new BasicStroke(size, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
			g().draw(shape.toAWTShape(x, y));
		}
	}
	@Override
	public void fillShape(ZShape shape, int x, int y, ZPaint color) 
	{
		this.color(color);
		g().fill(shape.toAWTShape(x, y));
	}
	@Override
	public void drawString(String string, int x, int y, ZFont font, ZPaint color) 
	{
		this.color(color);
		g().setFont(font.toAWTFont());
		g().drawString(string, x, y);
	}

	@Override
	public void drawImage(ZImage image, int x, int y, int width, int height)
	{
		g().drawImage(image.asAWTImage(), x, y, width, height, null);
	}
	@Override
	public ZDrawer subDrawer(ZShape shape, int x, int y, double degree)
	{
		Shape clip = AffineTransform.getRotateInstance(Math.toRadians(degree), shape.getWidth() / 2, shape.getHeight() / 2).createTransformedShape(shape.toAWTShape(0, 0));
		Rectangle bounds = clip.getBounds();
		Graphics2D subSource = (Graphics2D) g().create(x, y, bounds.width, bounds.height);
		subSource.setClip(clip);
		subSource.rotate(Math.toRadians(degree), shape.getWidth() / 2, shape.getHeight() / 2);
		subSource.setColor(Color.black);
		return new ZGraphicsAdapter(subSource);
	}
//	<< DRAWING <<

//	>> CONFIGURATION >>
	@Override
	public ZShape getShape() 
	{
		return this.shape.copy();
	}
//	<< CONFIGURATION <<

//	>> IMPLEMENTATIONS >>
	@Override
	public ZDrawer copy() 
	{
		return new ZGraphicsAdapter(this.sub());
	}
//	<< IMPLEMENTATIONS <<
}
