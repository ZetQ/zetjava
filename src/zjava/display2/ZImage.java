package zjava.display2;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;

import zjava.ZObject;
import zjava.data.File;
import zjava.errors.AccessException;
import zjava.lang.functionals.Consumer;
import zjava.lang.tools.Copyable;
import zjava.lang.types.dec.SizeGetter;

public class ZImage extends ZObject implements SizeGetter, Copyable<ZImage>
{
	private BufferedImage
		image = null;
	
	public static ZImage path(String path) throws AccessException
	{
		return path(new File(path));
	}
	public static ZImage path(File file) throws AccessException
	{
		if(file.exists())
		{
			if(file.isImage())
			{
				try
				{
					return new ZImage(ImageIO.read(file.toFile()));
				}
				catch (IOException e)
				{
					throw new AccessException("unable to read image", e);
				}
			}
			throw new AccessException("file is not an image");
		}
		throw new AccessException("file does not exist");
	}
	public static ZImage url(String url) throws AccessException
	{
	    try
		{
	    	URL imgURL = new URL(url);
			return new ZImage(ImageIO.read(imgURL));
		}
	    catch(MalformedURLException e)
	    {
	    	throw new AccessException("unable to read url", e);
	    }
	    catch (IOException e)
		{
	    	throw new AccessException("unable to get image", e);
		}
	}
	
//	>> CONSTRUCTORS >>
	private ZImage(BufferedImage image)
	{
		this.image = image;
	}
	public ZImage(int width, int height)
	{
		this(width, height, drawer -> {});
	}
	public ZImage(int width, int height, Consumer<ZDrawer> drawing)
	{
		this.image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		drawing.consume(this.createDrawer());
	}
//	<< CONSTRUCTORS <<
	
//	>> MISC >>
	public ZDrawer createDrawer()
	{
		return new ZGraphicsAdapter(this.image.createGraphics());
	}
//	<< MISC <<
	
//	>> TRANSFORM >>
	public BufferedImage toAWTImage()
	{
		return this.copy().image;
	}
	BufferedImage asAWTImage()
	{
		return this.image;
	}
//	<< TRANSFORM <<
	
//	>> IMPLEMENTATIONS >>
	@Override
	public int getWidth()
	{
		return this.image.getWidth();
	}
	@Override
	public int getHeight()
	{
		return this.image.getHeight();
	}
	@Override
	public ZImage copy()
	{
		ZImage copy = new ZImage(this.getWidth(), this.getHeight());
		ZDrawer drawer = copy.createDrawer();
		drawer.drawImage(this, 0, 0, this.getWidth(), this.getHeight());
		return copy;
	}
//	<< IMPLEMENTATIONS <<

}
