package zjava.display2;

import zjava.ZObject;
import zjava.auto.annotations.Attribute;
import zjava.lang.reflect.ZClass;

public class ZStyle<T> extends ZObject
{
	public static <P> ZStyle<P> next(Class<P> type, P defValue)
	{
		return new ZStyle<>(type, defValue);
	}
	
	@Attribute(showName=false)
	private final ZClass<T>
		clazz;
	
	private final T
		defValue;
	
	private ZStyle(Class<T> clazz, T defValue)
	{
		this.clazz = ZClass.get(clazz);
		this.defValue = defValue;
	}
	public T getDefault()
	{
		return this.defValue;
	}
	public Class<T> getStyleType()
	{
		return this.clazz.asClass();
	}
}
