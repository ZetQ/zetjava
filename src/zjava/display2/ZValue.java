package zjava.display2;

@FunctionalInterface
public interface ZValue 
{
	public static final ZValue
		_0 = (ref) -> 0,
		_100 = (ref) -> ref;
	
	public int calc(int ref); 
}
