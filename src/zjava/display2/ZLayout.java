package zjava.display2;

import zjava.ZObject;
import zjava.lang.types.collections.ZMap;
import zjava.lang.utils.Ray;

public class ZLayout extends ZObject implements ZLayoutable
{
	private final ZMap<ZState, ZMap<ZStyle<?>, Object>>
		styles = new ZMap<>();
	
//	>> CONSTRUCTORS >>
	public ZLayout() {}
	public ZLayout(ZStyleSet... styles)
	{
		for(ZStyleSet set : styles)
		{
			this.style(set);
		}
	}
//	<< CONSTRUCTORS <<
	
//	>> PRIVATE SECTION >>
	@SuppressWarnings("unchecked")
	private <P> P getStyle(ZMap<ZStyle<?>, Object> map, ZStyle<P> style)
	{
		Object value = map.get(notNull(style));
		return null == value ? null : (P) value;
	}
	public <P> P[] getStyles(ZMap<ZStyle<?>, Object> map, ZStyle<P>[] styles)
	{
		if(0 == styles.length)
		{
			return null;
		}
		
		P[] array = Ray.create(styles[0].getStyleType(), styles.length);
		for(int i = 0; i < styles.length; ++i)
		{
			array[i] = getStyle(map, styles[i]);
		}
		return array;
	}
//	<< PRIVATE SECTION <<
	
//	>> GETTERS >>
	public <P> P get(ZStyle<P> style, ZState state)
	{
		ZMap<ZStyle<?>, Object> map = this.styles.get(notNull(state));
		if(null == map) return null;
		return this.getStyle(map, style);
	}
//	<< GETTERS <<

	@Override
	public void style(ZState state, ZStyleSet... styles)
	{
		ZMap<ZStyle<?>, Object> map = this.styles.get(notNull(state));
		if(null == map)
		{
			map = new ZMap<>();
			this.styles.add(state, map);
		}
		for(ZStyleSet set : styles)
		{
			for(ZRestyle<?> restyle : set)
			{
				map.put(restyle.getStyle(), notNull(restyle.getValue()));
			}
		}
	}
	@Override
	public void reset(ZStyle<?> style)
	{
		for(ZMap<ZStyle<?>, Object> map : this.styles.subValues())
		{
			map.remove(style);
		}
	}
}
