package zjava.display2;

import java.awt.Component;

import zjava.ZObject;
import zjava.display2.attributes.Bounds;
import zjava.display2.elements.ZElement;
import zjava.errors.ArgumentException;
import zjava.lang.types.ZBounds;
import zjava.lang.types.collections.MapEntry;
import zjava.lang.types.collections.ZList;
import zjava.lang.types.collections.ZMap;
import zjava.lang.types.dec.BoundsGetter;
import zjava.lang.types.dec.PointGetter;

public abstract class ZContainer extends ZObject implements PointGetter
{	
	private final ZMap<String, ZList<ZElement>>
		bgElements = new ZMap<>(),
		elements = new ZMap<>(),
		fgElements = new ZMap<>();
	
	private final ZList<String>
		hiddenGroups = new ZList<>();
	
	private ZContainer
		parent = null;
	
	protected ZContainer() {}
	
//	>> PRIVATE SECTION >>
	private <P extends ZContainer> ZContainer local(P object)
	{
		return object;
	}
	private void clearEmptyGroups(ZMap<String, ZList<ZElement>> inMap)
	{
		inMap.removeByValue(value -> value.isEmpty());
	}
	private void addNew(ZElement element, ZMap<String, ZList<ZElement>> inMap, String group)
	{
		element = notNull(element);
		if(this == element)
		{
			throw new ArgumentException("a ZElement should not be added to itself");
		}
		local(element).setParent(this);
		if(!inMap.containsKey(group))
		{
			inMap.add(group, new ZList<>(element));
		}
		else
		{
			inMap.get(group).add(element);
		}
	}
	private boolean removeElement(ZElement element, ZMap<String, ZList<ZElement>> inMap)
	{
		ZList<Boolean> removed = new ZList<>();
		inMap.eachValue(list -> { removed.add(list.remove(element)); });
		this.clearEmptyGroups(inMap);
		local(element).setParent(null);
		this.removeComponents(element);
		return removed.contains(true);
	}
	private ZList<ZElement> getVisible(ZMap<String, ZList<ZElement>> map)
	{
		ZList<ZElement> visible = new ZList<>();
		for(MapEntry<String, ZList<ZElement>> entry : map)
		{
			if(!this.hiddenGroups.contains(entry.getKey()))
			{
				visible.add(entry.getValue());
			}
		}
		return visible;
	}
	private void drawElement(BoundsGetter parent, ZDrawer drawer, ZElement element)
	{
		int x = element.getStyle(Bounds.x()).calc(parent.getWidth());
		int y = element.getStyle(Bounds.y()).calc(parent.getHeight());
		int width = element.getStyle(Bounds.width()).calc(parent.getWidth());
		int height = element.getStyle(Bounds.height()).calc(parent.getHeight());
		ZDrawer subDrawer = drawer.subDrawer(element.getStyle(Bounds.shape()).create(width, height), x, y, element.getStyle(Bounds.rotate()));
		local(element).drawInit(subDrawer, new ZBounds(x, y, width, height));
	}
//	<< PRIVATE SECTION <<
	
//	>> PROTECTED SECTION >>
	protected void drawInit(ZDrawer drawer, BoundsGetter bounds)
	{	
		for(ZElement element : this.getVisible(this.bgElements))
		{
			this.drawElement(bounds, drawer, element);
		}
		for(ZElement element : this.getVisible(this.elements))
		{
			this.drawElement(bounds, drawer, element);
		}
		for(ZElement element : this.getVisible(this.fgElements))
		{
			this.drawElement(bounds, drawer, element);
		}
	}
	protected void addComponent(ZElement source, Component component)
	{
		if(null == this.parent)
		{
			throw new ArgumentException("no parent to display component");
		}
		this.parent.addComponent(source, component);
	}
	protected void removeComponents(ZElement source)
	{
		if(null == this.parent)
		{
			throw new ArgumentException("no parent to display component");
		}
		this.parent.removeComponents(source);
	}
	protected ZContainer getParent()
	{
		return this.parent;
	}
	protected void setParent(ZContainer parent)
	{
		this.parent = parent;
	}
//	<< PROTECTED SECTION <<
	
//	>> ADD >>
	public void add(ZElement element)
	{
		this.addNew(element, this.elements, null);
	}
	public void add(ZElement element, String group)
	{
		this.addNew(element, this.elements, notNull(group));
	}
	public void addBG(ZElement element)
	{
		this.addNew(element, this.bgElements, null);
	}
	public void addBG(ZElement element, String group)
	{
		this.addNew(element, this.bgElements, notNull(group));
	}
	public void addFG(ZElement element)
	{
		this.addNew(element, this.fgElements, null);
	}
	public void addFG(ZElement element, String group)
	{
		this.addNew(element, this.fgElements, notNull(group));
	}
//	<< ADD <<
	
//	>> REMOVE >>
	public boolean remove(String name)
	{
		return this.elements.remove(name);
	}
	public boolean removeAny(ZElement element)
	{
		boolean
		ce = remove(element),
		bg = removeBG(element),
		fg = removeFG(element);
		return ce || bg || fg;
	}
	public boolean remove(ZElement element)
	{
		return this.removeElement(element, this.elements);
	}
	public boolean removeBG(ZElement element)
	{
		return this.removeElement(element, this.bgElements);
	}
	public boolean removeFG(ZElement element)
	{
		return this.removeElement(element, this.fgElements);
	}
//	<< REMOVE <<
	
}
