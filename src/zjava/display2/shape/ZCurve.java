package zjava.display2.shape;

import java.awt.geom.CubicCurve2D;

import zjava.ZObject;
import zjava.auto.annotations.Attribute;

public class ZCurve extends ZObject
{	

	
	@Attribute
	private final double
		width,
		height,
		startWidth,
		startHeight,
		endWidth,
		endHeight;
	
	@Attribute
	private final boolean
		isMove;
	
	public ZCurve(double width, double height)
	{
		this(width, height, 0, 0, 0, 0);
	}
	public ZCurve(double width, double height, boolean isMove)
	{
		this(width, height, 0, 0, 0, 0, isMove);
	}
	public ZCurve(double width, double height, double ctrlWidth, double ctrlHeight)
	{
//		TODO test this - should work like a quad
		this(width, height, ctrlWidth, ctrlHeight, width - ctrlWidth, height - ctrlHeight);
	}
	public ZCurve(double width, double height, double startWidth, double startHeight, double endWidth, double endHeight)
	{
		this(width, height, startWidth, startHeight, endWidth, endHeight, false);
	}
	public ZCurve(double width, double height, double startWidth, double startHeight, double endWidth, double endHeight, boolean isMove)
	{
		this.width = width;
		this.height = height;
		this.startWidth = startWidth;
		this.startHeight = startHeight;
		this.endWidth = endWidth;
		this.endHeight = endHeight;
		
		this.isMove = isMove;
	}
	
	public boolean isMove()
	{
		return this.isMove;
	}
	
//	>> SIZE >>
	public double getWidth()
	{
		return this.width;
	}
	public double getHeight()
	{
		return this.height;
	}
//	<< SIZE <<
	
//	>> START >>
	public double getStartWidth()
	{
		return this.startWidth;
	}
	public double getStartHeight()
	{
		return this.startHeight;
	}
//	<< START <<
	
//	>> END >>
	public double getEndWidth()
	{
		return this.endWidth;
	}
	public double getEndHeight()
	{
		return this.endHeight;
	}
//	<< END <<
	
//	>> TRANSFORM >>
	public CubicCurve2D toAWTCurve(double x, double y)
	{
		return new CubicCurve2D.Double(x, y, x + this.getStartWidth(), y + this.getStartHeight(), x + this.getEndWidth(), y + this.getEndHeight(), x + this.getWidth(), y + this.getHeight());
	}
	public ZCurve toReverse()
	{
		return new ZCurve(this.getWidth() * -1, this.getHeight() * -1, this.getEndWidth(), this.getEndHeight(), this.getStartWidth(), this.getStartHeight());
	}
	public ZCurve toMirrored()
	{
		return new ZCurve(this.getWidth() * -1, this.getHeight() * -1, this.getStartWidth() * -1, this.getStartHeight() * -1, this.getEndWidth() * -1, this.getEndHeight() * -1);
	}
//	<< TRANSFORM <<
}
