package zjava.display2.shape;

import zjava.display2.Unit;
import zjava.display2.ZValue;

@FunctionalInterface
public interface DynamicShape
{	
	public static DynamicShape rect()
	{
		return (width, height) -> ZShape.createRect(width, height);
	}
	public static DynamicShape ellipse()
	{
		return (width, height) -> ZShape.createEllipse(width, height);
	}
	public static DynamicShape roundRect(String arcW, String arcH)
	{
		ZValue w = Unit.decode(arcW), h = Unit.decode(arcH);
		return (width, height) -> ZShape.createRoundRect(width, height, w.calc(width), h.calc(height));
	}
	public static DynamicShape roundRect(String arc)
	{
		ZValue arcVal = Unit.decode(arc);
		return (width, height) -> {
			int ref = (int)((float)(width + height) / 2);
			int arcSize = arcVal.calc(ref);
			return ZShape.createRoundRect(width, height, arcSize, arcSize);
		};
	}
	public static DynamicShape triangle(double degree)
	{
		return (width, height) -> ZShape.createTriangle(width, height, degree);
	}
	public static DynamicShape line(String x, String y, String width, String height)
	{
		ZValue dX = Unit.decode(x), dY = Unit.decode(y), w = Unit.decode(width), h = Unit.decode(height);
		return (iWidth, iHeight) -> new ZShape(new ZCurve(dX.calc(iWidth), dY.calc(iHeight), true), 
											   new ZCurve(w.calc(iWidth), h.calc(iHeight)));
	}
	
	public ZShape create(int width, int height);
}
