package zjava.display2.shape;

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.awt.geom.RoundRectangle2D;
import java.util.Iterator;

import zjava.ZObject;
import zjava.lang.tools.Copyable;
import zjava.lang.types.collections.ZList;

public class ZShape extends ZObject implements Iterable<ZCurve>, Copyable<ZShape>
{
	public static ZShape createRect(double width, double height)
	{
		return new ZShape(new ZCurve(width, 0), new ZCurve(0, height), new ZCurve(-width, 0), new ZCurve(0, -height));
	}
	public static ZShape createEllipse(double width, double height)
	{
		return new ZShape(new Ellipse2D.Double(0, 0, width, height));
	}
	public static ZShape createRoundRect(double width, double height, double arcW, double arcH)
	{
//		arcW >= width ? width : arcW, arcH >= height ? height : arcH
		return new ZShape(new RoundRectangle2D.Double(0, 0, width, height, arcW, arcH));
	}
	public static ZShape createTriangle(double width, double height, double degree)
	{
		return new ZShape(new ZCurve(height, 0, true), new ZCurve(width, 0), new ZCurve(-width / 2, -height), new ZCurve(-width / 2, height)).toRotated(degree);
	}
	
	private final ZList<ZCurve>
		curves = new ZList<>();
	
	private double
		width,
		height;
	
	private double
		wPos = 0,
		hPos = 0,
		wNeg = 0,
		hNeg = 0;
	
//	>> CONSTRUCTORS >>
	public ZShape() {}
	public ZShape(ZCurve... curves)
	{
		for(ZCurve curve : curves) this.add(curve);
	}
	public ZShape(Shape shape)
	{
		this(shape, new AffineTransform());
	}
	public ZShape(Shape shape, AffineTransform at)
	{
		this.initCurves(shape.getBounds(), shape.getPathIterator(at));
	}
//	<< CONSTRUCTORS <<
	
//	>> INITIALIZERS >>
	private void initCurves(Rectangle offset, PathIterator path)
	{
		double x = 0, y = 0;
		while(!path.isDone())
		{
			double[] line = new double[6];
			int type = path.currentSegment(line);
			if(PathIterator.SEG_CLOSE != type)
			{
				ZCurve curve = null;
				
				boolean isMove = false;
				double endX = 0, endY = 0;
				switch(type)
				{
					case PathIterator.SEG_MOVETO: { isMove = true; }
					case PathIterator.SEG_LINETO:
					{
						endX = line[0]; endY = line[1];
						curve = new ZCurve(endX - x, endY - y, isMove);
						break;
					}
					case PathIterator.SEG_QUADTO:
					{
						endX = line[2]; endY = line[3];
						curve = new ZCurve(endX - x, endY - y, line[0] - x, line[1] - y);
						break;
					}
					case PathIterator.SEG_CUBICTO:
					{
						
						endX = line[4]; endY = line[5];
						curve = new ZCurve(endX - x, endY - y, line[0] - x, line[1] - y, line[2] - endX, line[3] - endY);
						break;
					}
				}
				this.add(curve);
				x = endX; y = endY;
			}
			path.next();
		}
	}
//	<< INITIALIZERS <<
	
//	>> FUNCTIONS >>
	protected void add(ZCurve curve)
	{
		this.curves.add(curve);
		this.width += curve.getWidth();
		this.height += curve.getHeight();
		
		if(curve.getWidth() > 0)
		{
			if(this.width > this.wPos)
			{
				this.wPos = this.width;
			}
		}
		else if(this.width < this.wNeg)
		{
			this.wNeg = this.width;
		}
		if(curve.getHeight() > 0)
		{
			if(this.height > this.hPos)
			{
				this.hPos = this.height;
			}
		}
		else if(this.height < this.hNeg)
		{
			this.hNeg = this.height;
		}
	}
//	<< FUNCTIONS <<
	
//	>> SIZE >>
	public double getWidth()
	{
		return this.wNeg * -1 + this.wPos;
	}
	public double getHeight()
	{
		return this.hNeg * -1 + this.hPos;
	}
//	<< SIZE <<
	
//	>> TRANSFORM >>
	public ZShape toRotated(double degree)
	{
		return new ZShape(this.toAWTShape(0, 0), AffineTransform.getRotateInstance(Math.toRadians(degree), this.getWidth() / 2, this.getHeight() / 2));
	}
	public Shape toAWTShape(double x, double y)
	{
		GeneralPath path = new GeneralPath();
		path.moveTo(x, y);
		double locX = x;
		double locY = y;
		
		for(ZCurve curve : this)
		{
			double startX = locX;
			double startY = locY;
			locX += curve.getWidth();
			locY += curve.getHeight();

			if(curve.isMove())
			{
				path.moveTo(locX, locY);
			}
			else
			{
				double sW = curve.getStartWidth(), sH = curve.getStartHeight(), eW = curve.getEndWidth(), eH = curve.getEndHeight();
				
				if(sW == 0 && sH == 0 && eW == 0 && eH == 0)
				{
					path.lineTo(locX, locY);
				}
				else
				{
					path.curveTo(startX + sW, startY + sH, 
							 locX + eW, locY + eH,
							 locX, locY);
				}
			}
		}
		path.closePath();
		return path;
	}
//	<< TRANSFORM <<
	
//	>> IMPLEMENTATIONS >>
	@Override
	public Iterator<ZCurve> iterator()
	{
		return this.curves.iterator();
	}
	@Override
	public ZShape copy()
	{
		return new ZShape(this.curves.toArray(ZCurve.class));
	}
//	<< IMPLEMENTATIONS <<
}
