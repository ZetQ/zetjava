package zjava.display2;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import zjava.display2.color.ZColor;
import zjava.display2.elements.ZElement;
import zjava.errors.UnexpectedException;
import zjava.errors.UnsupportedException;
import zjava.lang.functionals.Consumer;
import zjava.lang.types.ZBounds;
import zjava.lang.types.collections.ZList;
import zjava.lang.types.collections.ZMap;
import zjava.lang.types.dec.BoundsSetter;
import zjava.system.ZScreen;

public class ZWindow extends ZContainer implements BoundsSetter
{
	public static enum WindowMode { WINDOWED, MINIMIZED, MAXIMIZED, FULLSCREENED, INVISIBLE, ; }
	public static enum WindowAction 
	{
		/** window is closing */ CLOSE, 
		/** window was activated by mouse click */ ACTIVATION,
		/** window was deactivated by mouse click */ DEACTIVATION,
		/** window changes from {@code HIDDEN} to any other mode */ SHOW, 
		/** window changes from {@code MINIMIZED} to any other mode, excluding {@code HIDDEN} */ OPEN,
		/** window changes to {@code WINDOWED} */ WINDOW, 
		/** window changes to {@code MINIMIZED} */ MINIMIZE, 
		/** window changes to {@code MAXIMIZED} */ MAXMINIZE, 
		/** window changes to {@code FULLSCREENED} */ FULLSCREEN, 
		/** window changes to {@code HIDDEN} */ HIDE
	};
	
	private final static ZColor
		BACKGROUND_COLOR = new ZColor(240, 240, 240);
	
	private JFrame
		frame;
	
	private JPanel
		panel;
	
	private ZMap<ZElement, ZList<Component>>
		components = new ZMap<>();
		
	private ZBounds
		savedArea;
	
	private ZMap<WindowAction, ZList<Consumer<ZWindow>>>
		onMap = new ZMap<>();
	
	private ZColor
		bgColor = ZWindow.BACKGROUND_COLOR;
	
	public ZWindow(String title, String width, String height)
	{
		this(title, 0, 0, Unit.decode(width).calc(ZScreen.getWidth()),
						  Unit.decode(height).calc(ZScreen.getHeight()));
		this.center();
	}
	public ZWindow(String title, String x, String y, String width, String height)
	{
		this(title, Unit.decode(x).calc(ZScreen.getWidth()),
					Unit.decode(y).calc(ZScreen.getHeight()),
				  	Unit.decode(width).calc(ZScreen.getWidth()),
				  	Unit.decode(height).calc(ZScreen.getHeight()));
	}
	private ZWindow(String title, int x, int y, int width, int height)
	{
		this.init(title, x, y, width, height);
		
		for(WindowAction action : WindowAction.values())
		{
			this.onMap.add(action, new ZList<>());
		}
	}
	
//	>> INITIALIZERS >>
	private void init(String title, int x, int y, int width, int height)
	{
		this.initFrame(title, x, y, width, height);
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent wEvent) 
			{
				doAction(WindowAction.CLOSE);
				frame.dispose();
			}});
	}
	private void initFrame(String title, int x, int y, int width, int height)
	{
		this.frame = new JFrame(title);
		final ZWindow source = this;
		this.panel = new JPanel(null)
		{
			private static final long serialVersionUID = -2784103884120838358L;

			@Override
			public void paint(Graphics g)
			{
				super.paint(g);
				source.drawInit(new ZGraphicsAdapter((Graphics2D) g), source.getInside());
				g.dispose();
			}
		};
		this.components.eachValue(comps -> comps.forEach(comp -> this.panel.add(comp)));
		this.frame.add(this.panel);
		this.frame.setLocation(x, y);
		this.frame.setSize(width, height);
	}
//	<< INITIALIZERS <<
	
//	>> PRIVATE SECTION >>
	private void doAction(WindowAction action)
	{
		this.onMap.get(action).forEach(consumer -> consumer.consume(this));
	}
//	<< PRIVATE SECTION <<
	
//	>> ACTION LISTENER >>
	public void on(WindowAction action, Consumer<ZWindow> consumer)
	{
		this.onMap.get(action).add(consumer);
	}
	public boolean removeOn(WindowAction action, Consumer<ZWindow> consumer)
	{
		return this.onMap.get(action).remove(consumer);
	}
//	<< ACTION LISTENER <<
	
	public ZBounds getInside()
	{
		Insets in = this.frame.getInsets();
		return new ZBounds(0, 0, this.getWidth() - in.left - in.right, this.getHeight() - in.top - in.bottom);
	}
	public WindowMode getMode()
	{
		if(!this.frame.isVisible())
		{
			return WindowMode.INVISIBLE;
		}
		
		switch(this.frame.getExtendedState())
		{
			case JFrame.NORMAL:
			{
				return WindowMode.WINDOWED;
			}
			case JFrame.ICONIFIED:
			{
				return WindowMode.MINIMIZED;
			}
			case JFrame.MAXIMIZED_BOTH:
			{
				if(frame.isUndecorated())
				{
					return WindowMode.FULLSCREENED;
				}
				return WindowMode.MAXIMIZED;
			}
		}
		throw new UnexpectedException("the current mode of the window (ExtendedState=" + this.frame.getExtendedState() + ") is not supported");
	}
	public void setMode(WindowMode mode)
	{
		WindowMode currMode = this.getMode();
		
		if(mode != currMode)
		{
			if(currMode != WindowMode.FULLSCREENED)
			{
				this.frame.setVisible(false);
				this.init(this.getTitle(), this.savedArea.getX(), this.savedArea.getY(), this.savedArea.getWidth(), this.savedArea.getHeight());
			}
			if(mode != WindowMode.INVISIBLE)
			{
				switch(mode)
				{
					case WINDOWED:
					{
						this.frame.setExtendedState(JFrame.NORMAL);
						break;
					}
					case MINIMIZED:
					{
						this.frame.setExtendedState(JFrame.ICONIFIED);
						break;
					}
					case MAXIMIZED:
					{
						this.frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
						break;
					}
					case FULLSCREENED:
					{
						this.frame.setVisible(false);
						this.savedArea = new ZBounds(this);
						this.init(this.getTitle(), this.getX(), this.getY(), this.getWidth(), this.getHeight());
						this.frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
						this.frame.setUndecorated(true);
						break;
					}
					case INVISIBLE:
					{
						throw new UnexpectedException("the mode INVISIBLE shouldn't be possible in this switch-statement");
					}
				}
				this.frame.setVisible(true);
			}
			else
			{
				this.frame.setVisible(false);
			}
		}
	}
	public void center()
	{
		this.frame.setLocationRelativeTo(null);
	}
	public void redraw()
	{
		this.frame.repaint();
	}
	public String getTitle()
	{
		return this.frame.getTitle();
	}
	public void setTitle(String title)
	{
		this.frame.setTitle(title);
	}
	public ZColor getBGColor()
	{
		return this.bgColor;
	}
	public void setBGColor(ZColor color)
	{
		this.bgColor = color;
	}
	@Override
	public int getX()
	{
		return this.frame.getX();
	}
	@Override
	public int getY()
	{
		return this.frame.getY();
	}
	@Override
	public int getWidth()
	{
		return this.frame.getWidth();
	}
	@Override
	public int getHeight()
	{
		return this.frame.getHeight();
	}
	@Override
	public void setWidth(int width)
	{
		this.frame.setSize(width, this.getHeight());
	}
	@Override
	public void setHeight(int height)
	{
		this.frame.setSize(this.getWidth(), height);
	}
	@Override
	public void setX(int x)
	{
		this.frame.setLocation(x, this.getY());
	}
	@Override
	public void setY(int y)
	{
		this.frame.setLocation(this.getX(), y);
	}
	
//	>> IMPLEMENTATIONS >>
	@Override
	protected void setParent(ZContainer parent)
	{
		throw new UnsupportedException("a window cannot be added to a container");
	}
	@Override
	protected void addComponent(ZElement source, Component component) 
	{
		if(this.components.containsKey(source))
		{
			this.components.get(source).add(component);
		}
		else
		{
			this.components.add(source, new ZList<>(component));
		}
		this.panel.add(component);
	}
	@Override
	protected void removeComponents(ZElement source) 
	{
		this.components.getByKey(key -> key == source).forEach(comps -> comps.forEach(comp -> this.panel.remove(comp)));
		this.components.remove(source);
	}
//	<< IMPLEMENTATIONS <<
}
