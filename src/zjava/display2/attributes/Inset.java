package zjava.display2.attributes;

import zjava.display2.Unit;
import zjava.display2.ZRestyle;
import zjava.display2.ZStyle;
import zjava.display2.ZStyleSet;
import zjava.display2.ZValue;

public class Inset
{
	private static final ZStyle<ZValue>
		NORTH_ID = ZStyle.next(ZValue.class, ZValue._0),
		EAST_ID = ZStyle.next(ZValue.class, ZValue._0),
		SOUTH_ID = ZStyle.next(ZValue.class, ZValue._0),
		WEST_ID = ZStyle.next(ZValue.class, ZValue._0);

	//>> STYLE >>
	public static ZStyle<ZValue>[] style()
	{
		@SuppressWarnings("unchecked") ZStyle<ZValue>[] array = (ZStyle<ZValue>[]) new ZStyle<?>[] { north(), east(), south(), west() };
		return array;
	}
	public static ZStyleSet style(String value)
	{
		return style(value, value, value, value);
	}
	public static ZStyleSet style(String v, String h)
	{
		return style(v, h, v, h);
	}
	public static ZStyleSet style(String north, String east, String south, String west)
	{
		return new ZStyleSet(north(north), east(east), south(south), west(west));
	}
	//<< STYLE <<
	
	//>> NORTH >>
	public static ZStyle<ZValue> north()
	{
		return NORTH_ID;
	}
	public static ZStyleSet north(String value)
	{
		return new ZStyleSet(new ZRestyle<>(north(), Unit.decode(value)));
	}
	//<< NORTH <<
	
	//>> EAST >>
	public static ZStyle<ZValue> east()
	{
		return EAST_ID;
	}
	public static ZStyleSet east(String value)
	{
		return new ZStyleSet(new ZRestyle<>(east(), Unit.decode(value)));
	}
	//<< EAST <<
	
	//>> SOUTH >>
	public static ZStyle<ZValue> south()
	{
		return SOUTH_ID;
	}
	public static ZStyleSet south(String value)
	{
		return new ZStyleSet(new ZRestyle<>(south(), Unit.decode(value)));
	}
	//<< SOUTH <<
	
	//>> WEST >>
	public static ZStyle<ZValue> west()
	{
		return WEST_ID;
	}
	public static ZStyleSet west(String value)
	{
		return new ZStyleSet(new ZRestyle<>(west(), Unit.decode(value)));
	}
	//<< WEST <<
	
	protected Inset() {}
}
