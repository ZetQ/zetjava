package zjava.display2.attributes;

import zjava.display2.Unit;
import zjava.display2.ZRestyle;
import zjava.display2.ZStyle;
import zjava.display2.ZStyleSet;
import zjava.display2.ZValue;
import zjava.display2.color.ZColor;
import zjava.lang.utils.Ray;

public class Text
{
	public static enum TextHor
	{
		LEFT, CENTER, RIGHT, JUSTIFY
	}
	public static enum TextVer
	{
		TOP, CENTER, BOTTOM
	}
	public static enum TextDeco
	{
		ITALIC, BOLD, UNDERLINE, OVERLINE, STRIKETHROUGH, SMALLCAPS
	}
	public static enum TextForm
	{
		NONE, UPPERCASE, LOWERCASE, CAPITALIZE, SUPERSCRIPT
	}
	public static enum TextWeight
	{
		_1(100), _2(200), _3(300), _4(400), _5(500), _6(600), _7(700), _8(800), _9(900);
		
		public static final TextWeight
			NORMAL = _4,
			BOLD = _7;
		
		public static TextWeight get(int weight)
		{
			return weight <= 150 ? _1 : weight <= 250 ? _2 : weight <= 350 ? _3 : weight <= 450 ? _4 : 
				   weight <= 550 ? _5 : weight <= 650 ? _6 : weight <= 750 ? _7 : weight <= 850 ? _8 : _9; 
		}
		
		private final int
			weight;
		
		private TextWeight(int weight)
		{
			this.weight = weight;
		}
		public int getWeight()
		{
			return weight;
		}
	}
	
	public static final ZStyle<ZColor>
		COLOR_ID = ZStyle.next(ZColor.class, ZColor.BLACK);
	
	public static final ZStyle<TextHor>
		HOR_ID = ZStyle.next(TextHor.class, TextHor.CENTER);
	
	public static final ZStyle<TextVer>
		VER_ID = ZStyle.next(TextVer.class, TextVer.CENTER);
	
	public static final ZStyle<TextDeco[]>
		DECO_ID = ZStyle.next(TextDeco[].class, new TextDeco[0]);
	
	public static final ZStyle<TextForm>
		FORM_ID = ZStyle.next(TextForm.class, TextForm.NONE);

	public static final ZStyle<ZValue>
		SIZE_ID = ZStyle.next(ZValue.class, Unit.decode("16px")),
		LETTERSPACE_ID = ZStyle.next(ZValue.class, ZValue._0),
		WORDSPACE_ID = ZStyle.next(ZValue.class, ZValue._0),
		LINESPACE_ID = ZStyle.next(ZValue.class, ZValue._100),
		SHADOWX_ID = ZStyle.next(ZValue.class, ZValue._0),
		SHADOWY_ID = ZStyle.next(ZValue.class, ZValue._0);
	
	public static final ZStyle<TextWeight>
		WEIGHT_ID = ZStyle.next(TextWeight.class, TextWeight.NORMAL);
	
	public static final ZStyle<String>
		FAMILY_ID = ZStyle.next(String.class, "Arial");
	
//	>> SHADOW >>
	public static ZStyle<ZValue>[] shadow()
	{
		@SuppressWarnings("unchecked") ZStyle<ZValue>[] array = (ZStyle<ZValue>[]) new ZStyle<?>[] { shadowX(), shadowY() };
		return array;
	}
	public static ZStyleSet shadow(String amount)
	{
		return shadow(amount, amount);
	}
	public static ZStyleSet shadow(String x, String y)
	{
		return new ZStyleSet(shadowX(x), shadowY(y));
	}
	public static ZStyle<ZValue> shadowX()
	{
		return SHADOWX_ID;
	}
	public static ZStyleSet shadowX(String x)
	{
		return new ZStyleSet(new ZRestyle<>(shadowX(), Unit.decode(x)));
	}
	public static ZStyle<ZValue> shadowY()
	{
		return SHADOWY_ID;
	}
	public static ZStyleSet shadowY(String y)
	{
		return new ZStyleSet(new ZRestyle<>(shadowY(), Unit.decode(y)));
	}
//	<< SHADOW <<
	
//	>> LINE SPACE >>
	public static ZStyle<ZValue> lineSpace()
	{
		return LINESPACE_ID;
	}
	public static ZStyleSet lineSpace(String space)
	{
		return new ZStyleSet(new ZRestyle<>(lineSpace(), Unit.decode(space)));
	}
//	<< LINE SPACE <<
	
//	>> WORD SPACE >>
	public static ZStyle<ZValue> wordSpace()
	{
		return WORDSPACE_ID;
	}
	public static ZStyleSet wordSpace(String space)
	{
		return new ZStyleSet(new ZRestyle<>(wordSpace(), Unit.decode(space)));
	}
//	<< WORD SPACE <<
	
//	>> LETTER SPACE >>
	public static ZStyle<ZValue> letterSpace()
	{
		return LETTERSPACE_ID;
	}
	public static ZStyleSet letterSpace(String space)
	{
		return new ZStyleSet(new ZRestyle<>(letterSpace(), Unit.decode(space)));
	}
//	<< LETTER SPACE <<
	
//	>> WEIGHT >>
	public static ZStyle<TextWeight> weight()
	{
		return WEIGHT_ID;
	}
	public static ZStyleSet weight(TextWeight weight)
	{
		return new ZStyleSet(new ZRestyle<>(weight(), weight));
	}
//	<< WEIGHT <<
	
//	>> FONT >>
	public static ZStyle<?>[] font()
	{
		return new ZStyle<?>[] { family(), size() };
	}
	public static ZStyleSet font(String family, String size)
	{
		return new ZStyleSet(family(family), size(size));
	}
//	<< FONT <<
	
//	>> FAMILY >>
	public static ZStyle<String> family()
	{
		return FAMILY_ID;
	}
	public static ZStyleSet family(String family)
	{
		return new ZStyleSet(new ZRestyle<>(family(), family));
	}
//	<< FAMILY <<
	
//	>> SIZE >>
	public static ZStyle<ZValue> size()
	{
		return SIZE_ID;
	}
	public static ZStyleSet size(String size)
	{
		return new ZStyleSet(new ZRestyle<>(size(), Unit.decode(size)));
	}
//	<< SIZE << 
	
//	>> TRANSFORM >>
	public static ZStyle<TextForm> form()
	{
		return FORM_ID;
	}
	public static ZStyleSet form(TextForm form)
	{
		return new ZStyleSet(new ZRestyle<>(form(), form));
	}
//	<< TRANSFORM <<
	
//	>> DECORATION >>
	public static ZStyle<TextDeco[]> deco()
	{
		return DECO_ID;
	}
	public static ZStyleSet deco(TextDeco... deco)
	{
		return new ZStyleSet(new ZRestyle<>(deco(), Ray.copy(deco)));
	}
//	<< DECORATION <<
	
//	>> LOCATION >>
	public static ZStyle<?>[] location()
	{
		return new ZStyle<?>[] { horizontal(), vertical() };
	}
	public static ZStyleSet location(TextHor h, TextVer v)
	{
		return new ZStyleSet(horizontal(h), vertical(v));
	}
	public static ZStyle<TextHor> horizontal()
	{
		return HOR_ID;
	}
	public static ZStyleSet horizontal(TextHor horizontal)
	{
		return new ZStyleSet(new ZRestyle<>(horizontal(), horizontal));
	}
	public static ZStyle<TextVer> vertical()
	{
		return VER_ID;
	}
	public static ZStyleSet vertical(TextVer vertical)
	{
		return new ZStyleSet(new ZRestyle<>(vertical(), vertical));
	}
//	<< LOCATION <<
	
//	>> COLOR >>
	public static ZStyle<ZColor> color()
	{
		return COLOR_ID;
	}
	public static ZStyleSet color(int red, int green, int blue)
	{
		return color(new ZColor(red, green, blue));
	}
	public static ZStyleSet color(int red, int green, int blue, int alpha)
	{
		return color(new ZColor(red, green, blue, alpha));
	}
	public static ZStyleSet color(ZColor color)
	{
		return new ZStyleSet(new ZRestyle<>(color(), color));
	}
//	<< COLOR <<
}
