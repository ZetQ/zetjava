package zjava.display2.attributes;

import zjava.display2.Unit;
import zjava.display2.ZRestyle;
import zjava.display2.ZStyle;
import zjava.display2.ZStyleSet;
import zjava.display2.ZValue;
import zjava.display2.color.ZColor;

public class Border extends ZAttribute
{
	@FunctionalInterface
	public static interface SizeRel
	{
		public static final SizeRel
			WIDTH = (width, height) -> width,
			HEIGHT = (width, height) -> height,
			SMALLER = (width, height) -> width > height ? height : width,
			BIGGER = (width, height) -> width > height ? width : height,
			AVERAGE = (width, height) -> (width + height) / 2;
		
		public int calc(int width, int height);
	}
	
	private static final ZStyle<ZColor>
		COLOR_ID = ZStyle.next(ZColor.class, ZColor.BLACK);
	
	private static final ZStyle<ZValue>
		SIZE_ID = ZStyle.next(ZValue.class, Unit.decode("1px"));
	
	private static final ZStyle<SizeRel>
		RELATION_ID = ZStyle.next(SizeRel.class, SizeRel.SMALLER);
	
//	>> STYLE >>
	public static ZStyle<?>[] style()
	{
		return new ZStyle<?>[] { color(), size(), relation() };
	}
	public static ZStyleSet style(String size, ZColor color)
	{
		return new ZStyleSet(size(size), color(color));
	}
//	<< STYLE <<
	
//	>> RELATION >>
	public static ZStyle<SizeRel> relation()
	{
		return RELATION_ID;
	}
	public static ZStyleSet relation(SizeRel relation)
	{
		return new ZStyleSet(new ZRestyle<>(relation(), relation));
	}
//	<< RELATION <<
	
//	>> SIZE >>
	public static ZStyle<ZValue> size()
	{
		return SIZE_ID;
	}
	public static ZStyleSet size(String size)
	{
		return new ZStyleSet(new ZRestyle<>(size(), Unit.decode(size)));
	}
//	<< SIZE <<
	
//	>> COLOR >>
	public static ZStyle<ZColor> color()
	{
		return COLOR_ID;
	}
	public static ZStyleSet color(int red, int green, int blue)
	{
		return color(new ZColor(red, green, blue));
	}
	public static ZStyleSet color(int red, int green, int blue, int alpha)
	{
		return color(new ZColor(red, green, blue, alpha));
	}
	public static ZStyleSet color(ZColor color)
	{
		return new ZStyleSet(new ZRestyle<>(color(), color));
	}
//	<< COLOR <<
	
	protected Border() {}
}
