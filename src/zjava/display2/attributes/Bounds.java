package zjava.display2.attributes;

import zjava.display2.Unit;
import zjava.display2.ZRestyle;
import zjava.display2.ZStyle;
import zjava.display2.ZStyleSet;
import zjava.display2.ZValue;
import zjava.display2.shape.DynamicShape;

public class Bounds extends ZAttribute
{
	private static final ZStyle<DynamicShape>
		SHAPE_ID = ZStyle.next(DynamicShape.class, DynamicShape.rect());
	
	private static final ZStyle<ZValue>
		X_ID = ZStyle.next(ZValue.class, ZValue._0),
		Y_ID = ZStyle.next(ZValue.class, ZValue._0),
		WIDTH_ID = ZStyle.next(ZValue.class, ZValue._100),
		HEIGHT_ID = ZStyle.next(ZValue.class, ZValue._100);
	
	private static final ZStyle<Double>
		ROTATE_ID = ZStyle.next(Double.class, 0d);
	
//	>> STYLE >>
	public static ZStyle<ZValue>[] style()
	{
		@SuppressWarnings("unchecked") ZStyle<ZValue>[] array = (ZStyle<ZValue>[]) new ZStyle<?>[] { shape(), x(), y(), width(), height(), rotate() };
		return array;
	}
	public static ZStyleSet style(DynamicShape shape, String x, String y, String width, String height)
	{
		return new ZStyleSet(shape(shape), x(x), y(y), width(width), height(height));
	}
	public static ZStyleSet style(String x, String y, String width, String height)
	{
		return new ZStyleSet(x(x), y(y), width(width), height(height));
	}
//	<< STYLE <<
	
//	>> SHAPE >>
	public static ZStyle<DynamicShape> shape()
	{
		return SHAPE_ID;
	}
	public static ZStyleSet shape(DynamicShape shape)
	{
		return new ZStyleSet(new ZRestyle<>(shape(), shape));
	}
//	<< SHAPE <<
	
//	>> LOCATION >>
	public static ZStyle<ZValue>[] location()
	{
		@SuppressWarnings("unchecked") ZStyle<ZValue>[] array = (ZStyle<ZValue>[]) new ZStyle<?>[] { x(), y() };
		return array;
	}
	public static ZStyleSet location(String location)
	{
		return location(location, location);
	}
	public static ZStyleSet location(String x, String y)
	{
		return new ZStyleSet(x(x), y(y));
	}
	public static ZStyle<ZValue> x()
	{
		return X_ID;
	}
	public static ZStyleSet x(String x)
	{
		return new ZStyleSet(new ZRestyle<>(x(), Unit.decode(x)));
	}
	public static ZStyle<ZValue> y()
	{
		return Y_ID;
	}
	public static ZStyleSet y(String y)
	{
		return new ZStyleSet(new ZRestyle<>(y(), Unit.decode(y)));
	}
//	<< LOCATION <<
	
//	>> SIZE >>
	public static ZStyle<ZValue>[] size()
	{
		@SuppressWarnings("unchecked") ZStyle<ZValue>[] array = (ZStyle<ZValue>[]) new ZStyle<?>[] { width(), height() };
		return array;
	}
	public static ZStyleSet size(String size)
	{
		return size(size, size);
	}
	public static ZStyleSet size(String width, String height)
	{
		return new ZStyleSet(width(width), height(height));
	}
	public static ZStyle<ZValue> width()
	{
		return WIDTH_ID;
	}
	public static ZStyleSet width(String width)
	{
		return new ZStyleSet(new ZRestyle<>(width(), Unit.decode(width)));
	}
	public static ZStyle<ZValue> height()
	{
		return HEIGHT_ID;
	}
	public static ZStyleSet height(String height)
	{
		return new ZStyleSet(new ZRestyle<>(height(), Unit.decode(height)));
	}
//	<< SIZE <<
	
//	>> ROTATION >>
	public static ZStyle<Double> rotate()
	{
		return ROTATE_ID;
	}
	public static ZStyleSet rotate(double degree)
	{
		return new ZStyleSet(new ZRestyle<>(rotate(), degree));
	}
//	<< ROTATION <<
	
	protected Bounds() {}
}
