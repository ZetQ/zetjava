package zjava.display2.attributes;

import zjava.display2.ZImage;
import zjava.display2.ZRestyle;
import zjava.display2.ZStyle;
import zjava.display2.ZStyleSet;
import zjava.display2.color.ZColor;
import zjava.display2.color.ZPaint;
import zjava.lang.utils.Ray;

public class Background extends ZAttribute
{	
	public static enum ImageAdapt
	{
		STRETCH, REPEAT, POSITION
	}
	
	private static final ZStyle<ZPaint>
		COLOR_ID = ZStyle.next(ZPaint.class, ZColor.LIGHTGRAY);
	
	private static final ZStyle<ZImage[]>
		IMAGE_ID = ZStyle.next(ZImage[].class, new ZImage[0]);
	
	private static final ZStyle<ImageAdapt>
		ADAPTH_ID = ZStyle.next(ImageAdapt.class, ImageAdapt.POSITION),
		ADAPTV_ID = ZStyle.next(ImageAdapt.class, ImageAdapt.POSITION);
	
//	>> ALL >>
	public static ZStyle<?>[] style()
	{
		return new ZStyle<?>[] { COLOR_ID, IMAGE_ID, ADAPTH_ID, ADAPTV_ID };
	}
	public static ZStyleSet style(ZColor color, ZImage... images)
	{
		return new ZStyleSet(color(color), image(images));
	}
	public static ZStyleSet style(ZColor color, ImageAdapt adaption, ZImage... images)
	{
		return new ZStyleSet(color(color), verAdapt(adaption), horAdapt(adaption), image(images));
	}
	public static ZStyleSet style(ImageAdapt adaption, ZImage... images)
	{
		return new ZStyleSet(verAdapt(adaption), horAdapt(adaption), image(images));
	}
//	<< ALL <<
	
//	>> ADAPT >>
	public static ZStyle<ImageAdapt>[] adapt()
	{
		@SuppressWarnings("unchecked") ZStyle<ImageAdapt>[] array = (ZStyle<ImageAdapt>[]) new ZStyle<?>[] { horAdapt(), verAdapt() };
		return array;
	}
	public static ZStyleSet adapt(ImageAdapt adaption)
	{
		return adapt(adaption, adaption);
	}
	public static ZStyleSet adapt(ImageAdapt h, ImageAdapt v)
	{
		return new ZStyleSet(horAdapt(h), verAdapt(v));
	}
	public static ZStyle<ImageAdapt> horAdapt()
	{
		return ADAPTH_ID;
	}
	public static ZStyleSet horAdapt(ImageAdapt adaption)
	{
		return new ZStyleSet(new ZRestyle<ImageAdapt>(horAdapt(), adaption));
	}
	public static ZStyle<ImageAdapt> verAdapt()
	{
		return ADAPTV_ID;
	}
	public static ZStyleSet verAdapt(ImageAdapt adaption)
	{
		return new ZStyleSet(new ZRestyle<ImageAdapt>(verAdapt(), adaption));
	}
//	<< ADAPT <<
	
//	>> IMAGE >>
	public static ZStyle<ZImage[]> image()
	{
		return IMAGE_ID;
	}
	public static ZStyleSet image(ZImage... images)
	{
		return new ZStyleSet(new ZRestyle<ZImage[]>(image(), Ray.copy(images)));
	}
//	<< IMAGE <<
	
//	>> COLOR >>
	public static ZStyle<ZPaint> color()
	{
		return COLOR_ID;
	}
	public static ZStyleSet color(int red, int green, int blue)
	{
		return color(new ZColor(red, green, blue));
	}
	public static ZStyleSet color(int red, int green, int blue, int alpha)
	{
		return color(new ZColor(red, green, blue, alpha));
	}
	public static ZStyleSet color(ZPaint color)
	{
		return new ZStyleSet(new ZRestyle<>(color(), color));
	}
//	<< COLOR <<
	
	protected Background() {}
}
