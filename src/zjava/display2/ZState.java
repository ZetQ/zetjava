package zjava.display2;

import zjava.ZObject;
import zjava.display2.elements.ZElement;

public abstract class ZState extends ZObject
{
	public static final ZState
		NONE = new ZState() 
		{
			@Override
			public boolean appliesTo(ZElement element)
			{
				return true;
			}
		},
		HOVER = new ZState()
		{
			@Override
			public boolean appliesTo(ZElement element)
			{
				return element.isHovered();
			}
		},
		CLICK = new ZState()
		{
			@Override
			public boolean appliesTo(ZElement element)
			{
				return element.isClicked();
			}
		},
		SELECT = new ZState()
		{
			@Override
			public boolean appliesTo(ZElement element)
			{
				if(element instanceof ZSelectable)
				{
					return ((ZSelectable)element).isSelected();
				}
				return false;
			}
		};
	
	public ZState() {}
	
	public abstract boolean appliesTo(ZElement element);
}
