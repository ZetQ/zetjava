package zjava.display2;

import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.PathIterator;

import zjava.ZObject;

abstract class ZStroke extends ZObject implements Stroke
{
	private final float
		size;
	
	private ZStroke(float size)
	{
		this.size = size;
	}
	
	public float getSize()
	{
		return this.size;
	}
	
	public static class Dotted extends ZStroke
	{
		public Dotted(float size)
		{
			super(size);
		}
		
		@Override
		public Shape createStrokedShape(Shape p)
		{
			Area area = new Area();
			PathIterator it = p.getPathIterator(new AffineTransform());
			while(!it.isDone())
			{
				
			}
		
			return area;}
	}
}
