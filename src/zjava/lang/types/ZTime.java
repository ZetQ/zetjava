package zjava.lang.types;

import zjava.ZObject;
import zjava.lang.tools.Copyable;
import zjava.lang.types.dec.TimeSetter;

public class ZTime extends ZObject implements TimeSetter, Copyable<ZTime>
{
	public static long getMillis()
	{
		return System.currentTimeMillis();
	}
	public static int toDays(long millis)
	{
		return (int) ((double) millis / 86400000);
	}
	public static int toHours(long millis)
	{
		return (int) ((double) millis / 3600000);
	}
	public static int toMinutes(long millis)
	{
		return (int)((double) millis / 60000);
	}
	public static int toSeconds(long millis)
	{
		return (int) ((double) millis / 1000);
	}
	public static int toMillis(long millis)
	{
		return (int) ((double) millis % 1000);
	}
	
	public ZTime(String time)
	{
		
	}
	public ZTime(long millis)
	{
		
	}
	public ZTime(int hours, int minutes)
	{
		
	}
	public ZTime(int hours, int minutes, int seconds)
	{
		
	}
	public ZTime(int hours, int minutes, int seconds, int millis)
	{
		
	}
	
	@Override
	public ZTime copy()
	{
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setHours(int hours) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setMinutes(int minutes) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setSeconds(int seconds) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setMillis(int millis) {
		// TODO Auto-generated method stub
		
	}
	
}
