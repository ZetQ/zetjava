package zjava.lang.types;

import zjava.ZObject;
import zjava.auto.annotations.Attribute;
import zjava.lang.tools.Copyable;
import zjava.lang.types.dec.PointAdder;
import zjava.lang.types.dec.PointGetter;
import zjava.lang.utils.DoubleMath;

public class ZPoint extends ZObject implements PointAdder, Copyable<ZPoint>
{
	@Attribute
	private int 
		x;
	
	@Attribute
	private int
		y;

//	>> CONSTRUCTORS >>
	public ZPoint()
	{
		this(0, 0);
	}
	public ZPoint(java.awt.geom.Point2D location)
	{
		this((int) location.getX(), (int) location.getY());
	}
	public ZPoint(PointGetter size)
	{
		this(size.getX(), size.getY());
	}
	public ZPoint(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
//	<< CONSTRUCTORS <<
	
//	>> MISC >>
	public double distance(ZPoint location)
	{
		return this.distance(location.getX(), location.getY());
	}
	public double distance(int x, int y)
	{
		int leg1 = x - this.getX();
		int leg2 = y - this.getY();
		return DoubleMath.pythagoras(leg1, leg2);
	}
//	<< MISC <<
	
//	>> X >>
	@Override
	public int getX()
	{
		return this.x;
	}
	@Override
	public void setX(int x)
	{
		this.x = x;
	}
//	<< X <<
	
//	>> Y >>
	@Override
	public int getY()
	{
		return this.y;
	}
	@Override
	public void setY(int y)
	{
		this.y = y;
	}
//	<< Y <<
	
//	>> TRANSFORM >>
	public java.awt.Point toAWTPoint()
	{
		return new java.awt.Point(this.getX(), this.getY());
	}
//	<< TRANSFORM <<
	
//	>> IMPLEMENTATIONS >>
	@Override
	public ZPoint copy()
	{
		return new ZPoint(this);
	}
//	<< IMPLEMENTATIONS <<
}
