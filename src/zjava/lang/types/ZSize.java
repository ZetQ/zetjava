package zjava.lang.types;

import zjava.ZObject;
import zjava.auto.annotations.Attribute;
import zjava.lang.tools.Copyable;
import zjava.lang.types.dec.SizeAdder;
import zjava.lang.types.dec.SizeGetter;

public class ZSize extends ZObject implements SizeAdder, Copyable<ZSize>
{
	@Attribute
	private int
		width,
		height;
	
//	>> CONSTRUCTORS >>
	public ZSize()
	{
		this(0, 0);
	}
	public ZSize(java.awt.geom.Dimension2D size)
	{
		this((int) size.getWidth(), (int) size.getHeight());
	}
	public ZSize(SizeGetter size)
	{
		this(size.getWidth(), size.getHeight());
	}
	public ZSize(int width, int height)
	{
		this.width = width;
		this.height = height;
	}
//	<< CONSTRUCTORS <<
	
//	>> WIDTH >>
	@Override
	public int getWidth() 
	{
		return this.width;
	}
	@Override
	public void setWidth(int width) 
	{
		this.width = width;
	}
//	<< WIDTH <<
	
//	>> HEIGHT >>
	@Override
	public int getHeight() 
	{
		return this.height;
	}
	@Override
	public void setHeight(int height) 
	{
		this.height = height;
	}
//	<< HEIGHT <<
	
//	>> TRANSFORM >>
	public java.awt.Dimension toAWTDimension()
	{
		return new java.awt.Dimension(this.getWidth(), this.getHeight());
	}
//	<< TRANSFORM <<
	
//	>> IMPLEMENTATIONS >>
	@Override
	public ZSize copy()
	{
		return new ZSize(this);
	}
//	<< IMPLEMENTATIONS <<
}
