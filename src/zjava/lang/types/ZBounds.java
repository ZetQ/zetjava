package zjava.lang.types;

import java.awt.Rectangle;

import zjava.ZObject;
import zjava.auto.annotations.Attribute;
import zjava.auto.annotations.Testing;
import zjava.lang.tools.Copyable;
import zjava.lang.types.dec.BoundsAdder;
import zjava.lang.types.dec.BoundsGetter;
import zjava.lang.types.dec.PointGetter;
import zjava.lang.types.dec.SizeGetter;

public class ZBounds extends ZObject implements BoundsAdder, Copyable<ZBounds>
{
	@Attribute
	private int 
		x, 
		y,
		width,
		height;

//	>> CONSTRUCTORS >>
	public ZBounds()
	{
		this.x = 0;
		this.y = 0;
		this.width = 0;
		this.height = 0;
	}
	public ZBounds(Rectangle bounds)
	{
		this((int)bounds.getX(), (int)bounds.getY(), (int)bounds.getWidth(), (int)bounds.getHeight());
	}
	public ZBounds(BoundsGetter bounds)
	{
		this.setBounds(bounds);
	}
	public ZBounds(PointGetter location, SizeGetter size)
	{
		this.setBounds(location, size);
	}
	public ZBounds(int x, int y, int width, int height)
	{
		this.setBounds(x, y, width, height);
	}
//	<< CONSTRUCTORS <<
	
//	>> MISC >>
	public boolean contains(PointGetter location)
	{
		return this.contains(location.getX(), location.getY());
	}
	@Testing
	public boolean contains(int x, int y)
	{
		return this.getX() <= x && this.getY() <= y && this.getEndX() >= x && this.getEndY() >= y;
	}
	@Testing
	public boolean contains(ZBounds bounds)
	{
		int x_width = this.getX() + this.getWidth();
		int y_height = this.getY() + this.getHeight();
		
		if(bounds.getX() < x_width && bounds.getY() < y_height)
		{
			if(bounds.getX() + bounds.getWidth() > this.getX() && bounds.getY() + bounds.getHeight() > this.getY())
			{
				return true;
			}
		}
		return false;
	}
	@Testing
	public boolean intersects(ZBounds bounds)
	{
		return this.contains(bounds) || this.contains(bounds.getEndPoint()) || this.contains(this.getX(), this.getEndY()) || this.contains(this.getEndY(), this.getEndX());
	}
	public int getEndX()
	{
		return this.getX() + this.getWidth();
	}
	public int getEndY()
	{
		return this.getY() + this.getHeight();
	}
	public ZPoint getEndPoint()
	{
		return new ZPoint(this.getEndX(), this.getEndY());
	}
//	<< MISC
	
//	>> X >>
	@Override
	public int getX()
	{
		return this.x;
	}
	@Override
	public void setX(int x)
	{
		this.x = x;
	}
//	<< X <<
	
//	>> Y >>
	@Override
	public int getY()
	{
		return this.y;
	}
	@Override
	public void setY(int y)
	{
		this.y = y;
	}
//	<< Y <<
	
//	>> WIDTH >>
	@Override
	public int getWidth()
	{
		return this.width;
	}
	@Override
	public void setWidth(int width)
	{
		this.width = width;
	}
//	<< WIDTH <<
	
//	>> HEIGHT >>
	@Override
	public int getHeight()
	{
		return this.height;
	}
	@Override
	public void setHeight(int height)
	{
		this.height = height;
	}
//	<< HEIGHT
	
//	>> TRANSFORM >>
	public Rectangle toAWTRectangle()
	{
		return new Rectangle(this.getX(), this.getY(), this.getWidth(), this.getHeight());
	}
//	<< TRANSFORM <<
	
//	>> IMPLEMENTATIONS >>
	@Override
	public ZBounds copy()
	{
		return new ZBounds(this);
	}
//	<< IMPLEMENTATIONS <<
}
