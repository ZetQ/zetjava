package zjava.lang.types.collections;

import zjava.lang.utils.Iterate;
import zjava.lang.utils.Ray;

public class Array<T> extends ASorted<T> implements IArray<T>
{
	private final Object[]
		array;
	
//	>> CONSTRUCTOR >>
	public Array(int size)
	{
		this.array = new Object[size];
	}
	public Array(int size, T defValue)
	{
		this.array = new Object[size];
		for(int i = 0; i < this.array.length; ++i) this.array[i] = defValue;
	}
	@SafeVarargs
	public Array(T... objects)
	{
		this.array = new Object[objects.length];
		for(int i = 0; i < objects.length; ++i) this.array[i] = objects[i];
	}
	public Array(ICollection<T> collection)
	{
		this.array = new Object[collection.getSize()];
		int i = 0; for(T object : collection)
		{
			this.array[i] = object;
		++i;}
	}
	public Array(Iterable<T> iterable)
	{
		this.array = new Object[Iterate.getSize(iterable)];
		int i = 0; for(T object : iterable)
		{
			this.array[i] = object;
		++i; }
	}
//	<< CONSTRUCTORS <<
	
//	>> PROTECTED SECTION >>
	protected Object[] useArray()
	{
		return this.array;
	}
//	<< PROTECTED SECTION <<
	
//	>> SIZE >>
	@Override
	public void clear()
	{
		for(int i = 0; i < this.getSize(); ++i) this.array[i] = null;
	}
//	<< SIZE <<
	
//	>> TRANSFORM >>
	public ZList<T> toList()
	{
		return new ZList<>(this);
	}
	@Override
	public T[] toArray(Class<T> type)
	{
		T[] array = Ray.create(type, this.getSize());
		for(int i = 0; i < this.getSize(); ++i)
		{
			array[i] = this.unsafeGet(i);
		}
		return array;
	}
//	<< TRANSFORM <<
	
//	>> IMPLEMENTATIONS >>
	@Override
	public Array<T> copy()
	{
		return new Array<>(this);
	}
//	<< IMPLEMENTATIONS <<
}
