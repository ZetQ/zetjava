package zjava.lang.types.collections;

public interface IArchiveEntry<K, V> extends IEntry<K, V>
{
//	>> GET >>
	public IArray<V> getVal();
//	<< GET <<
	
//	>> ADD >>
	public void addVal(V val);
//	<< ADD <<
	
//	>> IMPLEMENTATIONS >>
	public IArchiveEntry<K, V> copy();
//	<< IMPLEMENTATIONS <<
}
