package zjava.lang.types.collections;

import zjava.lang.functionals.Consumer;
import zjava.lang.functionals.Eval;

public interface ITable<K, V, T extends IEntry<K, V>> extends ICollection<T>
{
//	>> GET >>
	public ISorted<V> getWhere(Eval<T> eval);
	public ISorted<V> getWhereKey(Eval<K> eval);
	public ISorted<V> getWhereVal(Eval<V> eval);
//	<< GET <<
	
//	>> REMOVE >>
	public Object remove(K key);
	public ISorted<K> removeAll(V val);
	public ISorted<T> removeWhere(Eval<T> eval);
	public ISorted<V> removeWhereKey(Eval<K> eval);
	public ISorted<K> removeWhereVal(Eval<V> eval);
//	<< REMOVE <<
	
//	>> CONTAINS >>
	public boolean contains(K key, V val);
	public boolean containsKey(K key);
	public boolean containsVal(V val);
	public boolean containsWhere(Eval<T> eval);
	public boolean containsWhereKey(Eval<K> eval);
	public boolean containsWhereVal(Eval<V> eval);
//	<< CONTAINS <<
	
//	>> AMOUNT >>
	public int amountOf(V val);
	public int amountWhere(Eval<T> eval);
	public int amountWhereKey(Eval<K> eval);
	public int amountWhereVal(Eval<V> eval);
//	<< AMOUNT <<
	
//	>> EACH >>
	public void eachKey(Consumer<K> consumer);
//	<< EACH <<
	
//	>> TRANSFORM >>
	public java.util.Map<K, ?> toMap();
//	<< TRANSFORM <<
	
//	>> SUB >>
	public ISorted<K> subKeys();
	public ISorted<V> subVals();
//	<< SUB <<
	
//	>> ITERATE >>
	public Iterable<K> itKeys();
	public Iterable<V> itVals();
//	<< ITERATE <<
	
//	>> IMPLEMENTATIONS >>
	public ITable<K, V, T> copy();
//	<< IMPLEMENTATIONS <<
}
