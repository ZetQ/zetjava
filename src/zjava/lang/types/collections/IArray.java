package zjava.lang.types.collections;

import zjava.lang.functionals.Producer;
import zjava.lang.functionals.Eval;

public interface IArray<T> extends ISorted<T>
{
//	>> GET >>
	@Override public IArray<T> getRange(int index1, int index2);
	@Override public IArray<T> getWhere(Eval<T> eval);
	@Override public <P> IArray<P> getBy(Producer<T, P> producer);
	@Override public <P> IArray<P> getWhereBy(Eval<T> eval, Producer<T, P> producer);
//	<< GET <<
	
//	>> SET >>
	@Override public IArray<T> setWhere(T object, Eval<T> eval);
//	<< SET <<
	
//	>> CONTAINS >>
	@Override public boolean containsWhere(Eval<T> eval);
//	<< CONTAINS <<
	
//	>> TRANSFORM >>
	public IList<T> toList();
	public T[] toArray(Class<T> type);
//	<< TRANSFORM <<
	
//	>> IMPLEMENTATIONS >>
	@Override public IArray<T> copy();
//	<< IMPLEMENTATIONS <<
}
