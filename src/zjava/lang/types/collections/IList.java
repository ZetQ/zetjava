package zjava.lang.types.collections;

import zjava.lang.functionals.Producer;
import zjava.lang.functionals.Eval;

public interface IList<T> extends ISorted<T>
{
//	>> GET >>
	@Override public IArray<T> getRange(int index1, int index2);
	@Override public <P> IArray<P> getBy(Producer<T, P> producer);
	@Override public IArray<T> getWhere(Eval<T> eval);
	@Override public <P> IArray<P> getWhereBy(Eval<T> eval, Producer<T, P> producer);
//	<< GET <<
	
//	>> ADD >>
	public void add(T object);
	public void add(T[] array);
	public void add(Iterable<T> iterable);
//	<< ADD <<
	
//	>> SET >>
	@Override public IArray<T> setWhere(T object, Eval<T> eval);
//	<< SET <<
	
//	>> CONTAINS >>
	@Override public boolean containsWhere(Eval<T> eval);
//	<< CONTAINS <<
	
//	>> REMOVE >>
	public T remove(int index);
	public boolean remove(T object);
	public boolean removeWhere(Eval<T> eval);
//	<< REMOVE <<
	
//	>> SIZE >>
	public void setSize(int size);
//	<< SIZE <<
	
//	>> TRANSFORM >>
	public IArray<T> toArray();
	public java.util.List<T> toList();
//	<< TRANSFORM <<
	
//	>> IMPLEMENTATIONS >>
	@Override public IList<T> copy();
//	<< IMPLEMENTATIONS <<
}
