package zjava.lang.types.collections;

import zjava.ZObject;
import zjava.auto.annotations.Attribute;

public abstract class AEntry<K, V> extends ZObject implements IEntry<K, V>
{
	@Attribute
	private final K
		key;
	
	public AEntry(K key)
	{
		this.key = key;
	}

//	>> KEY >>
	@Override
	public K getKey()
	{
		return this.key;
	}
//	<< KEY <<
}
