package zjava.lang.types.collections;

import java.util.Iterator;

import zjava.ZObject;
import zjava.errors.ArgumentException;
import zjava.lang.functionals.Consumer;
import zjava.lang.functionals.Eval;
import zjava.lang.functionals.Producer;
import zjava.lang.utils.Iterate;

public abstract class ASorted<T> extends ZObject implements ISorted<T>
{		
//	>> PROTECTED SECTION >>
	protected abstract Object[] useArray();
	protected T unsafeGet(int index)
	{
		if(index >= 0)
		{
			@SuppressWarnings("unchecked") T object = (T) this.useArray()[index];
			return object;
		}
		else
		{
			@SuppressWarnings("unchecked") T object = (T) this.useArray()[this.getSize() + index];
			return object;
		}
	}
	protected void checkIndex(int index)
	{
		if(this.isEmpty()) throw new ArgumentException("collection is empty");
		if(index >= this.getSize()) 
			throw new ArgumentException("index out of bounds: (max=" + (this.getSize() - 1) + ", index=" + index +")");
		if(index <= -this.getSize() - 1)
			throw new ArgumentException("index out of bounds: (min=" + (-this.getSize()) + ", index=" + index +")");
	}
//	<< PROTECTED SECTION <<
	
//	>> GET >>
	@Override
	public T get(int index)
	{
		this.checkIndex(index);
		return this.unsafeGet(index);
	}
	@Override
	public Array<T> getRange(int index1, int index2)
	{
		this.checkIndex(index1);
		this.checkIndex(index2);
		int start = index1 < index2 ? index1 : index2;
		int end = start == index1 ? index2 : index1;
	
		Array<T> array = new Array<>(end - start);
		for(int i = start; i <= end; ++i)  array.set(i - start, this.unsafeGet(i));
		return array;
	}
	@Override
	public Array<T> getWhere(Eval<T> eval)
	{
		ZList<T> list = new ZList<>();
		for(T current : this) if(eval.evaluate(current)) list.add(current);
		return list.toArray();
	}
	@Override
	public <P> Array<P> getBy(Producer<T, P> producer)
	{
		Array<P> array = new Array<>(this.getSize());
		for(int i = 0; i < this.getSize(); ++i) array.set(i, producer.produce(this.unsafeGet(i)));
		return array;
	}
	@Override
	public <P> Array<P> getWhereBy(Eval<T> eval, Producer<T, P> producer)
	{
		ZList<P> list = new ZList<>();
		for(T current : this) if(eval.evaluate(current)) list.add(producer.produce(current));
		return list.toArray();
	}
//	<< GET <<
	
//	>> SET >>
	@Override
	public T set(int index, T object)
	{
		this.checkIndex(index);
		T value = this.unsafeGet(index);
		this.useArray()[index] = object;
		return value;
	}
	@Override
	public Array<T> setWhere(T object, Eval<T> eval)
	{
		ZList<T> list = new ZList<>();
		for(int i = 0; i < this.getSize(); ++i) 
		{	
			T current = this.unsafeGet(i);
			if(eval.evaluate(current))
			{
				list.add(current);
				this.useArray()[i] = object;
			}
		}
		return list.toArray();
	}
//	<< SET <<
	
//	>> CONTAINS >>
	@Override
	public boolean contains(T object)
	{
		if(null == object) 
		{ 
			for(T current : this) if(null == current) return true; 
		}
		else 
		{
			for(T current : this) if(null != current && object.equals(current)) return true; 
		}
		return false;
	}
	
	@Override
	public boolean containsWhere(Eval<T> eval)
	{
		for(T current : this) if(eval.evaluate(current)) return true;
		return false;
	}
//	<< CONTAINS <<
	
//	>> AMOUNT >>
	@Override
	public int amountOf(T object)
	{
		int amount = 0;
		if(null == object)
		{
			for(T current : this) if(null == current) ++amount;
		}
		else
		{
			for(T current : this) if(null != current && object.equals(current)) ++amount;
		}
		return amount;
	}
	@Override
	public int amountWhere(Eval<T> eval)
	{
		notNull(eval);
		int amount = 0;
		for(T current : this) if(eval.evaluate(current)) ++amount;
		return amount;
	}
//	<< AMOUNT <<
	
//	>> INDEX >>
	@Override
	public int firstIndex(T object)
	{
		if(null == object)
		{
			for(int i = 0; i < this.getSize(); ++i) if(null == this.unsafeGet(i)) return i;
		}
		else
		{
			for(int i = 0; i < this.getSize(); ++i) if(null != this.unsafeGet(i) && object.equals(this.unsafeGet(i))) return i;
		}
		return -1;
	}
	@Override
	public int firstIndexWhere(Eval<T> eval)
	{
		for(int i = 0; i < this.getSize(); ++i) if(eval.evaluate(this.unsafeGet(i))) return i;
		return -1;
	}
	@Override
	public int lastIndex(T object)
	{
		if(null == object)
		{
			for(int i = this.getSize() -1; i >= 0; --i) if(null == this.unsafeGet(i)) return i;
		}
		else
		{
			for(int i = this.getSize() -1; i >= 0; --i) if(null != this.unsafeGet(i) && object.equals(this.unsafeGet(i))) return i;
		}
		return -1;
	}
	@Override
	public int lastIndexWhere(Eval<T> eval)
	{
		for(int i = this.getSize() -1; i >= 0; --i) if(eval.evaluate(this.unsafeGet(i))) return i;
		return -1;
	}
//	<< INDEX <<
	
//	>> SIZE >>
	@Override
	public int getSize()
	{
		return this.useArray().length;
	}
//	<< SIZE <<
	
//	>> EACH >>
	@Override
	public void each(Consumer<T> consumer)
	{
		for(T current : this) consumer.consume(current);
	}
//	<< EACH <<
	
//	>> IMPLEMENTATIONS >>
	@Override
	public Iterator<T> iterator()
	{
		return Iterate.create(this::getSize, this::get);
	}
//	<< IMPLEMENTATIONS <<

}
