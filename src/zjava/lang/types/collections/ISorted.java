package zjava.lang.types.collections;

import zjava.lang.functionals.Eval;
import zjava.lang.functionals.Producer;

public interface ISorted<T> extends ICollection<T>
{
//	>> GET >>
	public T get(int index);
	public ISorted<T> getRange(int index1, int index2);
	public ISorted<T> getWhere(Eval<T> eval);
	public <P> ISorted<P> getBy(Producer<T, P> producer);
	public <P> ISorted<P> getWhereBy(Eval<T> eval, Producer<T, P> producer);
//	<< GET <<
	
//	>> SET >>
	public T set(int index, T object);
	public ISorted<T> setWhere(T object, Eval<T> eval);
//	<< SET <<
	
//	>> CONTAINS >>
	public boolean contains(T object);
	public boolean containsWhere(Eval<T> eval);
//	<< CONTAINS <<
	
//	>> AMOUNT >>
	public int amountOf(T object);
	public int amountWhere(Eval<T> eval);
//	<< AMOUNT <<
	
//	>> INDEX >>
	public int firstIndex(T object);
	public int firstIndexWhere(Eval<T> eval);
	public int lastIndex(T object);
	public int lastIndexWhere(Eval<T> eval);
//	<< INDEX <<
	
//	>> IMPLEMENTATIONS >>
	@Override public ISorted<T> copy();
//	<< IMPLEMENTATIONS <<
}
