package zjava.lang.types.collections;

import zjava.lang.functionals.Consumer;
import zjava.lang.tools.Copyable;

public interface ICollection<T> extends Iterable<T>, Copyable<ICollection<T>>
{
//	>> EACH >>
	public void each(Consumer<T> consumer);
//	<< EACH <<
	
//	>> SIZE >>
	public int getSize();
	public default boolean isEmpty()
	{
		return this.getSize() < 1;
	}
	public void clear();
//	<< SIZE <<
}
