package zjava.lang.types.collections;

import zjava.lang.functionals.Consumer;
import zjava.lang.functionals.Eval;

public interface IMap<K, V, T extends IMapEntry<K, V>> extends ITable<K, V, T>
{
//	>> GET >>
	public V get(K key);
	public V getOr(K key, V or);
	@Override public IArray<V> getWhere(Eval<T> eval);
	@Override public IArray<V> getWhereKey(Eval<K> eval);
	@Override public IArray<V> getWhereVal(Eval<V> eval);
//	<< GET <<
	
//	>> SET >>
	public V set(K key, V val);
	public void setWhere(V val, Eval<T> eval);
	public void setWhereKey(V val, Eval<K> eval);
	public void setWhereVal(V val, Eval<V> eval);
//	<< SET <<
	
//	>> REMOVE >>
	@Override public V remove(K key);
	@Override public IArray<K> removeAll(V val);
	@Override public IArray<T> removeWhere(Eval<T> eval);
	@Override public IArray<V> removeWhereKey(Eval<K> eval);
	@Override public IArray<K> removeWhereVal(Eval<V> eval);
//	<< REMOVE <<
	
//	>> EACH >>
	public void eachVal(Consumer<V> consumer);
//	<< EACH <<
	
//	>> SUB >>
	@Override public IArray<K> subKeys();
	@Override public IArray<V> subVals();
//	<< SUB <<
	
//	>> TRANSFORM >>
	@Override public java.util.Map<K, V> toMap();
//	<< TRANSFORM <<
	
//	>> IMPLEMENTATIONS >>
	@Override public IMap<K, V, T> copy();
//	<< IMPLEMENTATIONS <<
}
