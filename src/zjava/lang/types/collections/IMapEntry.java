package zjava.lang.types.collections;

public interface IMapEntry<K, V> extends IEntry<K, V>
{
//	>> GET >>
	public V getVal();
//	<< GET <<
	
//	>> SET >>
	public V setVal(V val);
//	<< SET <<
	
//	>> IMPLEMENTATIONS >>
	public IMapEntry<K, V> copy();
//	<< IMPLEMENTATIONS <<
}
