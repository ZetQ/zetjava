package zjava.lang.types.collections;

import java.util.ArrayList;

import zjava.errors.ArgumentException;
import zjava.lang.functionals.Eval;
import zjava.lang.utils.Iterate;
import zjava.lang.utils.Ray;

public class ZList<T> extends ASorted<T> implements IList<T>
{
	private Object[]
		array;
	
//	>> CONSTRUCTORS >>
	public ZList()
	{
		this.array = this.create(0);
	}
	@SafeVarargs
	public ZList(T... objects)
	{
		this.array = new Object[objects.length];
		for(int i = 0; i < objects.length; ++i) this.array[i] = objects[i];
	}
	public ZList(ICollection<T> collection)
	{
		this.array = new Object[collection.getSize()];
		int i = 0; for(T object : collection)
		{
			this.array[i] = object;
		++i;}
	}
	public ZList(Iterable<T> iterable)
	{
		this.array = this.create(Iterate.getSize(iterable));
		int i = 0; for(T object : iterable)
		{
			this.array[i] = object;
		++i;}
	}
//	<< CONSTRUCTORS <<

//	>> PRIVATE SECTION >>
	private Object[] create(int size)
	{
		return new Object[size];
	}
	private void modifyArrayBy(int amount)
	{
		this.array = Ray.copy(this.array, this.getSize() + amount);
	}
	private void removeIndex(int index)
	{
		Object[] array = this.create(this.array.length - 1);
		int next = 0;
		for (int i = 0; i < this.array.length; ++i)
		{
			if (i != index)
			{
				array[next] = this.array[i];
				++next;
			}
		}
		this.array = array;
	}
//	<< PRIVATE SECTION <<
	
//	>> PROTECTED SECTION >>
	@Override
	protected Object[] useArray()
	{
		return this.array;
	}
//	<< PROTECTED SECTION <<
	
//	>> ADD >>
	@Override
	public void add(T object)
	{
		this.modifyArrayBy(1);
		this.array[this.array.length - 1] = object;
	}
	@Override
	public void add(T[] array) 
	{
		for(T object : array)
		{
			this.add(object);
		}
	}
	@Override
	public void add(Iterable<T> iterable)
	{
		for(T object : iterable) this.add(object);
	}
//	<< ADD <<
	
//	>> REMOVE >>
	@Override
	public T remove(int index)
	{
		this.checkIndex(index);
		@SuppressWarnings("unchecked") T value = (T) this.array[index];
		this.removeIndex(index);
		return value;
	}
	@Override
	public boolean remove(T object)
	{
		boolean removed = false;
		if(null == object)
		{
			for(int i = this.getSize() - 1; i >= 0; --i) if(null == this.array[i]) 
			{
				this.remove(i);
				removed = true;
			}
		}
		else
		{
			for(int i = this.getSize() - 1; i >= 0; --i) if(null != this.array[i] && object.equals(this.array[i])) 
			{
				this.remove(i);
				removed = true;
			}
		}
		return removed;
	}
	@Override
	public boolean removeWhere(Eval<T> eval)
	{
		boolean removed = false;
		for(int i = this.getSize() - 1; i >= 0; --i) if(eval.evaluate(this.unsafeGet(i)))
		{
			this.remove(i);
			removed = true;
		}
		return removed;
	}
//	<< REMOVE <<
	
//	>> SIZE >>
	@Override
	public void clear()
	{
		for(int i = 0; i < this.getSize(); ++i) this.array[i] = null;
	}
	@Override
	public void setSize(int size)
	{
		if(size < 0) throw new ArgumentException("negative size: " + size);
		Ray.copy(this.array, size);
	}
//	<< SIZE <<
	
//	>> TRANSFORM >>
	@Override
	public Array<T> toArray()
	{
		return new Array<>(this);
	}
	@Override
	public java.util.List<T> toList()
	{
		ArrayList<T> list = new ArrayList<>(this.getSize());
		for(int i = 0; i < this.getSize(); ++i) list.set(i, this.unsafeGet(i));
		return list;
	}
//	<< TRANSFORM <<
	
//	>> IMPLEMENTATIONS >>
	@Override
	public ZList<T> copy()
	{
		return new ZList<>(this);
	}
//	<< IMPLEMENTATIONS <<
}
