package zjava.lang.types.collections;

import zjava.lang.tools.Copyable;

public interface IEntry<K, V> extends Copyable<IEntry<K, V>>
{
//	>> GET >>
	public K getKey();
//	<< GET <<
}
