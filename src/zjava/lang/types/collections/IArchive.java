package zjava.lang.types.collections;

import zjava.lang.functionals.Consumer;
import zjava.lang.functionals.Eval;

public interface IArchive<K, V, T extends IArchiveEntry<K, V>> extends ITable<K, V, T>
{
//	>> GET >>
	public IArray<V> get(K key);
	@Override public IArray<V> getWhere(Eval<T> eval);
	@Override public IArray<V> getWhereKey(Eval<K> eval);
	@Override public IArray<V> getWhereVal(Eval<V> eval);
//	<< GET <<
	
//	>> ADD >>
	public void add(K key, V val);
//	<< ADD <<
	
//	>> REMOVE >>
	@Override public IArray<V> remove(K key);
	@Override public IArray<K> removeAll(V val);
	@Override public IArray<T> removeWhere(Eval<T> eval);
	@Override public IArray<V> removeWhereKey(Eval<K> eval);
	@Override public IArray<K> removeWhereVal(Eval<V> eval);
	public boolean remove(K key, V val);
//	<< REMOVE <<
	
//	>> AMOUNT >>
	public int amountIn(K key);
	public int amountOf(K key, V val);
//	<< AMOUNT <<
	
//	>> EACH >>
	public void eachVal(K key, Consumer<V> consumer);
//	<< EACH <<
	
//	>> ITERATE >>
	public Iterable<V> itVals(K key);
//	<< ITERATE <<
	
//	>> SUB >>
	@Override public IArray<K> subKeys();
	@Override public IArray<V> subVals();
	public IArray<V> subVals(K key);
//	<< SUB <<
	
//	>> TRANSFORM >>
	@Override public java.util.Map<K, java.util.List<V>> toMap();
//	<< TRANSFORM <<
	
//	>> IMPLEMENTATIONS >>
	@Override public IArchive<K, V, T> copy();
//	<< IMPLEMENTATIONS <<
}
