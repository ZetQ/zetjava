package zjava.lang.types.collections;

import zjava.auto.annotations.Attribute;

public class MapEntry<K, V> extends AEntry<K, V> implements IMapEntry<K, V>
{	
	@Attribute
	private V
		val;
	
//	>> CONSTRUCTOR >>
	public MapEntry(K key, V val)
	{
		super(key);
		this.val = val;
	}
//	<< CONSTRUCTORS <<

//	>> VAL >>
	@Override
	public V getVal()
	{
		return this.val;
	}
	@Override
	public V setVal(V val)
	{
		V current = this.getVal();
		this.val = val;
		return current;
	}
//	<< VAL <<

//	>> IMPLEMENTATIONS >>
	@Override
	public MapEntry<K, V> copy()
	{
		return new MapEntry<>(this.getKey(), this.getVal());
	}
//	<< IMPLEMENTATIONS <<
}
