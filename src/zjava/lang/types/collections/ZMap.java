package zjava.lang.types.collections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import zjava.ZObject;
import zjava.lang.functionals.Consumer;
import zjava.lang.functionals.Eval;
import zjava.lang.utils.Iterate;

public class ZMap<K, V> extends ZObject implements IMap<K, V, MapEntry<K, V>>
{
	private final ZList<MapEntry<K, V>>
		entries;
	
//	>> CONSTRUCTORS >>
	public ZMap()
	{
		this.entries = new ZList<>();
	}
	public ZMap(java.util.Map<K, V> map)
	{
		this.entries = new ZList<>();
		for(Entry<K, V> entry : map.entrySet())
		{
			this.entries.add(new MapEntry<>(entry.getKey(), entry.getValue()));
		}
	}
//	<< CONSTRUCTORS <<
	
//	>> PRIVATE SECTION >>
	private MapEntry<K, V> getEntry(K key)
	{
		if(null == key)
		{
			for(MapEntry<K, V> entry : this.entries)
			{
				if(null == entry.getKey()) return entry;
			}
		}
		else
		{
			for(MapEntry<K, V> entry : this.entries)
			{
				if(null != entry.getKey() && key.equals(entry.getKey())) return entry;
			}
		}
		return null;
	}
//	<< PRIVATE SECTION <<
	
//	>> GET >>
	@Override
	public V get(K key)
	{
		return this.getOr(key, null);
	}
	@Override
	public V getOr(K key, V or)
	{
		MapEntry<K, V> entry = this.getEntry(key);
		return null == entry ? or : entry.getVal();
	}
	@Override 
	public Array<V> getWhere(Eval<MapEntry<K, V>> eval)
	{
		ZList<V> list = new ZList<>();
		for(MapEntry<K, V> entry : this.entries) 
			if(eval.evaluate(entry)) list.add(entry.getVal());
		return list.toArray();
	}
	@Override 
	public Array<V> getWhereKey(Eval<K> eval)
	{
		ZList<V> list = new ZList<>();
		for(MapEntry<K, V> entry : this.entries) 
			if(eval.evaluate(entry.getKey())) list.add(entry.getVal());
		return list.toArray();
	}
	@Override 
	public Array<V> getWhereVal(Eval<V> eval)
	{
		ZList<V> list = new ZList<>();
		for(MapEntry<K, V> entry : this.entries) 
			if(eval.evaluate(entry.getVal())) list.add(entry.getVal());
		return list.toArray();
	}
//	<< GET <<
	
//	>> SET >>
	@Override 
	public V set(K key, V val)
	{
		MapEntry<K, V> entry = this.getEntry(key);
		if(null == entry) 
		{
			this.entries.add(new MapEntry<>(key, val));
			return null;
		}
		else return entry.setVal(val);
	}
	@Override 
	public void setWhere(V val, Eval<MapEntry<K, V>> eval)
	{
		for(MapEntry<K, V> entry : this.entries)
			if(eval.evaluate(entry))
				entry.setVal(val);
	}
	@Override 
	public void setWhereKey(V val, Eval<K> eval)
	{
		for(MapEntry<K, V> entry : this.entries)
			if(eval.evaluate(entry.getKey()))
				entry.setVal(val);
	}
	@Override 
	public void setWhereVal(V val, Eval<V> eval)
	{
		for(MapEntry<K, V> entry : this.entries)
			if(eval.evaluate(entry.getVal()))
				entry.setVal(val);
	}
//	<< SET <<
	
//	>> REMOVE >>
	@Override 
	public V remove(K key)
	{
		if(null == key)
		{
			for(int i = 0; i < this.getSize(); ++i)
				if(null == this.entries.get(i).getKey())
					return this.entries.remove(i).getVal();
		}
		else
		{
			for(int i = 0; i < this.getSize(); ++i)
			{
				MapEntry<K, V> entry = this.entries.get(i);
				if(null != entry.getKey() && key.equals(entry.getKey()))
					return this.entries.remove(i).getVal();
			}
		}
		return null;
	}
	@Override 
	public Array<K> removeAll(V val)
	{
		ZList<K> list = new ZList<>();
		if(null == val)
		{
			for(int i = this.getSize() - 1; i <= 0; --i)
			{
				MapEntry<K, V> entry = this.entries.get(i);
				if(null == entry.getVal())
				{
					list.add(entry.getKey());
					this.entries.remove(i);
				}
			}
		}
		else
		{
			for(int i = this.getSize() - 1; i <= 0; --i)
			{
				MapEntry<K, V> entry = this.entries.get(i);
				if(null != entry.getVal() && val.equals(entry.getVal()))
				{
					list.add(entry.getKey());
					this.entries.remove(i);
				}
			}
		}
		return list.toArray();
	}
	@Override 
	public Array<MapEntry<K, V>> removeWhere(Eval<MapEntry<K, V>> eval)
	{
		ZList<MapEntry<K, V>> list = new ZList<>();
		for(int i = this.getSize() - 1; i <= 0; --i)
		{
			MapEntry<K, V> entry = this.entries.get(i);
			if(eval.evaluate(entry))
			{
				list.add(entry);
				this.entries.remove(i);
			}
		}
		return list.toArray();
	}
	@Override 
	public Array<V> removeWhereKey(Eval<K> eval)
	{
		ZList<V> list = new ZList<>();
		for(int i = this.getSize() - 1; i <= 0; --i)
		{
			MapEntry<K, V> entry = this.entries.get(i);
			if(eval.evaluate(entry.getKey()))
			{
				list.add(entry.getVal());
				this.entries.remove(i);
			}
		}
		return list.toArray();
	}
	@Override 
	public Array<K> removeWhereVal(Eval<V> eval)
	{
		ZList<K> list = new ZList<>();
		for(int i = this.getSize() - 1; i <= 0; --i)
		{
			MapEntry<K, V> entry = this.entries.get(i);
			if(eval.evaluate(entry.getVal()))
			{
				list.add(entry.getKey());
				this.entries.remove(i);
			}
		}
		return list.toArray();
	}
//	<< REMOVE <<
	
//	>> CONTAINS >>
	@Override
	public boolean contains(K key, V val)
	{
		MapEntry<K, V> entry = this.getEntry(key);
		return null == entry ? false : null == val ? null == entry.getVal() : val.equals(entry.getVal());
	}
	@Override
	public boolean containsKey(K key)
	{
		return null != this.getEntry(key);
	}
	@Override
	public boolean containsVal(V val)
	{
		if(null == val)
		{
			for(MapEntry<K, V> entry : this.entries)
				if(null == entry.getVal())
					return true;
		}
		else
		{
			for(MapEntry<K, V> entry : this.entries)
				if(null != entry.getVal() && val.equals(entry.getVal()))
					return true;
		}
		return false;
	}
	@Override
	public boolean containsWhere(Eval<MapEntry<K, V>> eval)
	{
		for(MapEntry<K, V> entry : this.entries)
			if(eval.evaluate(entry))
				return true;
		return false;
	}
	@Override
	public boolean containsWhereKey(Eval<K> eval)
	{
		for(MapEntry<K, V> entry : this.entries)
			if(eval.evaluate(entry.getKey()))
				return true;
		return false;
	}
	@Override
	public boolean containsWhereVal(Eval<V> eval)
	{
		for(MapEntry<K, V> entry : this.entries)
			if(eval.evaluate(entry.getVal()))
				return true;
		return false;
	}
//	<< CONTAINS <<
	
//	>> AMOUNT >>
	@Override
	public int amountOf(V val)
	{
		int amount = 0;
		if(null == val)
		{
			for(MapEntry<K, V> entry : this.entries)
				if(null == entry.getVal())
					++amount;
		}
		else
		{
			for(MapEntry<K, V> entry : this.entries)
				if(null != entry.getVal() && val.equals(entry.getVal()))
					++amount;
		}
		return amount;
	}
	@Override
	public int amountWhere(Eval<MapEntry<K, V>> eval)
	{
		int amount = 0;
		for(MapEntry<K, V> entry : this.entries)
			if(eval.evaluate(entry))
				++amount;
		return amount;
	}
	@Override
	public int amountWhereKey(Eval<K> eval)
	{
		int amount = 0;
		for(MapEntry<K, V> entry : this.entries)
			if(eval.evaluate(entry.getKey()))
				++amount;
		return amount;
	}
	@Override
	public int amountWhereVal(Eval<V> eval)
	{
		int amount = 0;
		for(MapEntry<K, V> entry : this.entries)
			if(eval.evaluate(entry.getVal()))
				++amount;
		return amount;
	}
//	<< AMOUNT <<
	
//	>> EACH >>
	@Override
	public void each(Consumer<MapEntry<K, V>> consumer)
	{
		this.entries.each(consumer);
	}
	@Override
	public void eachKey(Consumer<K> consumer)
	{
		for(MapEntry<K, V> entry : this.entries) consumer.consume(entry.getKey());
	}
	@Override
	public void eachVal(Consumer<V> consumer)
	{
		for(MapEntry<K, V> entry : this.entries) consumer.consume(entry.getVal());
	}
//	<< EACH <<
	
//	>> SIZE >>
	@Override
	public int getSize()
	{
		return this.entries.getSize();
	}
	@Override
	public void clear()
	{
		this.entries.clear();
	}
//	<< SIZE <<
	
//	>> ITERATE >>
	@Override
	public Iterator<MapEntry<K, V>> iterator()
	{
		return this.entries.iterator();
	}
	@Override
	public Iterable<K> itKeys()
	{
		return () -> Iterate.create(this::getSize, i -> this.entries.get(i).getKey());
	}
	@Override
	public Iterable<V> itVals()
	{
		return () -> Iterate.create(this::getSize, i -> this.entries.get(i).getVal());
	}
//	<< ITERATE <<
	
//	>> SUB >>
	@Override 
	public Array<K> subKeys()
	{
		return this.entries.getBy(entry -> entry.getKey());
	}
	@Override public Array<V> subVals()
	{
		return this.entries.getBy(entry -> entry.getVal());
	}
//	<< SUB <<
	
//	>> TRANSFORM >>
	@Override 
	public java.util.Map<K, V> toMap()
	{
		java.util.Map<K, V> map = new HashMap<>();
		for(MapEntry<K, V> entry : this.entries) map.put(entry.getKey(), entry.getVal());
		return map;
	}
//	<< TRANSFORM <<
	
//	>> IMPLEMENTATIONS >>
	@Override
	public ZMap<K, V> copy()
	{
		ZMap<K, V> map = new ZMap<>();
		for(MapEntry<K, V> entry : this.entries)
		{
			map.set(entry.getKey(), entry.getVal());
		}
		return map;
	}
//	<< IMPLEMENTATIONS <<
}
