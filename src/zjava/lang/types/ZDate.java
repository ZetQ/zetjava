package zjava.lang.types;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import zjava.ZObject;
import zjava.auto.annotations.Attribute;
import zjava.errors.ArgumentException;
import zjava.errors.UnexpectedException;
import zjava.lang.tools.Copyable;
import zjava.lang.types.dec.DateSetter;
import zjava.lang.types.dec.TimeGetter;
import zjava.lang.utils.ZString;

public class ZDate extends ZObject implements DateSetter, Copyable<ZDate>, TimeGetter
{
	private static final String
		REGEX_DELIMITER = "[^A-Za-z0-9]",
		REGEX_ENCLOSE = "",
		REGEX_DMY = REGEX_ENCLOSE + "\\d?\\d(" + REGEX_DELIMITER +")\\d?\\d\\1(\\d\\d)?\\d\\d" + REGEX_ENCLOSE,
		REGEX_YMD = REGEX_ENCLOSE + "\\d\\d\\d\\d(" + REGEX_DELIMITER +")\\d?\\d\\1\\d?\\d" + REGEX_ENCLOSE;
	
	public static int getCurrYear()
	{
		return Calendar.getInstance().get(Calendar.YEAR);
	}
	public static boolean isDMYDate(String date)
	{
		return ZString.doesMatch(date, REGEX_DMY);
	}
	public static boolean isYMDDate(String date)
	{
		return ZString.doesMatch(date, REGEX_YMD);
	}
	public static boolean isDate(String date)
	{
		return isDMYDate(date) || isYMDDate(date);
	}
	
	@Attribute
	private int
		day,
		month,
		year;
	
	public ZDate(String date)
	{
		this.setDate(date);
	}
	public ZDate(int day, int month, int year)
	{
		this.setDate(day, month, year);
	}

//	>> IMPLEMENTATION >>
	@Override
	public int getYear()
	{
		return this.year;
	}
	@Override
	public void setYear(int year)
	{
		this.year = year;
	}
	@Override
	public int getMonth()
	{
		return this.month;
	}
	@Override
	public int getDay()
	{
		return this.day;
	}
	@Override
	public void setMonth(int month)
	{
		if(month > 0 && month < 13)
		{
			this.month = month;
		}
		else
		{
			throw new ArgumentException("the int " + month + " is out of range for the month");
		}
	}
	@Override
	public void setDay(int day)
	{
		if(day > 0 && this.getDaysOfMonth() >= day)
		{
			this.day = day;
		}
		else
		{
			throw new ArgumentException("the int " + day + " is out of range for the day");
		}
	}
	@Override
	public ZDate copy()
	{
		return new ZDate(this.day, this.month, this.day);
	}
//	<< IMPLEMENTATION <<
	
	
//	>> TRANSFORM >>
	public java.util.Date toUtilDate()
	{
		try 
		{
            return new SimpleDateFormat("dd MM yyyy").parse(this.getDay() + " " + this.getMonth() + " " + this.getYear());
        } 
		catch (ParseException e) 
		{
            throw new UnexpectedException("the Date " + this + " couldn't be parsed to a UtilDate", e);
        }
	}
	public void toCalendar(Calendar calendar)
	{
		calendar.set(this.getYear(), this.getMonth(), this.getDay());
	}
//	<< TRANSFORM <<
}
