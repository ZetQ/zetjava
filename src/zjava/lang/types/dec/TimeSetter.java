package zjava.lang.types.dec;

import zjava.lang.types.ZTime;

public interface TimeSetter extends TimeGetter
{
	public void setHours(int hours);
	public void setMinutes(int minutes);
	public void setSeconds(int seconds);
	public void setMillis(int millis);
	
	public default void setTime(long millis)
	{
		this.setHours(ZTime.toHours(millis));
		this.setMinutes(ZTime.toMinutes(millis));
		this.setSeconds(ZTime.toSeconds(millis));
		this.setMillis(ZTime.toMillis(millis));
	}
	public default void setTime(int hours, int minutes, int seconds, int millis)
	{
		this.setHours(hours);
		this.setMinutes(minutes);
		this.setSeconds(seconds);
		this.setMillis(millis);
	}
}
