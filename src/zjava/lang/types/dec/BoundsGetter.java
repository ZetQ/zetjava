package zjava.lang.types.dec;

import zjava.lang.types.ZBounds;

public interface BoundsGetter extends PointGetter, SizeGetter
{
	public default ZBounds getBounds()
	{
		return new ZBounds(this);
	}
}
