package zjava.lang.types.dec;

public interface SizeSetter extends SizeGetter
{
	public default void setSize(int width, int height)
	{
		this.setWidth(width);
		this.setHeight(height);
	}
	public default void setSize(SizeGetter size)
	{
		this.setWidth(size.getWidth());
		this.setHeight(size.getHeight());
	}
	public void setWidth(int width);
	public void setHeight(int height);
}
