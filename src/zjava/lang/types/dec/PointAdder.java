package zjava.lang.types.dec;

public interface PointAdder extends ModelSetter
{
//	>> LOCATION >>
	public default void addLocation(int location)
	{
		this.setX(this.getX() + location);
		this.setY(this.getY() + location);
	}
	public default void addLocation(PointGetter location)
	{
		this.setX(this.getX() + location.getX());
		this.setY(this.getY() + location.getY());
	}
	public default void addLocation(int x, int y)
	{
		this.setX(this.getX() + x);
		this.setY(this.getY() + y);
	}
//	<< LOCATION <<
	
//	>> X >>
	public default void addX(int x)
	{
		this.setX(this.getX() + x);
	}
//	<< X <<
	
//	>> Y >>
	public default void addY(int y)
	{
		this.setY(this.getY() + y);
	}
//	<< Y <<
}
