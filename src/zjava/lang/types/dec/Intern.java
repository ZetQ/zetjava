package zjava.lang.types.dec;

import java.util.Calendar;

import zjava.StaticObject;

final class Intern extends StaticObject
{
	private Intern() {}
	
	public static Calendar calendarFor(ZDateGetter date)
	{
		Calendar instance = Calendar.getInstance();
		instance.set(date.getYear(), date.getMonth(), date.getDay(), 0, 0);
		return instance;
	}
}
