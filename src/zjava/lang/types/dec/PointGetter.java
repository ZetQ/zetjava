package zjava.lang.types.dec;

import zjava.lang.types.ZPoint;

public interface PointGetter
{
	public default ZPoint getLocation()
	{
		return new ZPoint(this);
	}
	public int getX();
	public int getY();
}
