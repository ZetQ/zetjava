package zjava.lang.types.dec;

public interface ModelSetter extends PointGetter
{
	public default void setLocation(PointGetter location)
	{
		this.setX(location.getX());
		this.setY(location.getY());
	}
	public default void setLocation(int x, int y)
	{
		this.setX(x);
		this.setY(y);
	}
	public void setX(int x);
	public void setY(int y);
}
