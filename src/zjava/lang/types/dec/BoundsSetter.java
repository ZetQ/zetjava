package zjava.lang.types.dec;

public interface BoundsSetter extends BoundsGetter, ModelSetter, SizeSetter
{
	public default void setBounds(BoundsGetter bounds)
	{
		this.setSize(bounds.getWidth(), bounds.getHeight());
		this.setLocation(bounds.getX(), bounds.getY());
	}
	public default void setBounds(PointGetter location, SizeGetter size)
	{
		this.setSize(size);
		this.setLocation(location);
	}
	public default void setBounds(int x, int y, int width, int height)
	{
		this.setSize(width, height);
		this.setLocation(x, y);
	}
}
