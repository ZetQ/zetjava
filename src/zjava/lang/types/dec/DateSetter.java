package zjava.lang.types.dec;

import zjava.errors.ArgumentException;
import zjava.lang.reflect.ZClass;
import zjava.lang.types.ZDate;

public interface DateSetter extends ZDateGetter
{
	public void setYear(int year);
	public void setMonth(int month);
	public void setDay(int day);
	
	public default void addYear(int year)
	{
		this.setYear(this.getYear() + year);
	}
	public default void addMonth(int month)
	{
		this.setMonth(this.getMonth() + month);
	}
	public default void addDay(int day)
	{
		this.setDay(this.getDay() + day);
	}
	public default void addDate(ZDate date)
	{
		this.addDate(date.getDay(), this.getMonth(), this.getYear());
	}
	public default void addDate(int day, int month, int year)
	{
		this.setDate(this.getDay() + day, this.getMonth() + month, this.getYear() + year);
	}
	
	public default void setDate(int day, int month, int year)
	{
		this.setYear(year);
		this.setMonth(month);
		this.setDay(day);
	}
	public default void setDate(String date)
	{	
		date = date.trim();
		
		String[] parts = date.split((String)ZClass.get(ZDate.class).getField("REGEX_DELIMITER").getValue());
		int year, month, day;
		if(ZDate.isDMYDate(date))
		{
			day = Integer.valueOf(parts[0]);
			month = Integer.valueOf(parts[1]);
			year = Integer.valueOf(parts[2]);
		}
		else if(ZDate.isYMDDate(date))
		{
			day = Integer.valueOf(parts[2]);
			month = Integer.valueOf(parts[1]);
			year = Integer.valueOf(parts[3]);
		}
		else
		{
			throw new ArgumentException("the string \"" + date + "\" cannot be converted into a date");
		}
		this.setDate(day, month, year);
	}
}
