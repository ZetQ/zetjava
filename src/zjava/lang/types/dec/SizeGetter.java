package zjava.lang.types.dec;

import zjava.lang.types.ZSize;

public interface SizeGetter
{
	public default ZSize getSize()
	{
		return new ZSize(this);
	}
	public int getWidth();
	public int getHeight();
}
