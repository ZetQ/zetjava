package zjava.lang.types.dec;

public interface SizeAdder extends SizeSetter
{
//  >> SIZE >>
	public default void addSize(int size)
	{
		this.setWidth(this.getWidth() + size);
		this.setHeight(this.getHeight() + size);
	}
	public default void addSize(SizeGetter size)
	{
		this.setWidth(this.getWidth() + size.getWidth());
		this.setHeight(this.getHeight() + size.getHeight());
	}
	public default void addSize(int width, int height)
	{
		this.setWidth(this.getWidth() + width);
		this.setHeight(this.getHeight() + height);
	}
//	<< SIZE <<
	
//	>> WIDTH >>
	public default void addWidth(int width)
	{
		this.setWidth(this.getWidth() + width);
	}
//	<< WIDTH <<
	
//	>> HEIGHT >>
	public default void addHeight(int height)
	{
		this.setHeight(this.getHeight() + height);
	}
//	<< HEIGHT <<
}
