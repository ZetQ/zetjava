package zjava.lang.types.dec;

import java.util.Calendar;

import zjava.errors.ArgumentException;
import zjava.errors.UnexpectedException;
import zjava.lang.utils.ZString;

public interface ZDateGetter
{	
	public static enum Day
	{
		MONDAY(1),
		TUESDAY(2),
		WEDNESDAY(3),
		THURSDAY(4),
		FRIDAY(5),
		SATURDAY(6),
		SUNDAY(7)
		;
		private final int
			number;
		
		private Day(int number)
		{
			this.number = number;
		}
		public int getNumber()
		{
			return this.number;
		}
		public String getName()
		{
			return ZString.firstUpper(this.name());
		}
		
//		>> STATIC SECTION >>
		public static Day get(int day)
		{
			switch(day)
			{
				case 1: return MONDAY;
				case 2: return TUESDAY;
				case 3: return WEDNESDAY;
				case 4: return THURSDAY;
				case 5: return FRIDAY;
				case 6: return SATURDAY;
				case 7: return SUNDAY;
			}
			throw new ArgumentException("unknown day: " + day + " | range: 1 - 7");
		}
//		<< STATIC SECTION <<
	}
	public static enum Month
	{
		JANUARY(1),
		FEBRUARY(2),
		MARCH(3),
		APRIL(4),
		MAY(5),
		JUNE(6),
		JULY(7),
		AUGUST(8),
		SEPTEMBER(9),
		OCTOBER(10),
		NOVEMBER(11),
		DECEMBER(12)
		;
		
		private final int
			number;

		private Month(int number)
		{
			this.number = number;
		}
		public int getNumber()
		{
			return this.number;
		}
		public String getName()
		{
			return ZString.firstUpper(this.name());
		}
		
//		>> STATIC SECTION >>
		public static Month get(int month)
		{
			switch(month)
			{
				case 1: return JANUARY;
				case 2: return FEBRUARY;
				case 3: return MARCH;
				case 4: return APRIL;
				case 5: return MAY;
				case 6: return JUNE;
				case 7: return JULY;
				case 8: return AUGUST;
				case 9: return SEPTEMBER;
				case 10: return OCTOBER;
				case 11: return NOVEMBER;
				case 12: return DECEMBER;
			}
			throw new ArgumentException("unknown month: " + month + " | range: 1 - 12");
		}
//		<< STATIC SECTION <<
	}
	
//	>> DEFINITION >>
	public int getYear();
	public int getMonth();
	public int getDay();
//	<< DEFINITION <<
	
//	>> MISC >>
	public default boolean isLeapYear()
	{
		int year = this.getYear();
		if(year % 4 != 0) 
		{
			return false;
		} 
		else if(year % 400 == 0) 
		{
			return true;
		} 
		else if(year % 100 == 0) 
		{
			return false;
		} 
		else 
		{
			return true;
		}
	}
//	<< MISC <<
	
//	>> COMPARE >>
	public default boolean isBefore(ZDateGetter date)
	{
		return this.getYear() < date.getYear() ? true : this.getMonth() < date.getMonth() ? true : this.getDay() < date.getDay();
	}
	public default boolean isWhen(ZDateGetter date)
	{
		return this.getYear() == date.getYear() && this.getMonth() == date.getMonth() && this.getDay() == date.getDay();
	}
	public default boolean isAfter(ZDateGetter date)
	{
		return !this.isBefore(date);
	}
//	<< COMPARE <<
	
//	>> WEEK OF >>
	public default int getWeekOfYear()
	{
		return Intern.calendarFor(this).get(Calendar.WEEK_OF_YEAR);
	}
	public default int getWeekOfMonth()
	{
		return Intern.calendarFor(this).get(Calendar.WEEK_OF_MONTH);
	}
//	<< WEEK OF <<
	
//	>> DAY OF >>
	public default int getDayOfWeek()
	{
		switch(Intern.calendarFor(this).get(Calendar.DAY_OF_WEEK))
		{
			case Calendar.MONDAY: return Day.MONDAY.getNumber();
			case Calendar.TUESDAY: return Day.TUESDAY.getNumber();
			case Calendar.WEDNESDAY: return Day.WEDNESDAY.getNumber();
			case Calendar.THURSDAY: return Day.THURSDAY.getNumber();
			case Calendar.FRIDAY: return Day.FRIDAY.getNumber();
			case Calendar.SATURDAY: return Day.SATURDAY.getNumber();
			case Calendar.SUNDAY: return Day.SUNDAY.getNumber();
		}
		throw new UnexpectedException("unknown day of week: " + Intern.calendarFor(this).get(Calendar.DAY_OF_WEEK));
	}
	public default int getDayOfYear()
	{
		return Intern.calendarFor(this).get(Calendar.DAY_OF_YEAR);
	}
//	<< DAY OF <<
	
//	>> DAYS OF >>
	public default int getDaysOfYear()
	{
		return this.isLeapYear() ? 366 : 365;
	}
	public default int getDaysOfMonth()
	{
		switch(Month.get(this.getMonth()))
		{
//			>> months with 31 days >>
			case JANUARY: 
			case MARCH:
			case MAY:
			case JULY:
			case OCTOBER:
			case DECEMBER: return 31;
//			<< months with 31 days <<
			
//			>> months with 30 days >>
			case APRIL:
			case JUNE:
			case AUGUST:
			case SEPTEMBER:
			case NOVEMBER: return 30;
//			<< months with 30 days <<
			
//			>> check for leapyear >>
			case FEBRUARY: return this.isLeapYear() ? 29 : 28;
//			<< check for leapyear <<
		}
		throw new UnexpectedException("unknown month: " + this.getMonth());
	}
//	<< DAYS OF <<
}
