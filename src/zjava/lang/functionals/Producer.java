package zjava.lang.functionals;

@FunctionalInterface
public interface Producer<I, O>
{
	public O produce(I object);
}
