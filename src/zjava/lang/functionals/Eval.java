package zjava.lang.functionals;

public interface Eval<T> 
{
	public boolean evaluate(T object);
}