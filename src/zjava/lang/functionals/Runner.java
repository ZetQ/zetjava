package zjava.lang.functionals;

@FunctionalInterface
public interface Runner
{
	public void execute();
}
