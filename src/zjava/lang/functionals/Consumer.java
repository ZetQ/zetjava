package zjava.lang.functionals;

@FunctionalInterface
public interface Consumer<I>
{
	public void consume(I object);
}
