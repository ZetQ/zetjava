package zjava.lang.functionals;

@FunctionalInterface
public interface Provider<O>
{
	public O provide();
}
