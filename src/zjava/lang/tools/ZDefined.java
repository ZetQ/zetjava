package zjava.lang.tools;

import zjava.ZObject;
import zjava.auto.annotations.Attribute;

public class ZDefined<T> extends ZObject
{
	private static final RuntimeException
		DEFAULT_EXCEPTION = new NullPointerException("the value is not set");
	
	@Attribute(showName=false)
	private T
		value;
	
	private RuntimeException
		exception;
	
	public ZDefined()
	{
		this(null, DEFAULT_EXCEPTION);
	}
	public ZDefined(T value)
	{
		this(value, DEFAULT_EXCEPTION);
	}
	public ZDefined(RuntimeException exception)
	{
		this(null, exception);
	}
	public ZDefined(T value, RuntimeException exception)
	{
		this.value = value;
		this.exception = exception;
	}
	
	public boolean isSet()
	{
		return null != this.value;
	}
	public T get()
	{
		if(this.isSet())
		{
			return this.value;
		}
		throw this.exception;
	}
	public void set(T value)
	{
		this.value = value;
	}
	
	
	
}
