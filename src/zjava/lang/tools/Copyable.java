package zjava.lang.tools;

public interface Copyable<T extends Copyable<T>>
{
	/**
	 * Creates a new instance of this class.
	 * <br>The new instance will be equal to this object.
	 * 
	 * <ol type="disc">
	 * <li>Although the new instance is basically equal to this one, 
	 * <br>the equals-method will most likely not return <b>true</b></li>
	 * </ol>
	 * @return the created instance
	 */
	public T copy();
}
