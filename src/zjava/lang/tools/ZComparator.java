package zjava.lang.tools;

import java.util.Comparator;
import java.util.function.Function;

import zjava.errors.UnexpectedException;

@FunctionalInterface
public interface ZComparator<T>
{
	public static enum Relation
	{
		SMALLER, EQUAL, BIGGER;
	}
	
	public static <P extends Comparable<? extends P>> ZComparator<P> create(Comparator<P> comp)
	{
		return (left, right) -> {
			int result = comp.compare(left, right);
			if(result > 0)
			{
				return Relation.BIGGER;
			}
			if(result < 0)
			{
				return Relation.SMALLER;
			}
			return Relation.EQUAL;
		};
	}
	public static <I, O extends Comparable<O>> ZComparator<I> create(Function<I, O> function)
	{
		return (leftIn, rightIn) -> {
			O left = function.apply(leftIn);
			O right = function.apply(rightIn);
			
			int result = left.compareTo(right);
			if(result < 0)
			{
				return Relation.SMALLER;
			}
			if(result > 0)
			{
				return Relation.BIGGER;
			}
			return Relation.EQUAL;
		};
	}
	public static <P extends Comparable<P>> ZComparator<P> create(Class<P> clazz)
	{
		return (left, right) -> {
			int result = left.compareTo(right);
			if(result < 0)
			{
				return Relation.SMALLER;
			}
			if(result > 0)
			{
				return Relation.BIGGER;
			}
			return Relation.EQUAL;
		};
	}
	
	public Relation leftCompare(T left, T right);
	public default Relation rightCompare(T left, T right)
	{
		Relation relation = this.leftCompare(left, right);
		switch(relation)
		{
			case BIGGER:
			{
				return Relation.SMALLER;
			}
			case EQUAL:
			{
				return relation;
			}
			case SMALLER:
			{
				return Relation.SMALLER;
			}
		}
		throw new UnexpectedException("the ZValueRelation \"" + relation + "\" is unknown");
	}
	
	public default boolean equals(T left, T right)
	{
		return this.leftCompare(left, right) == Relation.EQUAL;
	}
	public default boolean leftBigger(T left, T right)
	{
		return this.leftCompare(left, right) == Relation.BIGGER;
	}
	public default boolean rightBigger(T left, T right)
	{
		return this.rightCompare(left, right) == Relation.BIGGER;
	}
	public default boolean leftSmaller(T left, T right)
	{
		return this.leftCompare(left, right) == Relation.SMALLER;
	}
	public default boolean rightSmaller(T left, T right)
	{
		return this.rightCompare(left, right) == Relation.SMALLER;
	}
	public default boolean leftBiggerEquals(T left, T right)
	{
		Relation relation = this.leftCompare(left, right);
		return relation == Relation.BIGGER || relation == Relation.EQUAL;
	}
	public default boolean rightBiggerEquals(T left, T right)
	{
		Relation relation = this.rightCompare(left, right);
		return relation == Relation.BIGGER || relation == Relation.EQUAL;
	}
	public default boolean leftSmallerEquals(T left, T right)
	{
		Relation relation = this.leftCompare(left, right);
		return relation == Relation.SMALLER || relation == Relation.EQUAL;
	}
	public default boolean rightSmallerEquals(T left, T right)
	{
		Relation relation = this.rightCompare(left, right);
		return relation == Relation.SMALLER || relation == Relation.EQUAL;
	}
}
