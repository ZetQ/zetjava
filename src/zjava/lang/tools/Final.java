package zjava.lang.tools;

import zjava.ZObject;
import zjava.auto.annotations.Attribute;

public class Final<T> extends ZObject
{
	@Attribute
	private T
		value;
	
	public Final()
	{
		this(null);
	}
	public Final(T value)
	{
		this.value = value;
	}
	
	public T get()
	{
		return this.value;
	}
	public void set(T value)
	{
		this.value = value;
	}
}
