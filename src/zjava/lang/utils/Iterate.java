package zjava.lang.utils;

import java.util.Iterator;
import java.util.function.Consumer;

import zjava.lang.functionals.Producer;
import zjava.lang.functionals.Provider;

public final class Iterate
{
	private Iterate() {}
	
	public static int getSize(Iterable<?> iterable)
	{
		int amount = 0;
		for(@SuppressWarnings("unused") Object o : iterable)
		{
			++amount;
		}
		return amount;
	}
	public static<P> int getSize(Iterable<P> iterable, Producer<P, Integer> producer)
	{
		int amount = 0;
		for(P object : iterable)
		{
			amount += producer.produce(object);
		}
		return amount;
	}
	public static <P> Iterator<P> create(Producer<Integer, Boolean> hasNext, Producer<Integer, P> next)
	{
		return new Iterator<P>()
		{
			private int
				index = 0;
			
			@Override
			public boolean hasNext()
			{
				return hasNext.produce(this.index);
			}
			@Override
			public P next()
			{
				return next.produce(this.index++);
			}
	
		};
	}
	public static <P> Iterator<P> create(Provider<Integer> getSize, Producer<Integer, P> get)
	{
		return new Iterator<P>()
		{
			private int 
				index = 0;
			
			@Override
			public boolean hasNext()
			{
				return this.index < getSize.provide();
			}
			@Override
			public P next()
			{
				return get.produce(this.index++);
			}
		
		};
	}
	
	public static <P> void forEach(final Iterable<P> iterable, final Consumer<P> consumer)
	{
		for(P object : iterable)
		{
			consumer.accept(object);
		}
	}
}
