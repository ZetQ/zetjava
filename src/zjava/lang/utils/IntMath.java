package zjava.lang.utils;

import zjava.StaticObject;
import zjava.auto.annotations.Testing;

public class IntMath extends StaticObject
{
	private IntMath() {}
	
//	>> RANDOM >>
	public static int random(int number1, int number2)
	{
		return (int) DoubleMath.random(number1, number2);
	}
//	<< RANDOM <<
	
//	>> TRANSFORM >>
	public static int positive(Number value)
	{
		return (int) DoubleMath.positive(value);
	}
	public static int negative(Number value)
	{
		return (int) DoubleMath.negative(value);
	}
//	<< TRANSFORM <<
	
//	>> FORMULAS >>
	public static int pythagoras(Number cathetus1, Number cathetus2)
	{
		return (int) DoubleMath.pythagoras(cathetus1, cathetus2);
	}
//	<< FORMULAS <<
	
//	>> ARITHMETIC OPERATIONS >>
	public static int add(Number value, Number... values)
	{
		return (int) DoubleMath.add(value, values);
	}
	public static int sub(Number value, Number... values)
	{
		return (int) DoubleMath.sub(value, values);
	}
	public static int mul(Number value, Number... values)
	{
		return (int) DoubleMath.mul(value, values);
	}
	public static int div(Number value, Number... values)
	{
		return (int) DoubleMath.div(value, values);
	}
	public static int raise(Number value, Number exponent)
	{
		return (int) DoubleMath.raise(value, exponent);		
	}
	public static int sqrt(Number value)
	{
		return (int) DoubleMath.sqrt(value);
	}
	@Testing
	public static int root(Number value, Number root)
	{
		 return (int) DoubleMath.root(value, root);
	}
//	<< ARITHMETIC OPERATIONS <<
	
//	>> SIZE >>
	public static int greatest(Number value, Number... values)
	{
		return (int) DoubleMath.greatest(value, values);
	}
	public static int smallest(Number value, Number... values)
	{
		return (int) DoubleMath.smallest(value, values);
	}
	public static int min(Number value, Number min)
	{
		return (int) DoubleMath.min(value, min);
	}
	public static int max(Number value, Number max)
	{
		return (int) DoubleMath.max(value, max);
	}
	public static int minmax(Number value, Number min, Number max)
	{
		return (int) DoubleMath.minmax(value, min, max);
	}
//	<< SIZE <
}
