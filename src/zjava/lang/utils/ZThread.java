package zjava.lang.utils;

import java.util.Iterator;

import zjava.ZObject;
import zjava.errors.UnexpectedException;
import zjava.lang.functionals.Runner;
import zjava.lang.tools.Final;
import zjava.lang.functionals.Consumer;

public class ZThread extends ZObject
{
	public static void sleep(long pause)
	{
		try
		{
			if(pause <= 0)
			{
				return;
			}
			Thread.sleep(pause);
		}
		catch(InterruptedException e)
		{
			throw new UnexpectedException("the thread could not be paused", e);
		}
	}
	public static ZThread execute(Runner seq)
	{
		return new ZThread(new Thread(seq::execute));
	}
	public static ZThread loop(long delay, Runner seq)
	{
		Final<ZThread> thread = new Final<>();
		thread.set(new ZThread(new Thread(() -> {
			while(!thread.get().isStopped())
			{
				seq.execute();
				ZThread.sleep(delay);
			}
		})));
		return thread.get();
	}
	public static ZThread loop(int amount, long delay, Runner seq)
	{
		Final<ZThread> thread = new Final<>();
		thread.set(new ZThread(new Thread(() -> {
			int i = 0;
			while(!thread.get().isStopped() && i < amount)
			{
				seq.execute();
				ZThread.sleep(delay);
				++i;
			}
		})));
		return thread.get();
	}
	public static <P> ZThread forEach(Iterable<P> iterable, Consumer<P> consumer)
	{
		Final<ZThread> thread = new Final<>();
		thread.set(new ZThread(new Thread(() -> {
			Iterator<P> it = iterable.iterator();
			while(!thread.get().isStopped() && it.hasNext())
			{
				consumer.consume(it.next());
			}
		})));
		return thread.get();
	}
	
	private final Thread
		thread;
	
	private boolean
		isStopped = false;
	
	private ZThread(Thread thread)
	{
		this.thread = thread;
		thread.start();
	}

	public void stop()
	{
		this.isStopped = true;
	}
	
	public boolean isStopped()
	{
		return this.isStopped;
	}
	public boolean isAlive()
	{
		return this.thread.isAlive();
	}
	
//	public static ZStatus run(ZRunnable runnable) 
//	{
//		Thread thread = new Thread(runnable::run);
//		thread.start();
//		return () -> thread.isAlive();
//	}
//	public static ZStatus loop(ZRunnable runnable)
//	{
//		Thread thread = new Thread(() -> {
//			while(true)
//			{
//				runnable.run();
//			}
//		});
//		thread.start();
//		return () -> thread.isAlive();
//	}
//	public static ZStatus loop(ZRunnable runnable, long pause)
//	{
//		Thread thread = new Thread(() -> {
//			while(true)
//			{
//				runnable.run();
//				ZThread.pause(pause);
//			}
//		});
//		thread.start();
//		return () -> thread.isAlive();
//	}
//	public static ZStatus loop(ZProvider<Boolean> runnable)
//	{
//		Thread thread = new Thread(() -> { 
//			while(!runnable.provide());
//		});
//		thread.start();
//		return () -> thread.isAlive();
//	}
//	public static ZStatus loop(ZProvider<Boolean> runnable, long pause)
//	{
//		Thread thread = new Thread(() -> { 
//			while(runnable.provide()) 
//			{ 
//				ZThread.pause(pause); 
//			} 
//		});
//		thread.start();
//		return () -> thread.isAlive();
//	}
//	public static ZStatus forAmount(ZRunnable runnable, long amount)
//	{
//		ZList<Thread> status = new ZList<>();
//		for(int i = 0; i < amount; ++i)
//		{
//			Thread thread = new Thread(() -> runnable.run());
//			thread.start();
//			status.add(thread);
//		}
//		return () -> !status.contains(false, thread -> thread.isAlive());
//	}
//	public static <P> ZStatus forEach(Iterable<P> iterable, ZConsumer<P> consumer)
//	{
//		ZList<Thread> status = new ZList<>();
//		for(P object : iterable)
//		{
//			Thread thread = new Thread(() -> consumer.consume(object));
//			thread.start();
//			status.add(thread);
//		}
//		return () -> !status.contains(false, thread -> thread.isAlive());
//	}
}
