package zjava.lang.utils;

import java.lang.reflect.Array;
import java.util.Arrays;

import zjava.StaticObject;
import zjava.ZJava;
import zjava.auto.ZAutoString;
import zjava.errors.ArgumentException;
import zjava.errors.UnexpectedException;
import zjava.lang.functionals.Eval;
import zjava.lang.functionals.Producer;
import zjava.lang.reflect.ZClass;

public final class Ray extends StaticObject
{
	private Ray() {}
	
//	>> PRIVATE SECTION >>
	private static String stringArrayClass(Class<?> arrayClass)
	{
		return ZClass.get(arrayClass).getName();
	}
	private static String stringArrayElement(int number, Object object)
	{
		return number + ZJava.ToString.getCounter() + (object instanceof ZAutoString ? ((ZAutoString)object).getDoku() : ZJava.ToString.getStart() + ZString.convert(object) + ZJava.ToString.getEnd());
	}
//	<< PRIVATE SECTION <<
	
//	>> MERGE >>
	@SafeVarargs
	public static <P> P[] merge(P[] array, P[]... arrays)
	{
		int length = array.length;
		for(P[] current : arrays)
		{
			length += current.length;
		}
		P[] merged = create(array, length);
		for(int i = 0; i < array.length; ++i)
		{
			merged[i] = array[i];
		}
		int add = array.length;
		for(P[] current : arrays)
		{
			for(int i = 0; i < current.length; ++i)
			{
				merged[i + add] = current[i];
			}
			add += current.length;
		}
		return merged;
	}
//	<< MERGE <<
	
//	>> OVERRIDE >>
	public static <P> void override(final P[] destination, final P[] source)
	{
		int length = destination.length > source.length ? destination.length : source.length;
		for(int i = 0; i < length; ++i)
		{
			if(destination.length > i && source.length > i)
			{
				destination[i] = source[i];
			}
			else if(source.length < i)
			{
				destination[i] = null;
			}
			else
			{
				return;
			}
		}
	}
	public static void override(final byte[] destination, final byte[] source)
	{
		int length = destination.length > source.length ? destination.length : source.length;
		for(int i = 0; i < length; ++i)
		{
			if(destination.length > i && source.length > i)
			{
				destination[i] = source[i];
			}
			else if(source.length < i)
			{
				destination[i] = 0;
			}
			else
			{
				return;
			}
		}
	}
	public static void override(final short[] destination, final short[] source)
	{
		int length = destination.length > source.length ? destination.length : source.length;
		for(int i = 0; i < length; ++i)
		{
			if(destination.length > i && source.length > i)
			{
				destination[i] = source[i];
			}
			else if(source.length < i)
			{
				destination[i] = 0;
			}
			else
			{
				return;
			}
		}
	}
	public static void override(final int[] destination, final int[] source)
	{
		int length = destination.length > source.length ? destination.length : source.length;
		for(int i = 0; i < length; ++i)
		{
			if(destination.length > i && source.length > i)
			{
				destination[i] = source[i];
			}
			else if(source.length < i)
			{
				destination[i] = 0;
			}
			else
			{
				return;
			}
		}
	}
	public static void override(final long[] destination, final long[] source)
	{
		int length = destination.length > source.length ? destination.length : source.length;
		for(int i = 0; i < length; ++i)
		{
			if(destination.length > i && source.length > i)
			{
				destination[i] = source[i];
			}
			else if(source.length < i)
			{
				destination[i] = 0;
			}
			else
			{
				return;
			}
		}
	}
	public static void override(final float[] destination, final float[] source)
	{
		int length = destination.length > source.length ? destination.length : source.length;
		for(int i = 0; i < length; ++i)
		{
			if(destination.length > i && source.length > i)
			{
				destination[i] = source[i];
			}
			else if(source.length < i)
			{
				destination[i] = 0;
			}
			else
			{
				return;
			}
		}
	}
	public static void override(final double[] destination, final double[] source)
	{
		int length = destination.length > source.length ? destination.length : source.length;
		for(int i = 0; i < length; ++i)
		{
			if(destination.length > i && source.length > i)
			{
				destination[i] = source[i];
			}
			else if(source.length < i)
			{
				destination[i] = 0;
			}
			else
			{
				return;
			}
		}
	}
	public static void override(final char[] destination, final char[] source)
	{
		int length = destination.length > source.length ? destination.length : source.length;
		for(int i = 0; i < length; ++i)
		{
			if(destination.length > i && source.length > i)
			{
				destination[i] = source[i];
			}
			else if(source.length < i)
			{
				destination[i] = 0;
			}
			else
			{
				return;
			}
		}
	}
	public static void override(final boolean[] destination, final boolean[] source)
	{
		int length = destination.length > source.length ? destination.length : source.length;
		for(int i = 0; i < length; ++i)
		{
			if(destination.length > i && source.length > i)
			{
				destination[i] = source[i];
			}
			else if(source.length < i)
			{
				destination[i] = false;
			}
			else
			{
				return;
			}
		}
	}
//	<< OVERRIDE >>
	
//	>> SWITCH_INDEX >>
	public static void switchIndex(final Object[] array, final int index1, final int index2)
	{
		Object save = array[index1];
		array[index1] = array[index2];
		array[index2] = save; 
	}
	public static void switchIndex(final byte[] array, final int index1, final int index2)
	{
		
	}
	//TODO
//	<< SWITCH_INDEX <<
	
//	>> SORT DESCENDING (Z-A) (0-n) >>
	//TODO
//	<< SORT DESCENDING (Z-A) (0-n) <<
	
//	>> SORT ASCENDING (A-Z) (n-0) >>
	public static <P extends Comparable<? super P>> void sortAsc(final P[] array)
	{
		P[] sorted = Ray.create(array, array.length);
		forLoop : for(P object : array)
		{
			if(null != object)
			{
				int index = 0;
				while(index < array.length)
				{
					if(null == sorted[index])
					{
						sorted[index] = object;
						continue forLoop;
					}
					else if(Relation.more(object, sorted[index]))
					{
						P save = object;
						object = sorted[index];
						sorted[index] = save;
					}
					++index;
				}
			}
		}
		Ray.override(array, sorted);
	}
	public static void sortAsc(final int[] array)
	{
		int[] sorted = new int[array.length];
		
		for(int object : array)
		{			
			int check = 0;
			while(check < array.length)
			{
				if(object > sorted[check])
				{
					int save = object;
					object = sorted[check];
					sorted[check] = save;
				}
				++check;
			}
		}
		Ray.override(array, sorted);
	}
	//TODO
//	<< SORT ASCENDING (A-Z) (n-0) <<
	
//	>> COMPONENT TYPE >>
	public static <P> ZClass<P> getType(P[] array)
	{
		@SuppressWarnings("unchecked") Class<P> type = (Class<P>) array.getClass().getComponentType();
		return ZClass.get(type);
	}
//	<< COMPONENT TYPE <<
	
//	>> CREATE >>
	public static <P> P[] create(ZClass<P> clazz, int length)
	{
		return Ray.create(clazz.toClass(), length);
	}
	@SuppressWarnings("unchecked")
	public static <P> P[] create(Class<P> clazz, int length)
	{
		if(length < 0) { throw new ArgumentException("a array cannot be created with an negative index"); }
		return (P[]) Array.newInstance(clazz, length);
	}
	@SuppressWarnings("unchecked")
	public static <P> P[] create(P[] refArray, int length)
	{
		return (P[]) Array.newInstance(refArray.getClass().getComponentType(), length);
	}
//	<< CREATE <<
	
//	>> COPY >>
	public static <P> P[] copy(final P[] array)
	{
		return Arrays.copyOf(array, array.length);
	}
	public static byte[] copy(final byte[] array)
	{
		return Arrays.copyOf(array, array.length);
	}
	public static short[] copy(final short[] array)
	{
		return Arrays.copyOf(array, array.length);
	}
	public static int[] copy(final int[] array)
	{
		return Arrays.copyOf(array, array.length);
	}
	public static long[] copy(final long[] array)
	{
		return Arrays.copyOf(array, array.length);
	}
	public static float[] copy(final float[] array)
	{
		return Arrays.copyOf(array, array.length);
	}
	public static double[] copy(final double[] array)
	{
		return Arrays.copyOf(array, array.length);
	}
	public static char[] copy(final char[] array)
	{
		return Arrays.copyOf(array, array.length);
	}
	public static boolean[] copy(final boolean[] array)
	{
		return Arrays.copyOf(array, array.length);
	}
	public static <P> P[] copy(final P[] array, final int length)
	{
		if(length < 0) { throw new ArgumentException("an array cannot have a negative length"); };
		return Arrays.copyOf(array, length);
	}
	public static byte[] copy(final byte[] array, final int length)
	{
		if(length < 0) { throw new ArgumentException("an array cannot have a negative length"); };
		return Arrays.copyOf(array, length);
	}
	public static short[] copy(final short[] array, final int length)
	{
		if(length < 0) { throw new ArgumentException("an array cannot have a negative length"); };
		return Arrays.copyOf(array, length);
	}
	public static int[] copy(final int[] array, final int length)
	{
		if(length < 0) { throw new ArgumentException("an array cannot have a negative length"); };
		return Arrays.copyOf(array, length);
	}
	public static long[] copy(final long[] array, final int length)
	{
		if(length < 0) { throw new ArgumentException("an array cannot have a negative length"); };
		return Arrays.copyOf(array, length);
	}
	public static float[] copy(final float[] array, final int length)
	{
		if(length < 0) { throw new ArgumentException("an array cannot have a negative length"); };
		return Arrays.copyOf(array, length);
	}
	public static double[] copy(final double[] array, final int length)
	{
		if(length < 0) { throw new ArgumentException("an array cannot have a negative length"); };
		return Arrays.copyOf(array, length);
	}
	public static char[] copy(final char[] array, final int length)
	{
		if(length < 0) { throw new ArgumentException("an array cannot have a negative length"); };
		return Arrays.copyOf(array, length);
	}
	public static boolean[] copy(final boolean[] array, final int length)
	{
		if(length < 0) { throw new ArgumentException("an array cannot have a negative length"); };
		return Arrays.copyOf(array, length);
	}
//	<< COPY <<
	
//	>> CONTAINS EXTRACT >>
	public static <P1, P2> boolean contains(P1[] array, P2 object, Producer<P1, P2> producer)
	{
		if(null == object)
		{
			for(P1 element : array)
			{
				if(null == element)
				{
					return true;
				}
			}
		}
		else
		{
			for(P1 element : array)
			{
				if(null != element && object.equals(producer.produce(element)))
				{
					return true;
				}
			}
		}
		return false;
	}
	public static <P> boolean contains(P[] array, boolean value, Eval<P> eval)
	{
		for(P object : array)
		{
			if(eval.evaluate(object) == value)
			{
				return true;
			}
		}
		return false;
	}
//	<< CONTAINS EXTRACT <<
	
//	>> CONTAINS ALL >>
	public static <P> boolean containsAll(P[] array, P[] objects)
	{
		for(P object : objects)
		{
			if(!Ray.contains(array, object))
			{
				return false;
			}
		}
		return true;
	}
	//TODO primitive containsAll
//	<< CONTAINS ALL <<
	
//	>> CONTAINS >>
	public static <P> boolean contains(final P[] array, final P object)
	{
		if(null == object)
		{
			for(final P element : array)
			{
				if(null == element)
				{
					return true;
				}
			}
		}
		else
		{
			for(final P element : array)
			{
				if(null != element && object.equals(element))
				{
					return true;
				}
			}
		}
		return false;
	}
	public static boolean contains(final byte[] array, final byte value)
	{
		for(final byte element : array)
		{
			if(element == value)
			{
				return true;
			}
		}
		return false;
	}
	public static boolean contains(final short[] array, final short value)
	{
		for(final short element : array)
		{
			if(element == value)
			{
				return true;
			}
		}
		return false;
	}
	public static boolean contains(final int[] array, final int value)
	{
		for(final int element : array)
		{
			if(element == value)
			{
				return true;
			}
		}
		return false;
	}
	public static boolean contains(final long[] array, final long value)
	{
		for(final long element : array)
		{
			if(element == value)
			{
				return true;
			}
		}
		return false;
	}
	public static boolean contains(final float[] array, final float value)
	{
		for(final float element : array)
		{
			if(element == value)
			{
				return true;
			}
		}
		return false;
	}
	public static boolean contains(final double[] array, final double value)
	{
		for(final double element : array)
		{
			if(element == value)
			{
				return true;
			}
		}
		return false;
	}
	public static boolean contains(final char[] array, final char value)
	{
		for(final char element : array)
		{
			if(element == value)
			{
				return true;
			}
		}
		return false;
	}
	public static boolean contains(final boolean[] array, final boolean value)
	{
		for(final boolean element : array)
		{
			if(element == value)
			{
				return true;
			}
		}
		return false;
	}
//	<< CONTAINS <<
	
//	>> TO_STRING >>
	public static String toString(final Object[] array)
	{
		String string = Ray.stringArrayClass(array.getClass()).replaceFirst("\\[\\]", "") +  "[";
		for(int i = 0; i < array.length; ++i)
		{
			string += Ray.stringArrayElement(i, array[i]);
			if(i < array.length - 1)
			{
				string +=  ZJava.ToString.getSeparation();
			}
		}
		return string + "]";
	}
	public static String toString(final byte[] array)
	{
		String string = Ray.stringArrayClass(array.getClass()).replaceFirst("\\[\\]", "") +  "[";
		for(int i = 0; i < array.length; ++i) { string += Ray.stringArrayElement(i, array[i]); if(i < array.length - 1) { string += ZJava.ToString.getSeparation(); }}
		return string + "]";
	}
	public static String toString(final short[] array)
	{
		String string = Ray.stringArrayClass(array.getClass()).replaceFirst("\\[\\]", "") + "[";
		for(int i = 0; i < array.length; ++i) { string += Ray.stringArrayElement(i, array[i]); if(i < array.length - 1) { string += ZJava.ToString.getSeparation(); }}
		return string + "]";
	}
	public static String toString(final int[] array)
	{
		String string = Ray.stringArrayClass(array.getClass()).replaceFirst("\\[\\]", "") +  "[";
		for(int i = 0; i < array.length; ++i) { string += Ray.stringArrayElement(i, array[i]); if(i < array.length - 1) { string += ZJava.ToString.getSeparation(); }}
		return string + "]";
	}
	public static String toString(final long[] array)
	{
		String string = Ray.stringArrayClass(array.getClass()).replaceFirst("\\[\\]", "") +  "[";
		for(int i = 0; i < array.length; ++i) { string += Ray.stringArrayElement(i, array[i]); if(i < array.length - 1) { string += ZJava.ToString.getSeparation(); }}
		return string + "]";
	}
	public static String toString(final float[] array)
	{
		String string = Ray.stringArrayClass(array.getClass()).replaceFirst("\\[\\]", "") +  "[";
		for(int i = 0; i < array.length; ++i) { string += Ray.stringArrayElement(i, array[i]); if(i < array.length - 1) { string += ZJava.ToString.getSeparation(); }}
		return string + "]";
	}
	public static String toString(final double[] array)
	{
		String string = Ray.stringArrayClass(array.getClass()).replaceFirst("\\[\\]", "") +  "[";
		for(int i = 0; i < array.length; ++i) { string += Ray.stringArrayElement(i, array[i]); if(i < array.length - 1) { string += ZJava.ToString.getSeparation(); }}
		return string + "]";
	}
	public static String toString(final char[] array)
	{
		String string = Ray.stringArrayClass(array.getClass()).replaceFirst("\\[\\]", "") +  "[";
		for(int i = 0; i < array.length; ++i) { string += Ray.stringArrayElement(i, array[i]); if(i < array.length - 1) { string += ZJava.ToString.getSeparation(); }}
		return string + "]";
	}
	public static String toString(final boolean[] array)
	{
		String string = Ray.stringArrayClass(array.getClass()).replaceFirst("\\[\\]", "") +  "[";
		for(int i = 0; i < array.length; ++i) { string += Ray.stringArrayElement(i, array[i]); if(i < array.length - 1) { string += ZJava.ToString.getSeparation(); }}
		return string + "]";
	}
//	<< TO_STRING <<
	
//	>> EQUALS >>
	public static boolean equals(final Object[] array1, final Object[] array2)
	{
		if(array1 == array2) { return true; }
		if(((null == array1) == (null == array2)) && ZClass.get(array1.getClass()).isTypeOf(array2) && array1.length == array2.length)
		{
			for(int i = 0; i < array1.length; ++i)
			{
				Object object1 = array1[i];
				Object object2 = array2[i];
				
				if(null == object1)
				{
					if(null != object2)
					{
						return false;
					}
				}
				else
				{
					if(null == object2) { return false; }
					if(object1.getClass().isArray())
					{
						if(!object2.getClass().isArray()) { throw new UnexpectedException("cannot compare an array to a non-array"); }
						if(object1.getClass().getComponentType().isPrimitive())
						{
							//goes in here when arrays are multidimensional - primitive multidimensional arrays are Objects, but the last-level array is still primitive
							if(!object2.getClass().getComponentType().isPrimitive()) { throw new UnexpectedException("cannot compare a primitive and a non-primitive array"); }
							boolean result = false;
							if(object1 instanceof byte[]) { result = Ray.equals((byte[])object1, (byte[])object2); }
							else if(object1 instanceof short[]) { result = Ray.equals((short[])object1, (short[])object2); }
							else if(object1 instanceof int[]) { result = Ray.equals((int[])object1, (int[])object2); }
							else if(object1 instanceof long[]) { result = Ray.equals((long[])object1, (long[])object2); }
							else if(object1 instanceof float[]) { result = Ray.equals((float[])object1, (float[])object2); }
							else if(object1 instanceof double[]) { result = Ray.equals((double[])object1, (double[])object2); }
							else if(object1 instanceof char[]) { result = Ray.equals((char[])object1, (char[])object2); }
							else if(object1 instanceof boolean[]) { result = Ray.equals((boolean[])object1, (boolean[])object2); }
							else { throw new UnexpectedException("no matching primitive found");}
							if(!result)
							{
								return false;
							}
						}
						else if(!Ray.equals((Object[])object1, (Object[])object2))
						{
							return false;
						}
					}
					else
					{
						if(object2 == null || !object1.equals(object2))
						{
							return false;
						}
					}
				}
			}
			return true;
		}
		return false;
	}
	public static boolean equals(final byte[] array1, final byte[] array2)
	{
		if(array1 == array2) { return true; }
		if(((null == array1) == (null == array2)) && array1.length == array2.length)
		{
			for(int i = 0; i < array1.length; ++i)
			{
				if(array1[i] != array2[i])
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}
	public static boolean equals(final short[] array1, final short[] array2)
	{
		if(array1 == array2) { return true; }
		if(((null == array1) == (null == array2)) && array1.length == array2.length)
		{
			for(int i = 0; i < array1.length; ++i)
			{
				if(array1[i] != array2[i])
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}
	public static boolean equals(final int[] array1, final int[] array2)
	{
		if(array1 == array2) { return true; }
		if(((null == array1) == (null == array2)) && array1.length == array2.length)
		{
			for(int i = 0; i < array1.length; ++i)
			{
				if(array1[i] != array2[i])
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}
	public static boolean equals(final long[] array1, final long[] array2)
	{
		if(array1 == array2) { return true; }
		if(((null == array1) == (null == array2)) && array1.length == array2.length)
		{
			for(int i = 0; i < array1.length; ++i)
			{
				if(array1[i] != array2[i])
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}
	public static boolean equals(final float[] array1, final float[] array2)
	{
		if(array1 == array2) { return true; }
		if(((null == array1) == (null == array2)) && array1.length == array2.length)
		{
			for(int i = 0; i < array1.length; ++i)
			{
				if(array1[i] != array2[i])
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}
	public static boolean equals(final double[] array1, final double[] array2)
	{
		if(array1 == array2) { return true; }
		if(((null == array1) == (null == array2)) && array1.length == array2.length)
		{
			for(int i = 0; i < array1.length; ++i)
			{
				if(array1[i] != array2[i])
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}
	public static boolean equals(final char[] array1, final char[] array2)
	{
		if(array1 == array2) { return true; }
		if(((null == array1) == (null == array2)) && array1.length == array2.length)
		{
			for(int i = 0; i < array1.length; ++i)
			{
				if(array1[i] != array2[i])
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}
	public static boolean equals(final boolean[] array1, final boolean[] array2)
	{
		if(array1 == array2) { return true; }
		if(((null == array1) == (null == array2)) && array1.length == array2.length)
		{
			for(int i = 0; i < array1.length; ++i)
			{
				if(array1[i] != array2[i])
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}
//	<< EQUALS <<
}
