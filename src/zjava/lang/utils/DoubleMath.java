package zjava.lang.utils;

import java.util.Random;

import zjava.Constant.Arithmetic;
import zjava.StaticObject;
import zjava.auto.annotations.Testing;

public class DoubleMath extends StaticObject
{
	private DoubleMath() {}
	
//	>> RANDOM >>
	public static double random(double number1, double number2)
	{
		double min = number1 < number2 ? number1 : number2;
		double max = min == number2 ? number1 : number2;
		return min + (max - min) * new Random().nextDouble();
	}
//	<< RANDOM <<
	
//	>> TRANSFORM >>
	public static double positive(Number value)
	{
		double result = value.doubleValue();
		return result >= 0 ? result : result * -1;
	}
	public static double negative(Number value)
	{
		double result = value.doubleValue();
		return result <= 0 ? result : result * -1;
	}
//	<< TRANSFORM <<
	
//	>> FORMULAS >>
	public static double pythagoras(Number cathetus1, Number cathetus2)
	{
		return sqrt(raise(notNull(cathetus1).doubleValue(), 2) + raise(notNull(cathetus2.doubleValue()), 2));
	}
//	<< FORMULAS <<
	
//	>> ARITHMETIC OPERATIONS >>
	public static double add(Number value, Number... values)
	{
		double result = value.doubleValue();
		for(Number current : values)
		{
			result += current.doubleValue();
		}
		return result;
	}
	public static double sub(Number value, Number... values)
	{
		double result = value.doubleValue();
		for(Number current : values)
		{
			result -= current.doubleValue();
		}
		return result;
	}
	public static double mul(Number value, Number... values)
	{
		double result = value.doubleValue();
		for(Number current : values)
		{
			result *= current.doubleValue();
		}
		return result;
	}
	public static double div(Number value, Number... values)
	{
		double result = value.doubleValue();
		for(Number current : values)
		{
			result /= current.doubleValue();
		}
		return result;
	}
	public static double raise(Number value, Number exponent)
	{
		return StrictMath.pow(notNull(value).doubleValue(), notNull(exponent).doubleValue());				
	}
	public static double sqrt(Number value)
	{
		return StrictMath.sqrt(notNull(value).doubleValue());
	}
	@Testing
	public static double root(Number value, Number root)
	{
		 return Math.pow(Arithmetic.E, Math.log(notNull(value.doubleValue())) / notNull(root).doubleValue());
	}
//	<< ARITHMETIC OPERATIONS <<
	
//	>> SIZE >>
	public static double greatest(Number value, Number... values)
	{
		double result = value.intValue();
		for(Number currVal : values)
		{
			double current = currVal.doubleValue();
			if(result < current)
			{
				result = current;
			}
		}
		return result;
	}
	public static double smallest(Number value, Number... values)
	{
		double result = value.intValue();
		for(Number currVal : values)
		{
			double current = currVal.doubleValue();
			if(result > current)
			{
				result = current;
			}
		}
		return result;
	}
	public static double min(Number value, Number min)
	{
		double val = value.doubleValue(), compare = min.doubleValue();
		return val < compare ? compare : val;
	}
	public static double max(Number value, Number max)
	{
		double val = value.doubleValue(), compare = max.doubleValue();
		return val > compare ? compare : val;
	}
	public static double minmax(Number value, Number min, Number max)
	{
		double val = value.doubleValue(), compare = max.doubleValue();
		return val > compare ? compare : val;
	}
//	<< SIZE <
}
