package zjava.lang.utils;

import zjava.StaticObject;

public class Relation extends StaticObject
{
	public static <L extends Comparable<? super R>, R> boolean equal(L left, R right)
	{
		return left.compareTo(right) == 0;
	}
	public static <L extends Comparable<? super R>, R> boolean less(L left, R right)
	{
		return left.compareTo(right) < 0;
	}
	public static <L extends Comparable<? super R>, R> boolean more(L left, R right)
	{
		return left.compareTo(right) > 0;
	}
	public static <L extends Comparable<? super R>, R> boolean lessEqual(L left, R right)
	{
		return left.compareTo(right) <= 0;
	}
	public static <L extends Comparable<? super R>, R> boolean moreEqual(L left, R right)
	{
		return left.compareTo(right) >= 0;
	}
}
