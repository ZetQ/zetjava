package zjava.lang.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import zjava.errors.ArgumentException;

public class ZString
{
	private static final String
		CONST_NULL = "null";
	
	private static final String
		REGEX_FLOATINGPOINT = "((-|\\+)?)((\\d+\\.\\d+)|(\\d+(\\.\\d+)?(f|F|d|D)))",
		REGEX_BINARY = "[0, 1]+",
		REGEX_RGBA = "(\\d?\\d?\\d)\\s*,\\s*(\\d?\\d?\\d)\\s*,\\s*(\\d?\\d?\\d)(\\s*,\\s*(\\d?\\d?\\d))?",
		REGEX_MAIL = "[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))",
		REGEX_HEX = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$",
		REGEX_TEL = "^[+]?[0-9]{10,13}$",
		REGEX_NUMBER = "^(-?|\\+?)\\d+(\\.?(F?(\\d+)?F?))?$";
	
//	>> REGEX >>
	public static boolean doesMatch(String string, String regex)
	{
		Matcher matcher = Pattern.compile(regex).matcher(string);
		return matcher.find();
	}
	public static String getMatch(String string, String regex)
	{
		Matcher matcher = Pattern.compile(regex).matcher(string);
		if(matcher.find()) 
		{
			return matcher.group();
		}
		throw new ArgumentException("the string \"" + string + "\" does not match the regex \"" + regex + "\"");
	}
//	<< REGEX <<
	
//	>> MANIPULATION >>
	public static String convert(Object object)
	{
		if(null == object)
		{
			return CONST_NULL;
		}
		if(object instanceof String)
		{
			return new String((String)object);
		}
		if(object.getClass().isArray())
		{
			Class<?> clazz = object.getClass().getComponentType();
			if(clazz.isPrimitive())
			{
				return clazz == byte.class ? Ray.toString((byte[]) object) :
					   clazz == short.class ? Ray.toString((short[]) object) :
					   clazz == int.class ? Ray.toString((int[]) object) :
					   clazz == long.class ? Ray.toString((long[]) object) :
					   clazz == float.class ? Ray.toString((float[]) object) :
					   clazz == double.class ? Ray.toString((double[]) object) :
					   clazz == char.class ? Ray.toString((char[]) object) :
					                        Ray.toString((boolean[]) object);
			}
			return Ray.toString((Object[]) object);
		}
		return object.toString();
	}
	public static String firstUpper(String string)
	{			
		return Character.toUpperCase(string.charAt(0)) + string.substring(1).toLowerCase();	
	}
	public static String removeStart(String string, int amount)
	{
		if(amount < 1)
		{
			throw new ArgumentException("the amount to be removed has to be bigger than 0");
		}
		if(string.length() < amount)
		{
			throw new ArgumentException("the amount to be removed cannot be bigger than the length of the string");
		}
		if(string.length() == amount)
		{
			return "";
		}
		return string.substring(amount);
	}
	public static String removeEnd(String string, int amount)
	{
		if(amount < 1)
		{
			throw new ArgumentException("the amount to be removed has to be bigger than 0");
		}
		if(string.length() <= amount)
		{
			return "";
		}
		return string.substring(0, string.length() - amount);
	}
//	<< MANIPULATION <<
	
//	>> CHECK >>
	public static boolean isPositive(String string)
	{
		if(isNumber(string))
		{
			return string.trim().charAt(0) != '-';
		}
		throw new ArgumentException("the string \"" + string + "\" isn't a numeric value", ZString.class, "isNumber", String.class);
	}
	public static boolean isFloatingPoint(String string)
	{
//		returns true when a string is definitely a floating point, meaning x*.x*
		return string.trim().matches(REGEX_FLOATINGPOINT);
	}
	public static boolean isBinary(String string)
	{
		return string.matches(REGEX_BINARY);
	}
	public static boolean isColor(String code)
	{
		return ZString.isHexColor(code) || ZString.isRGBAColor(code);
	}
	public static boolean isRGBAColor(String code)
	{
		return code.trim().matches(REGEX_RGBA);
	}
	public static boolean isHexColor(String code)
	{
		return code.trim().matches(REGEX_HEX);
	} 
	public static boolean isMailAdress(String string)
	{
		return string.matches(REGEX_MAIL);
	}
	public static boolean isTelNumber(String string)
	{
		string = string.replaceAll("[^0-9&\\+]|\\s", "");
		return string.matches(REGEX_TEL);
	}
	public static boolean isNumber(String string)
	{
		return string.matches(REGEX_NUMBER);
	}
//	<< CHECK <<
}
