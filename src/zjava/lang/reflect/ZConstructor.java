package zjava.lang.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import zjava.errors.ArgumentException;
import zjava.errors.ZException;
import zjava.errors.UnexpectedException;
import zjava.lang.utils.ZString;

public class ZConstructor<T> extends ZExe
{
	private final Constructor<T>
		constructor;
	
//	>> CONSTRUCTOR >>
	ZConstructor(Constructor<T> constructor)
	{
		super(constructor);
		this.constructor = constructor;
	}
//	<< CONSTRUCTOR <<
	
//	>> INVOKE >>
	public T invoke(Object... arguments)
	{
		if(!this.getDeclaringClass().isAbstract())
		{
			this.compareArgs(arguments);
			try
			{
				boolean access = this.constructor.isAccessible();
				this.constructor.setAccessible(true);
				T instance = this.constructor.newInstance(arguments);
				this.constructor.setAccessible(access);
				return instance;
			} 
			catch (IllegalAccessException | IllegalArgumentException e)
			{
				throw new UnexpectedException("the constructor " + this.getName() + " could not be executed", e);
			}
			catch(InvocationTargetException | InstantiationException | ExceptionInInitializerError e)
			{
				ZException.throwUnchecked(e.getCause());
				throw new UnexpectedException("should not be able to reach this point");
			}
		}
		throw new ArgumentException("cannot create an instance of an abstract class");
	}
//	<< INVOKE <<

//	>> INFO >>
	@Override
	public String getFullName()
	{
		String name = this.getDeclaringClass().getName() + "(";
		for(ZClass<?> clazz : this.getParamTypes())
		{
			name += clazz.getSimpleName() + ", ";
		}
		return ZString.removeEnd(name, 2) + ")";
	}
//	<< INFO <<
}
