package zjava.lang.reflect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import zjava.ZJava;
import zjava.errors.ArgumentException;
import zjava.errors.ZException;
import zjava.errors.UnexpectedException;

public class ZMethod extends ZExe
{
	private final Method
		method;
	
//	>> CONSTRUCTOR >>
	ZMethod(Method method)
	{
		super(method);
		this.method = method;
	}
//	<< CONSTRUCTOR <<
	
//	>> PRIVATE SECTION >>
	private Object internInvoke(Object instance, Object... args)
	{
		this.compareArgs(args);
		
		try
		{
			boolean access = this.method.isAccessible();
			this.method.setAccessible(true);
			Object value = this.method.invoke(instance, args);
			this.method.setAccessible(access);
			return value;
		} 
		catch (IllegalAccessException | IllegalArgumentException e)
		{
			throw new UnexpectedException("could not access method: " + this.getName(), e);
		}
		catch(InvocationTargetException | ExceptionInInitializerError e)
		{
			ZException.throwUnchecked(e.getCause());
			throw new UnexpectedException("unreachable code");
		}
	}
//	<< PRIVATE SECTION <<
	
//	>> INVOKE >>
	public Object staticInvoke(Object... arguments)
	{
		if(!this.isStatic())
		{
			throw new ArgumentException("method is not static, cannot be called without an instance");
		}
		return this.internInvoke(null, arguments);
	}
	public Object invoke(Object instance, Object... arguments)
	{
		return this.internInvoke(instance, arguments);
	}
//	<< INVOKE <<
	
//	>> INFO >>
	public ZClass<?> getReturnType()
	{
		return ZClass.get(this.method.getReturnType());
	}
	public String getFullName()
	{
		String name = this.getDeclaringClass().getName() + "." + this.getName() + "(";
		for(ZClass<?> clazz : this.getParamTypes())
		{
			name += clazz.getSimpleName() + " "; 
		}
		name = name.trim() + "):" + this.getReturnType().getSimpleName();
		return name;
	}
//	<< INFO <<
	
//	>> STATE >>
	public boolean isBridge()
	{
		return this.method.isBridge();
	}
	public boolean isDefault()
	{
		return this.method.isDefault();
	}
//	<< STATE <<
	
//	>> TRANSFORM >>
	public Method toMethod()
	{
		return this.method;
	}
//	<< TRANSFORM <<
	
//	>> IMPLEMENTATIONS >>
	@Override
	public String getDoku()
	{
		return ZJava.ToString.getStart() + this.getFullName() + ZJava.ToString.getEnd();
	}
//	<< IMPLEMENTATIONS <<
}
