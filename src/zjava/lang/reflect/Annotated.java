package zjava.lang.reflect;

import java.lang.annotation.Annotation;

import zjava.ZObject;
import zjava.errors.ArgumentException;
import zjava.lang.types.collections.ZList;
import zjava.lang.utils.Ray;

abstract class Annotated extends ZObject
{
	private final Annotation[]
		annotations;
	
	protected Annotated(Annotation[] annotations)
	{
		this.annotations = annotations;
	}
	
//	>> HAS >>
	public boolean isAnnotatedBy(Class<? extends Annotation> annotation)
	{
		return Ray.contains(this.annotations, true, ann -> tf(annotation).is(ann.annotationType()));
	}
//	<< HAS <<
	
//	>> GET ANNOTATIONS >>
	public <P extends Annotation> P getAnnotation(Class<P> annotation)
	{
		for(Annotation current : this.annotations)
		{
			if(tf(annotation).is(current.annotationType()))
			{
				@SuppressWarnings("unchecked") P found = (P) current;
				return found;
			}
		}
		throw new ArgumentException("annotation not present: " + tf(annotation));
	}
	public <P extends Annotation> P[] getAnnotations(Class<P> annotation)
	{
		ZList<P> annotations = new ZList<>();
		for(Annotation current : this.annotations)
		{
			if(tf(annotation).is(current.annotationType()))
			{
				@SuppressWarnings("unchecked") P found = (P) current;
				annotations.add(found);
			}
		}
		return annotations.toArray(annotation);
	}
//	<< GET ANNOTATIONS <<
	
//	>> GET CLASS >>
	public ZClass<? extends Annotation>[] getAnnotationClasses()
	{
		@SuppressWarnings("unchecked") ZClass<? extends Annotation>[] array =  Ray.extract(this.annotations, annotation -> {
			@SuppressWarnings("rawtypes") Type clazz = annotation.getClass();
			return Type.get(clazz);
		}).toArray(ZClass.class);
		return array;
	}
//	<< GET CLASS <<
}
