package zjava.lang.reflect.n2;

import java.net.MalformedURLException;
import java.net.URL;
import zjava.ZObject;
import zjava.errors.ArgumentException;
import zjava.lang.types.collections.ZMap;

public class RefPack extends ZObject
{
	private static final ZMap<Package, RefPack>
		PACKAGES = new ZMap<>();
	
//	>> STATIC SECTION >>
	public static RefPack get(Package pack)
	{
		if(!self(PACKAGES).containsKey(notNull(pack)))
		{
			RefPack rPack = new RefPack(pack);
			self(PACKAGES).set(pack, rPack);
			return rPack;
		}
		return self(PACKAGES).get(pack);
	}
	public static RefPack get(String name)
	{
		Package pack = Package.getPackage(name);
		if(null == pack)
		{
			throw new ArgumentException("no package found: \"" + name + "\"");
		}
		return self(get(pack));
	}
//	<< STATIC SECTION <<
	
	private final Package
		pack;
	
//	>> CONSTRUCTORS >>
	private RefPack(Package pack)
	{
		this.pack = pack;
	}
//	<< CONSTRUCTORS <<
	
//	>> GETTERS >>
	public String getName()
	{
		return this.pack.getName();
	}
//	<< GETTERS <<
	
//	>> VERSION >>
	public String getSpecVersion()
	{
		return this.pack.getSpecificationVersion();
	}
	public String getImplVersion()
	{
		return this.pack.getImplementationVersion();
	}
//	<< VERSION <<
	
//	>> TITLE >>
	public String getSpecTitle()
	{
		return this.pack.getSpecificationTitle();
	}
	public String getImplTitle()
	{
		return this.pack.getImplementationTitle();
	}
//	<< TITLE <<
	
//	>> VENDOR >>
	public String getSpecVendor()
	{
		return this.pack.getSpecificationVendor();
	}
	public String getImplVendor()
	{
		return this.pack.getImplementationVendor();
	}
//	<< VENDOR <<
	
//	>> IS >>
	public boolean isSealed()
	{
		return this.pack.isSealed();
	}
	public boolean isSealedURL(String url)
	{
		try
		{
			return this.pack.isSealed(new URL(url));
		}
		catch(MalformedURLException e)
		{
			throw new ArgumentException("String \"" + url + "\" is not an URL");
		}
	}
//	<< IS <<
}
