package zjava.lang.reflect.n2;

import zjava.ZObject;
import zjava.Constant.ClassType;
import zjava.errors.ArgumentException;
import zjava.lang.types.collections.ZMap;
import zjava.lang.utils.ZString;

public class Type<T> extends ZObject
{
	private static final ZMap<Class<?>, Type<?>>
		CLASSES = new ZMap<>();
	
//	>> STATIC SECTION >>
	public static <P> Type<P> get(Class<P> clazz)
	{
		if(CLASSES.containsKey(notNull(clazz)))
		{
			Type<P> refClass = new Type<>(clazz);
			CLASSES.set(clazz, refClass);
			return refClass;
		}
		@SuppressWarnings("unchecked") Type<P> refClass = (Type<P>) CLASSES.get(clazz);
		return refClass;
	}
	public static Type<?> get(String name)
	{
		if(name.matches("*.*"))
		{
			switch(name)
			{
				case "void": 
					return get(void.class);
				case "short": 
					return get(short.class);
				case "byte": 
					return get(int.class);
				case "int": 
					return get(int.class);
				case "long": 
					return get(long.class);
				case "float": 
					return get(float.class);
				case "double": 
					return get(double.class);
				case "char": 
					return get(char.class);
				case "boolean": 
					return get(boolean.class);
			}
		}
		try
		{
			return get(Class.forName(name));
		}
		catch(ClassNotFoundException e)
		{
			throw new ArgumentException("Class not found: \"" + name + "\"", e);
		}
	}
//	<< STATIC SECTION <<

	private final Class<T>
		clazz;
	
	private final ClassType
		type;
	
	private final RefPack
		pack;
	
	private final String
		name;

//	>> CONSTRUCTORS >> 
	private Type(Class<T> clazz)
	{
		this.clazz = clazz;
		this.pack = RefPack.get(clazz.getPackage());
		this.type = ClassType.typeOf(this.clazz);
		this.name = this.initName();
	}
//	<< CONSTRUCTORS <<
	
//	>> GETTERS >>
	public String getName()
	{
		return (null == this.pack ? "" : this.pack.getName() + ".") + this.name;
	}
	public String getSimpleName()
	{
		return this.name;
	}
//	<< GETTERS <<
	
//	>> IS >>
	public boolean isPrimitive()
	{
		return this.def().isPrimitive();
	}
	public boolean isArray()
	{
		return this.def().isArray();
	}
	public boolean isAnonymus()
	{
		// new Type() {}; 
		return this.def().isAnonymousClass();
	}
	public boolean isMember()
	{
		//non-static inner class
		return this.def().isMemberClass();
	}
	public boolean isLocal()
	{
		//declared in method
		return this.def().isLocalClass();
	}
	public boolean isSynthetic()
	{
		//created by compiler, never specified in source
		return this.def().isSynthetic();
	}
//	<< IS <<
	
//	>> TYPE >>
	public ClassType getType()
	{
		return this.type;
	}
	public boolean isClass()
	{
		return this.getType() == ClassType.CLASS;
	}
	public boolean isInterface()
	{
		return this.getType() == ClassType.INTERFACE;
	}
	public boolean isAnnotation()
	{
		return this.getType() == ClassType.ANNOTATION;
	}
	public boolean isEnum()
	{
		return this.getType() == ClassType.ENUM;
	}
	public boolean isFunctional()
	{
		return this.getType() == ClassType.FUNCTIONAL;
	}
//	<< TYPE <<
	
//	>> TRANSFORM >>
	public Class<T> toClass()
	{
		return this.clazz;
	}
	@Override
	public String toString()
	{
		return this.getName() + ":" + this.getType().name().toLowerCase();
	}
//	<< TRANSFORM <<
	
//	>> PRIVATE SECTION >>
	private Class<T> def()
	{
		return this.clazz;
	}
	private String createName(String type, String name)
	{
		return "{" + type + "}:" + type;
	}
//	<< PRIVATE SECTION <<
	
//	>> INITIALIZERS >>
	private String initName()
	{
		if(this.isPrimitive())
		{
			return this.clazz.getName();
		}
		if(this.isFunctional())
		{
			return this.createName("functional", ZString.removeEnd(ZString.getMatch(this.clazz.getSimpleName(), "([A-z]|[0-9]|_)+\\$\\$"), 2));
		}
		else if(this.isArray())
		{
			Class<?> arrayClass = this.clazz;
			
			String brackets = "";
			Class<?> lastClass = arrayClass;
			arrayClass = arrayClass.getComponentType();
			while(null != arrayClass)
			{
				brackets += "[]";
				lastClass = arrayClass;
				arrayClass = arrayClass.getComponentType();
			}
			return this.createName("{array}", lastClass.getName() + brackets);
		}
		else if(this.isAnonymous())
		{
			return this.clazz.getSuperclass().getSimpleName() + "{}";
		}
		else if(this.isMember())
		{
			return this.clazz.getDeclaringClass().getSimpleName() + "." + this.clazz.getSimpleName();
		}
		return this.clazz.getName();
	}
//	<< INITIALIZERS <<
}
