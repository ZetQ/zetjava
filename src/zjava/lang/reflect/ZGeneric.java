package zjava.lang.reflect;

import java.lang.annotation.Annotation;
import java.lang.reflect.GenericDeclaration;

import zjava.lang.utils.Ray;

public abstract class ZGeneric extends Modified
{
	private final GenericType[]
		types;
	
	private final boolean
		isGeneric;
	
	protected ZGeneric(Annotation[] annotations, int modifiers, GenericDeclaration generic)
	{
		super(annotations, modifiers);
		this.types = Ray.extract(generic.getTypeParameters(), param -> new GenericType(param, this)).toArray(GenericType.class);
		this.isGeneric = this.types.length > 0;
	}
	
	public boolean isGeneric()
	{
		return this.isGeneric;
	}
	public GenericType[] getGenericTypes()
	{
		return Ray.copy(this.types);
	}
}
