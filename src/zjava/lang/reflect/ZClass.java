package zjava.lang.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import zjava.Constant.ClassType;
import zjava.errors.ArgumentException;
import zjava.lang.functionals.Eval;
import zjava.lang.types.collections.Array;
import zjava.lang.types.collections.ZList;
import zjava.lang.types.collections.ZMap;
import zjava.lang.utils.Ray;
import zjava.lang.utils.ZString;

public class ZClass<T> extends ZGeneric
{	
	private static final ZMap<Class<?>, ZClass<?>>
		CLASSES = new ZMap<>();
	
	public static <T> ZClass<T> get(Class<T> clazz)
	{
		if(null == clazz) return null;
		
		if(!CLASSES.containsKey(clazz))
		{
			ZClass<T> zclass = new ZClass<T>(clazz);
			CLASSES.set(clazz, zclass);
			return zclass;
		}
		else
		{
			@SuppressWarnings("unchecked") ZClass<T> zclass = (ZClass<T>) CLASSES.get(clazz);
			return zclass;
		}
	}
	public static ZClass<?> get(String name)
	{
		switch(name)
		{
			case "void": return ZClass.get(void.class);
			case "short": return ZClass.get(short.class);
			case "byte": return ZClass.get(int.class);
			case "int": return ZClass.get(int.class);
			case "long": return ZClass.get(long.class);
			case "float": return ZClass.get(float.class);
			case "double": return ZClass.get(double.class);
			case "char": return ZClass.get(char.class);
			case "boolean": return ZClass.get(boolean.class);
		}
		try
		{
			return ZClass.get(Class.forName(name));
		} 
		catch (ClassNotFoundException e)
		{
			throw new ArgumentException("class not found: " + name);
		}
	}
	public static <I, O extends I> O convert(I object)
	{
		@SuppressWarnings("unchecked") O output = (O) object;
		return output;
	}
	
	private final Class<T>
		clazz;
	
	private final ZPackage
		pack;
	
	private final Array<ZClass<?>>
		interfaces;
	
	private final Array<ZField>
		fields;
	
	private final Array<ZConstructor<T>>
		constructors;
	
	private final Array<ZMethod>
		methods;
	
	private final ClassType
		type;
	
	private final String
		name;
	
	//Used in exe-checking
	private boolean
		nameFound;
	
//	>> CONSTRUCTOR >>

	private ZClass(Class<T> clazz)
	{
		super(clazz.getAnnotations(), clazz.getModifiers(), clazz);
		this.clazz = clazz;
		this.type = ClassType.typeOf(clazz);
		Package pack = clazz.getPackage();
		this.pack = null == pack ? null : ZPackage.get(pack);
		this.interfaces = this.initInterfaces();
		this.fields = this.initFields();
		this.methods = this.initMethods();
		@SuppressWarnings({ "unchecked", "rawtypes" }) ZConstructor<T>[] array = Ray.getBy(clazz.getDeclaredConstructors(), cstr -> (ZConstructor) new ZConstructor<>(cstr)).toArray(ZConstructor.class);
		this.constructors = array;
		this.name = this.initName();
	}
//	<< CONSTRUCTOR <<
	
//	>> INITIALIZERS >>
	private ZField[] initFields()
	{
		ZList<ZField> list = new ZList<>();
		ZClass<?> current = this;
		while(null != current)
		{
			for(Field field : current.toClass().getDeclaredFields())
			{
				list.add(new ZField(field, current));
			}
			current = current.getParent();
		}
		return list.toArray(ZField.class);
	}
	private Array<ZMethod> initMethods()
	{
		ZList<ZMethod> list = new ZList<>();
		Class<?> current = this.clazz;
		while(null != current)
		{
			for(Method method : current.getDeclaredMethods())
			{
				method.get
				list.add(new ZMethod(method));
			}
			current = current.getSuperclass();
		}
		return list.toArray();
	}
	private Array<ZClass<?>> initInterfaces()
	{
		ZList<ZClass<?>> list = new ZList<>();
		for(Class<?> iface : this.clazz.getInterfaces())
		{
			list.add(get(iface));
		}
		return list.toArray();
	}
	private String initName()
	{
		if(this.isLambda())	
		{
			return ZString.removeEnd(ZString.getMatch(this.clazz.getSimpleName(), "([A-z]|[0-9]|_)+\\$\\$"), 2) + "{functional}";
		}
		else if(this.clazz.isArray())
		{
			Class<?> arrayClass = this.clazz;
			
			String brackets = "";
			Class<?> lastClass = arrayClass;
			arrayClass = arrayClass.getComponentType();
			while(null != arrayClass)
			{
				brackets += "[]";
				lastClass = arrayClass;
				arrayClass = arrayClass.getComponentType();
			}
			return lastClass.getSimpleName() + brackets;
		}
		else if(this.isAnonymous())
		{
			return this.clazz.getSuperclass().getSimpleName() + "{}";
		}
		else if(this.isMember())
		{
			return this.clazz.getDeclaringClass().getSimpleName() + "." + this.clazz.getSimpleName();
		}
		return this.clazz.getSimpleName();
	}
//	<< INITIALIZERS <<
	
//	>> PRIVATE SECTION >>
	public <P extends ZExe> P getExe(P[] execs, String name, Class<?>[] params)
	{
		this.nameFound = false;
		outer: for(P exe : execs)
		{
			if(exe.getName().equals(name))
			{
				this.nameFound = true;
				inner: for(ZClass<?> arg : exe.getParamTypes())
				{
					for(Class<?> param : params)
					{
						if(arg.parentOf(param))
						{
							continue inner;
						}
					}
					continue outer;
				}
				return exe;
			}
		}
		return null;
	}
//	<< PRIVATE SECTION <<
	
//	>> MISC >>
	public T cast(Object object) throws ArgumentException
	{
		if(null == object)
		{
			return null;
		}
		else
		{
			ZClass<?> reference = this.isPrimitive() ? this.getSibling() : this;
			if(reference.isTypeOf(object))
			{
				try
				{
					@SuppressWarnings("unchecked") T instance = (T)reference.clazz.cast(object);
					return instance;
				}
				catch(ClassCastException e)
				{
					throw new ArgumentException("unable to cast object", e);
				}
			}
			throw new ArgumentException("cannot cast instance of " + object.getClass().getName() + " to " + this.getName());
		}
	}
	public ZPackage getPackage()
	{
		return this.pack;
	}
	public ZClass<?> getArrayType()
	{
		return get(this.clazz.getComponentType());
	}
	public ZClass<?> getNonArray()
	{
		Class<?> clazz = this.clazz;
		while(clazz.isArray())
		{
			clazz = clazz.getComponentType();
		}
		return get(clazz);
	}
//	<< MISC <<
	
//	>> TYPE SPECIFIC >>
	public T[] getEnumConstants()
	{
		return this.clazz.getEnumConstants();
	}
//	<< TYPE SPECIFIC <<
	
//	>> INNER CLASS >>
	public ZClass<?> getDeclaringClass()
	{
		return get(this.clazz.getDeclaringClass());
	}
	//TODO declaring constructor / method
//	<< INNER CLASS <<
	
//	>> FIELDS >>
	public ZField[] getFields()
	{
		return Ray.copy(this.fields);
	}
	public boolean hasField(String name)
	{
		return Ray.contains(this.fields, true, field -> field.getName().equals(name));
	}
	public ZField getField(String name)
	{
		for(ZField field : this.fields)
		{
			if(field.getName().equals(name))
			{
				return field;
			}
		}
		throw new ArgumentException("no field that matches name: " + name);
	}
	public ZField[] getFields(Eval<ZField> eval)
	{
		return Ray.where(this.fields, eval).toArray(ZField.class);
	}
//	<< FIELDS <<
	
//	>> CONSTRUCTORS >>
	public ZConstructor<T>[] getConstructors()
	{
		return Ray.copy(this.constructors);
	}
	public boolean hasConstructor(Class<?>... params)
	{
		return null != this.getExe(this.constructors, this.constructors[0].getName(), params);
	}
	public ZConstructor<T> getConstructor(Class<?>... params)
	{
		ZConstructor<T> cstr = this.getExe(this.constructors, this.getSimpleName(), params);
		if(null == cstr) throw new ArgumentException("no constructor that matches parameters");
		return cstr;
	}
//	<< CONSTRUCTORS <<
	
//	>> METHODS >>
	public Object invokeStaticMethod(String method, Object... args)
	{
		@SuppressWarnings("rawtypes") ZMethod m = this.getMethod(method, 
					Ray.extract(args, arg -> (Class) arg.getClass()).toArray(Class.class));
		return m.staticInvoke(args);
	}
	public Object invokeMethod(Object instance, String method, Object... args)
	{
		@SuppressWarnings("rawtypes") ZMethod m = this.getMethod(method,
					Ray.extract(args, arg -> (Class) arg.getClass()).toArray(Class.class));
		return m.invoke(instance, args);
	}
	public ZMethod[] getMethods()
	{
		return Ray.copy(this.methods);
	}
	public boolean hasMethod(String name, Class<?>... params)
	{
		return null != this.getExe(this.methods, name, params);
	}
	public ZMethod getMethod(String name, Class<?>... params)
	{
		ZMethod method = this.getExe(this.methods, name, params);
		if(null == method) throw new ArgumentException(nameFound ? "no method that matches name: " + name : "no method that matches parameters");
		return method;
	}
	public ZMethod[] getMethods(Eval<ZMethod> eval)
	{
		return Ray.where(this.methods, eval).toArray(ZMethod.class);
	}
//	<< METHODS <<
	
//	>> INTERFACES >>
	public ZClass<?>[] getInterfaces()
	{
		return Ray.copy(this.interfaces);
	}
	public ZClass<?>[] getInterfaces(Eval<ZClass<?>> eval)
	{
		@SuppressWarnings("rawtypes") ZList<ZClass> list = new ZList<>();
		for(ZClass<?> iface : this.interfaces)
		{
			if(eval.evaluate(iface))
			{
				list.add(iface);
			}
		}
		return list.toArray(ZClass.class);
	}
	public boolean isImplementationOf(ZClass<?> clazz)
	{
		return this.isImplementationOf(clazz.toClass());
	}
	public boolean isImplementationOf(Class<?> clazz)
	{
		if(clazz.isInterface())
		{
			return Ray.contains(this.interfaces, true, iface -> iface.toClass() == clazz);
		}
		return false;
	}
//	<< INTERFACES <<
	
//	>> NAME >>
	public String getName()
	{
		return (null == this.pack ? "" : this.pack.getName() + ".") + this.name;
	}
	public String getSimpleName()
	{
		return this.name;
	}
//	<< NAME <<
	
//	>> HIERARCHY >>
	public ZClass<? super T> getParent()
	{
		Class<? super T> clazz = this.clazz.getSuperclass();
		return null == clazz ? null : ZClass.get(clazz);
	}
	public boolean childOf(ZClass<?> clazz)
	{
		return this.childOf(clazz.toClass());
	}
	public boolean childOf(Class<?> clazz)
	{
		return clazz.isAssignableFrom(this.toClass());
	}
	public boolean parentOf(ZClass<?> clazz)
	{
		return this.parentOf(clazz.toClass());
	}
	public boolean parentOf(Class<?> clazz)
	{
		return this.toClass().isAssignableFrom(clazz);
	}
	public boolean is(Class<?> clazz)
	{
		return this.toClass() == clazz;
	}
	public boolean isTypeOf(Object object)
	{
		if(null == object)
		{
			return false;
		}
		return this.toClass().isInstance(object);
	}
	public ZClass<?> getSibling()
	{
		return this.isPrimitive() ? 
		// PRIMITIVE
		this.clazz == byte.class ? get(Byte.class) :
		this.clazz == short.class ? get(Short.class) :
		this.clazz == int.class ? get(Integer.class) :
		this.clazz == long.class ? get(Long.class) :
		this.clazz == float.class ? get(Float.class) :
		this.clazz == double.class ? get(Double.class) :
		this.clazz == char.class ? get(Character.class) :
	    this.clazz == boolean.class ? get(Boolean.class) :
		this.clazz == void.class ? get(Void.class) : this :
		// NOT PRIMITIVE
   	    this.clazz == Byte.class ? get(byte.class) :
	    this.clazz == Short.class ? get(short.class) :
	    this.clazz == Integer.class ? get(int.class) :
	    this.clazz == Long.class ? get(long.class) :
	    this.clazz == Float.class ? get(float.class) :
	    this.clazz == Double.class ? get(double.class) :
	    this.clazz == Character.class ? get(char.class) :
	    this.clazz == Boolean.class ? get(boolean.class) :
	    this.clazz == Void.class ? get(void.class) : this;
	}
//	<< HIERARCHY <<
	
//	>> STATE >>
	public boolean isPrimitive()
	{
		return this.toClass().isPrimitive();
	}
	public boolean isArray()
	{
		return this.toClass().isArray();
	}
	public boolean isAnonymous()
	{
		// anonymous =>  new Type() { /* doStuff */ } 
		return this.toClass().isAnonymousClass();
	}
	public boolean isMember()
	{
		// member => non-static inner class
		return this.toClass().isMemberClass();
	}
	public boolean isLocal()
	{
		// local => declared in a method
		return this.toClass().isLocalClass();
	}
	public boolean isSynthetic()
	{
		// synthetic => created by compiler, not mentioned in code
		return this.toClass().isSynthetic();
	}
//	<< STATE <<
	
//	>> TYPE >>
	public ClassType getType()
	{
		return this.type;
	}
	public boolean isClass()
	{
		return ClassType.CLASS == this.type;
	}
	public boolean isInterface()
	{
		return ClassType.INTERFACE == this.type;
	}
	public boolean isAnnotation()
	{
		return ClassType.ANNOTATION == this.type;
	}
	public boolean isEnum()
	{
		return ClassType.ENUM == this.type;
	}
	public boolean isLambda()
	{
		return ClassType.LAMBDA == this.type;
	}
//	<< TYPE <<
	
//	>> TRANSFORM >>
	public Class<T> toClass()
	{
		return this.clazz;
	}
	@Override
	public String toString()
	{
		return this.getName() + ":" + this.getType().name().toLowerCase();
	}
//	<< TRANSFORM <<
}
