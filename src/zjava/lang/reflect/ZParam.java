package zjava.lang.reflect;

import java.lang.reflect.Parameter;

import zjava.Constant.Modifier;

public class ZParam extends Annotated
{
	private final Parameter 
		param;
	
	private final ZExe
		exe;
	
//	>> CONSTRUCTOR >>
	ZParam(Parameter param, ZExe exe)
	{
		super(param.getAnnotations());
		this.param = param;
		this.exe = exe;
	}
//	<< CONSTRUCTOR <<
	
//	>> INFO >>
	public String getName()
	{
		return this.param.getName();
	}
	public ZClass<?> getType()
	{
		return ZClass.get(this.param.getType());
	}
	public ZExe getExecutable()
	{
		return this.exe;
	}
//	<< INFO <<
	
//	>> STATE >>
	public boolean isFinal()
	{
		return Modifier.FINAL.isFor(this.param.getModifiers());
	}
	public boolean hasName()
	{
		return this.param.isNamePresent();
	}
	public boolean isGeneric()
	{
		return !this.param.getParameterizedType().getTypeName().equals(this.param.getType().getName());
	}
	public boolean isImplicit()
	{
		return this.param.isImplicit();
	}
	public boolean isSynthetic()
	{
		return this.param.isSynthetic();
	}
	public boolean isVarArgs()
	{
		return this.param.isVarArgs();
	}
//	<< STATE <<
}
