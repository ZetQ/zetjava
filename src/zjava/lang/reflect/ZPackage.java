package zjava.lang.reflect;

import zjava.ZObject;
import zjava.errors.ArgumentException;
import zjava.lang.types.collections.ZMap;

public class ZPackage extends ZObject
{
	private static final ZMap<Package, ZPackage>
		PACKAGES = new ZMap<>();
	
	public static ZPackage get(Package pack)
	{
		pack = notNull(pack);
		if(!PACKAGES.containsKey(pack))
		{
			ZPackage zpack = new ZPackage(pack);
			PACKAGES.add(pack, zpack);
			return zpack;
		}
		return PACKAGES.get(pack);
	}
	public static ZPackage get(String name)
	{
		Package pack = Package.getPackage(name);
		if(null == pack)
		{
			throw new ArgumentException("no package found");
		}
		return self(get(pack));
	}
	
	private final Package
		pack;
	
	private ZPackage(Package pack)
	{
		this.pack = pack;
	}
	
//	>> INFO >>
	public String getName()
	{
		return this.pack.getName();
	}
//	<< INFO <<
}
