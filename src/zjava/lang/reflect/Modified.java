package zjava.lang.reflect;

import java.lang.annotation.Annotation;

import zjava.Constant.Modifier;
import zjava.lang.utils.Ray;

abstract class Modified extends Visible
{
	private final Modifier[]
		modifiers;
	
	private final int
		intMods;
	
	protected Modified(Annotation[] annotations, int modifiers)
	{
		super(annotations, modifiers);
		this.intMods = modifiers;
		this.modifiers = Modifier.modiferFor(modifiers);
	}
	
	public Modifier[] getModifiers()
	{
		return Ray.copy(this.modifiers);
	}
	public boolean isModifiedBy(Modifier... modifiers)
	{
		return Ray.contains(this.modifiers, modifiers);
	}
	public boolean isAbstract()
	{
		return Modifier.ABSTRACT.isFor(this.intMods);
	}
	public boolean isStatic()
	{
		return Modifier.STATIC.isFor(this.intMods);
	}
	public boolean isFinal()
	{
		return Modifier.FINAL.isFor(this.intMods); 
	}
	public boolean isSynchronized()
	{
		return Modifier.SYNCHRONIZED.isFor(this.intMods); 
	}
	public boolean isVolatile()
	{
		return Modifier.VOLATILE.isFor(this.intMods); 
	}
	public boolean isTransient()
	{
		return Modifier.TRANSIENT.isFor(this.intMods); 
	}
	public boolean isNative()
	{
		return Modifier.NATIVE.isFor(this.intMods); 
	}
	public boolean isStrict()
	{
		return Modifier.STRICT.isFor(this.intMods); 
	}
}
