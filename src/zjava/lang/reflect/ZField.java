package zjava.lang.reflect;

import java.lang.reflect.Field;

import zjava.errors.ArgumentException;
import zjava.errors.UnexpectedException;

public class ZField extends Modified
{
	private final Field
		field;
	
	private final ZClass<?>
		declaringClass;
	
	private final String
		name;
	
	ZField(Field field, ZClass<?> declaringClass)
	{
		super(field.getAnnotations(), field.getModifiers());
		this.field = field;
		this.declaringClass = declaringClass;
		this.name = field.getName();
	}
	
//	>> PRIVATE SECTION >>
	private void internSet(Object value, Object instance)
	{
		if(null == instance && !this.isStatic())
		{
			throw new ArgumentException("non-static fields cannot be called without an instance of the declaring class");
		}
		if(!this.getType().isTypeOf(value))
		{
			throw new ArgumentException("type of new value doesn't match type of field");
		}
		
		try
		{
			boolean accessible = this.field.isAccessible();
			this.field.setAccessible(true);
			this.field.set(instance, value);
			this.field.setAccessible(accessible);
		} 
		catch (IllegalArgumentException | IllegalAccessException e)
		{
			throw new UnexpectedException("exception while setting value of field", e);
		}
	}
	private <P> P internGet(Object object)
	{
		try
		{
			if(null == object && !this.isStatic())
			{
				throw new ArgumentException("non-static fields cannot be called without an instance of the declaring class");
			}
			boolean accessible = this.field.isAccessible();
			this.field.setAccessible(true);
			@SuppressWarnings("unchecked") P value = (P) this.field.get(object);
			this.field.setAccessible(accessible);
			return value;
		} 
		catch (IllegalArgumentException | IllegalAccessException e)
		{
			throw new UnexpectedException("exception while getting value of field", e);
		}
	}
//	<< PRIVATE SECTION <<
	
//	>> SET >>
	public void setValue(Object value)
	{
		this.internSet(value, null);
	}
	public void setValue(Object value, Object instance)
	{
		if(!this.declaringClass.parentOf(instance.getClass()))
		{
			throw new ArgumentException("given object has to be an instance of the declaring class of the field");
		}
		this.internSet(value, instance);
	}
//	<< SET <<
	
//	>> GET >>
	public Object getValue()
	{
		return this.internGet(null);
	}
	public Object getValue(Object instance)
	{
		if(!this.declaringClass.parentOf(instance.getClass()))
		{
			throw new ArgumentException("given object has to be an instance of the declaring class of the field");
		}
		return this.internGet(instance);
	}
//	<< GET <<
	
//	>> INFOS >>
	public String getName()
	{
		return this.name;
	}
	public ZClass<?> getType()
	{
		return ZClass.get(this.field.getType());
	}
	public ZClass<?> getDeclaringClass()
	{
		return this.declaringClass;
	}
//	<< INFOS <<
	
//	>> STATUS >>
	public boolean isGeneric()
	{
		return !this.field.getGenericType().getTypeName().equals(this.field.getType().getName());
	}
	public boolean isEnumConstant()
	{
		return this.field.isEnumConstant();
	}
	public boolean isPrimitive()
	{
		return !this.getType().childOf(Object.class);
	}
//	<< STATUS <<
	
//	>> TRANSFORM >>
	public Field toField()
	{
		return this.field;
	}
//	<< TRANSFORM <<
}
