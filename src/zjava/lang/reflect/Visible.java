package zjava.lang.reflect;

import java.lang.annotation.Annotation;

import zjava.Constant.Visibility;

class Visible extends Annotated
{
	private final Visibility
		visibility;
	
	protected Visible(Annotation[] annotations, int modifiers)
	{
		super(annotations);
		this.visibility = Visibility.getFor(modifiers);
	}
	
	public Visibility getVisibility()
	{
		return this.visibility;
	}
}
