package zjava.lang.reflect;

public interface ZClassed 
{
	public default ZClass<?> getZClass()
	{
		return ZClass.get(this.getClass());
	}
}
