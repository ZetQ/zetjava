package zjava.lang.reflect;

import java.lang.reflect.Executable;

import zjava.errors.ArgumentException;
import zjava.lang.utils.Ray;

public abstract class ZExe extends ZGeneric
{
	private final Executable
		executable;
	
	private final ZParam[]
		params;
	
//	>> CONSTRUCTOR >>
	public ZExe(Executable executable)
	{
		super(executable.getAnnotations(), executable.getModifiers(), executable);
		
		this.executable = executable;
		this.params = Ray.extract(executable.getParameters(), param -> new ZParam(param, this)).toArray(ZParam.class);
	}
//	<< CONSTRUCTOR <<
	
//	>> PROTECTED SECTION >>
	protected void compareArgs(Object[] args)
	{
		ZClass<?>[] params = this.getParamTypes();
		if(params.length == args.length)
		{
			for(int i = 0; i < args.length; ++i)
			{
				Object current = args[i];
				if(null == current) continue;
				if(params[i].isTypeOf(current))
				{
					continue;
				}
				throw new ArgumentException("argument #" + (i+1) + " does not match the type " + params[i].getName());
			}
			return;
		}
		throw new ArgumentException("amount of parameters does not match: " + args.length + "/" + params.length);
	}
//	<< PROTECTED SECTION <<
	
//	>> PARAMETERS >>
	public int getParamAmount()
	{
		return this.executable.getParameterCount();
	}
	public ZClass<?>[] getParamTypes()
	{
		@SuppressWarnings("rawtypes")
		ZClass[] array = Ray.extract(this.executable.getParameterTypes(), (Class<?> clazz) -> { Type zclass = (Type) Type.get(clazz); return zclass; }).toArray(ZClass.class);
		return array;
	}
	public ZParam[] getParams()
	{
		return Ray.copy(this.params);
	}
//	<< PARAMETERS <<
	
//	>> INFO >>
	public String getName()
	{
		return this.executable.getName();
	}
	public abstract String getFullName();
	public ZClass<?> getDeclaringClass()
	{
		return ZClass.get(this.executable.getDeclaringClass());
	}
	public ZClass<? extends Exception>[] getExceptions()
	{
		@SuppressWarnings("unchecked") ZClass<? extends Exception>[] array = 
				Ray.forEach(this.executable.getExceptionTypes(), (Class<?> clazz) -> Type.get((Type<Exception>)clazz), (Class<ZClass<Exception>>)ZClass.get(Exception.class).getClass());
		return array;
	}
//	<< INFO <<
	
//	>> STATE >>
	public boolean isSynthetic()
	{
		return this.executable.isSynthetic();
	}
	public boolean isVarArgs()
	{
		return this.executable.isVarArgs();
	}
//	<< STATE <<
}
