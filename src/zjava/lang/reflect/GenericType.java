package zjava.lang.reflect;

import java.lang.reflect.TypeVariable;

import zjava.errors.UnexpectedException;
import zjava.lang.utils.Ray;

public class GenericType
{
	private final TypeVariable<?>
		variable;
	
	private final ZGeneric
		owner;
	
//	>> CONSTRUCTORS >>
	GenericType(TypeVariable<?> variable, ZGeneric owner)
	{
		this.variable = variable;
		this.owner = owner;
	}
//	<< CONSTRUCTORS <<
	
//	>> GETTERS >>
	public String getName()
	{
		return this.variable.getName();
	}
	public ZGeneric getGeneric()
	{
		return this.owner;
	}
	public ZClass<?>[] getBounds()
	{
		@SuppressWarnings("rawtypes")
		ZClass[] bounds = Ray.extract(this.variable.getBounds(), t -> {
			try {
				return (Type) Type.get(Type.forName(t.getTypeName().replaceAll("<.>", "")));
				} catch(ClassNotFoundException e) { throw new UnexpectedException("class not found: " + t.getTypeName()); }
		}).toArray(ZClass.class);
		return bounds;
	}
//	<< GETTERS <<
}
