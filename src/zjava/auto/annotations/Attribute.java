package zjava.auto.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Automatically converts the assigned field to a string, when {@code toString()} is called.
 * <br>Only works on objects that implement the {@link zjava.auto.ZAutoString}-interface in the right way.
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Attribute
{
	boolean showName() default true;
}

