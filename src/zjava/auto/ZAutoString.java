package zjava.auto;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import zjava.ZJava;
import zjava.auto.annotations.Attribute;
import zjava.lang.reflect.ZClassed;
import zjava.lang.reflect.ZField;
import zjava.lang.utils.ZString;

public interface ZAutoString extends ZClassed
{
	@Target({ElementType.FIELD})
	@Retention(RetentionPolicy.RUNTIME)
	public static @interface NotString {}
	
	public default String getDoku()
	{
		String add = "";
		String string = "";
		for(ZField field : this.getZClass().getFields(field -> field.isAnnotatedBy(Attribute.class)))
		{
			Attribute attribute = field.getAnnotation(Attribute.class);
			
			Object object = field.isStatic() ? field.getValue() : field.getValue(this);

			String name = field.getName();
			String content = ZString.convert(object);
			
			string += add;
			string += attribute.showName() ? name + ZJava.ToString.getEqual() : "";
			string += content;
			add = ZJava.ToString.getSeparation();
		}
		return ZJava.ToString.getStart() + string.trim() + ZJava.ToString.getEnd();
	}
	/**
	 * Replacement for the default {@code toString()}-method.
	 * <br>This method should only be called by {@code toString()}.
	 * 
	 * @return a String-representation of this object that shows all fields that are annotated with the {@link zjava.auto.annotations.Attribute}-Annotation.
	 */
	public default String getString()
	{
		String string = this.getStringVars();
		if(null != string)
		{
			String vars = this.getStringVars();
			if(vars == null)
			{
				vars = "";
			}
			else
			{
				vars = ZJava.ToString.getStart() + vars + ZJava.ToString.getEnd();
			}
			return this.refClass().getName() + vars;
		}
		return this.refClass().getName() + this.getDoku();
	}
	public default String getStringVars()
	{
		return null;
	}
	public default Class<?> refClass()
	{
		return this.getClass();
	}
}
