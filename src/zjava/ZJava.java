package zjava;

public final class ZJava
{		
	public static class ToString
	{
		private static final String
			START = "(",
			END = ")",
			EQUAL = "=",
			SEPARATION = ", ",
			COUNTER = "=>";
		
		public static String getStart()
		{
			return ToString.START;
		}
		public static String getEnd()
		{
			return ToString.END;
		}
		public static String getEqual()
		{
			return ToString.EQUAL;
		}
		public static String getSeparation()
		{
			return ToString.SEPARATION;
		}
		public static String getCounter()
		{
			return ToString.COUNTER;
		}
	}
	public static class Text
	{
		private static final String
			FONT_NAME = "Arial";
		
		private static final int
			FONT_SIZE = 16;
		
		public static String getFontName()
		{
			return Text.FONT_NAME;
		}
		public static int getFontSize()
		{
			return Text.FONT_SIZE;
		}
	}
}
