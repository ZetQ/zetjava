package zjava;

import zjava.errors.CodeException;

/**
 * Superclass for all classes that only provide static methods and aren't meant to be used as object.
 * <br>This class does not provide any additional methods or fields.
 * <br>Its only function is to throw an {@link zjava.errors.CodeException} in its constructor.
 * <br>
 * <h1>Note:</h1><ul> 
 * <li>the subclasses should still set the visibility of their constructor to <{@code private}.</li>
 * </ul>
 */
public abstract class StaticObject extends ZObject
{
	/**
	 * Constructor for {@link zjava.StaticObject }.
	 * <br>Will always throw an {@link zjava.errors.CodeException}, since classes that use this superclass shouldn't be used as objects.
	 */
	public StaticObject()
	{
		throw new CodeException("the class " + this.getClass() + " is a static class and therefore cannot be created");
	}
}
