package zjava;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.util.Iterator;
import java.util.UUID;

import zjava.errors.UnexpectedException;
import zjava.lang.reflect.ZClass;
import zjava.lang.types.collections.MapEntry;
import zjava.lang.types.collections.ZMap;
import zjava.lang.utils.ZString;

public class Console extends StaticObject
{
	private Console() {}
	
	private static final String
		WRITE_START = "\\{",
		WRITE_END = "\\}";
	
	/**
	 * Writes a String to the console.
	 * <br>You can insert objects into the String by writing the placeholder <i>{int}</i>.
	 * <br>Every occurrence of such a placeholder will be replaced by the corresponding object in the Object-array.
	 * <br>
	 * <p>
	 * <code>Console.write("{0} + {1} = {2}", 3, 4, 7);</code>
	 * <br>will result in {@code 3 + 4 = 7}.
	 * </p>
	 * <h3 class="subHeader">Note</h3>
	 * <ul type="disc">
	 * <li>placeholders can be used multiple times</li>
	 * <li>unused objects will be ignored</li>
	 * <li>unused placeholders (out of range) will be printed</li>
	 * <li>you can print a used placeholder by inserting it with another placeholder:
	 * <br><code>Console.write("{0}: {1}", "a placeholder", "{0}")</code> => a placeholder: {0}
	 * </li>
	 * </ul>.
	 * @param content -> String that will be printed. Can contain placeholders.
	 * @param objects -> array of objects that can replace placeholders.
	 */
	public static void write(String content, Object... objects)
	{
		content = notNull(content);
		ZMap<String, String> replacements = new ZMap<>();
		for(int i = 0; i < objects.length; ++i)
		{
			String uuid = UUID.randomUUID().toString();
			content = content.replaceAll(WRITE_START + i + WRITE_END, uuid);
			replacements.set(uuid, ZString.convert(objects[i]));
		}
		for(MapEntry<String, String> entry : replacements) content = content.replaceAll(entry.getKey(), entry.getVal());
		print(content);
		br();
	}
	
	/**
	 * Writes all arguments given to this method to the console.
	 * <br>The objects will be separated by ", ".
	 * <br>After writing, a break will be printed.
	 * @param objects -> the objects that should be printed
	 */
	public static void echo(Object... objects)
	{
		for(int i = 0; i < objects.length; ++i) 
			print(ZString.convert(objects[i]) + (i < objects.length -1 ? ", " : ""));
		br();
	}
	
	/**
	 * Writes a break ({@link System#lineSeparator()}) to the console.
	 */
	public static void br()
	{
		print(System.lineSeparator());
	}
	/**
	 * Writes a tabulator ({@code '\t'}) to the console.
	 */
	public static void tab()
	{
		print('\t');
	}

	/**
	 * Creates a dump of all arguments given to this method.
	 * <br>A dump is a String get represents the whole content of an object.
	 * <br>This string will get printed to the console.
	 * <h3 class="subHeader">classes that can be dumped:</h3>
	 * <ul type="disc">
	 * <li>all types of arrays, no matter if primitive or not</li>
	 * <li>{@link java.lang.Iterable}</li>
	 * <li>{@link zjava.lang.types.collections.ZMap}</li>
	 * <li>{@link java.awt.Shape}</li>
	 * </ul>
	 * 
	 * <h3 class="subHeader">Note:</h3>
	 * <ul type="disc">
	 * <li>If the method cannot find a way to dump it, the object will be printed with {@code echo(Object...)}</li>
	 * <li>after each dump, successful or not, a break will be printed</li>
	 * </ul>
	 * @param objects - objects that should be dumped to the console
	 */
	public static void dump(Object... objects) 
	{
		for(Object object : objects)
		{
			innerDump(object);
			br();
		}
	}
	
//	>> PRIVATE SECTION >>
	private static void print(Object object)
	{
		System.out.print(object);
	}
	private static void innerDump(Object object)
	{
		if(null == object) print("null");
		else
		{
			ZClass<?> clazz = ZClass.get(object.getClass()); 
			if(object.getClass().isArray())
			{
				print(clazz.getName() + ' ' + ZJava.ToString.getStart());
				clazz = clazz.getArrayType();
				if(clazz.isPrimitive())
				{
					if(clazz.is(byte.class))
					{
						byte[] array = (byte[]) object;
						for(int i = 0; i < array.length; ++i) dumpSeq(array[i], i, i >= array.length);
					}
					else if(clazz.is(short.class))
					{
						short[] array = (short[]) object;
						for(int i = 0; i < array.length; ++i) dumpSeq(array[i], i, i >= array.length);
					}
					else if(clazz.is(int.class))
					{
						int[] array = (int[]) object;
						for(int i = 0; i < array.length; ++i) dumpSeq(array[i], i, i >= array.length);
					}
					else if(clazz.is(float.class))
					{
						float[] array = (float[]) object;
						for(int i = 0; i < array.length; ++i) dumpSeq(array[i], i, i >= array.length);
					}
					else if(clazz.is(double.class))
					{
						double[] array = (double[]) object;
						for(int i = 0; i < array.length; ++i) dumpSeq(array[i], i, i >= array.length);
					}
					else if(clazz.is(char.class))
					{
						char[] array = (char[]) object;
						for(int i = 0; i < array.length; ++i) dumpSeq(array[i], i, i >= array.length);
					}
					else if(clazz.is(boolean.class))
					{
						boolean[] array = (boolean[]) object;
						for(int i = 0; i < array.length; ++i) dumpSeq(array[i], i, i >= array.length);
					}
					else throw new UnexpectedException("unchecked primitive array: " + clazz.getName());
				}
				else
				{
					Object[] array = (Object[]) object;
					for(int i = 0; i < array.length; ++i) dumpSeq(array[i], i, i >= array.length); 
				}
				br();
				print(ZJava.ToString.getEnd());
			}
			else if(object instanceof ZMap)
			{
				print(clazz.getName() + ' ' + ZJava.ToString.getStart());
				for(MapEntry<?, ?> entry : (ZMap<?, ?>) object)
				{
					br();
					print(entry.getKey() + " " + ZJava.ToString.getCounter() + " ");
					innerDump(entry.getVal());
				}
				br();
				print(ZJava.ToString.getEnd());
			}
			else if(object instanceof Iterable)
			{
				print(clazz.getName() + ' ' + ZJava.ToString.getStart());
				int i = 0;
				Iterator<?> iterator = ((Iterable<?>) object).iterator();
				while(iterator.hasNext())
				{
					dumpSeq(iterator.next(), i++, !iterator.hasNext());
				}
				br();
				print(ZJava.ToString.getEnd());
			}
			else if(object instanceof Shape)
			{
				print(clazz.getName() + ' ' + ZJava.ToString.getStart());
				
				PathIterator it = ((Shape)object).getPathIterator(new AffineTransform());
				double[] line = new double[6];
				while(!it.isDone())
				{
					br();
					switch(it.currentSegment(line))
					{
						case PathIterator.SEG_MOVETO:
						{
							print("MOVE to=(x=" + line[0] + ", y=" + line[1] + ")");
							break;
						}
						case PathIterator.SEG_LINETO:
						{
							print("LINE to=(x=" + line[0] + " y=" + line[1] + ")");
							break;
						}
						case PathIterator.SEG_QUADTO:
						{
							print("QUAD to=(x=" + line[2] + " y=" + line[3] + ") control=(" + "x=" + line[0] + " y=" + line[1] + ")");
							break;
						}
						case PathIterator.SEG_CUBICTO:
						{
							print("CUBIC to=(x=" + line[4] + " y=" + line[5] + ") control1=(" + "x=" + line[0] + " y=" + line[1] + ") control2=(" + "x=" + line[2] + " y=" + line[3] + ")");
							break;
						}
						case PathIterator.SEG_CLOSE:
						{
							print("CLOSE");
							break;
						}
					}
					it.next();
				}
				br();
				print(ZJava.ToString.getEnd());
			}
			else
			{
				print(object);
			}
		}
	}
	private static void dumpSeq(Object object, int index, boolean isLast)
	{
		br();
		print(index + " " + ZJava.ToString.getCounter() + " ");
		innerDump(object);
	}
}
