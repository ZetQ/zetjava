package zjava.display;

import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

import zjava.display.actions.ActiveAction;
import zjava.display.actions.ClickAction;
import zjava.display.actions.HoverAction;
import zjava.display.actions.KeyAction;
import zjava.display.actions.MotionAction;
import zjava.display.actions.WheelAction;
import zjava.display.actions.WinAction;
import zjava.display.dec.StringBoundsSetter;
import zjava.errors.UnexpectedException;
import zjava.lang.functionals.Consumer;
import zjava.lang.types.ZBounds;
import zjava.system.ZScreen;

public class ZWindow extends ZContainer 
					 implements StringBoundsSetter, ActiveAction<ZWindow>, ClickAction, KeyAction, MotionAction, WheelAction, WinAction<ZWindow>, HoverAction
{	
	private JFrame
		frame;
	
	private JPanel
		panel;
	
	private Consumer<ZWindow>
		onClose = (window) -> window.close();
		
	private final ListenerProvider<ZWindow>
		listener = new ListenerProvider<>(this, () -> this.getState(), () -> this.getLastState());
	
	private WinState
		lastState = null;
	
	private ZBounds
		winBounds = new ZBounds(); //saved bounds for fullscreen window
	
//	>> CONSTRUCTORS >>
	public ZWindow(String title)
	{
		this(title, "50%", "50%");
	}
	public ZWindow(String title, String width, String height)
	{
		this(title, "0", "0", width, height);
		this.center();
	}
	public ZWindow(String title, String x, String y, String width, String height)
	{
		int screenW = ZScreen.getWidth(), screenH = ZScreen.getHeight();
		this.initFrame(title, Unit.direct(x, screenW),  Unit.direct(y, screenH), 
							  Unit.direct(width, screenW), Unit.direct(height, screenH));
	}
//	<< CONSTRUCTORS <<
	
//	>> INITIALIZERS >>
	private void initFrame(String title, int x, int y, int width, int height)
	{
		this.frame = new JFrame(title);
		this.frame.setLocation(x, y);
		this.frame.setSize(width, height);
		this.panel = new JPanel();
		this.frame.add(this.panel);
		
		final ZWindow source = this;
		this.frame.addKeyListener(this.listener.forKey());
		this.frame.addMouseListener(this.listener.forMouse());
		this.frame.addMouseMotionListener(this.listener.forMove());
		this.frame.addMouseWheelListener(this.listener.forWheel());
		this.frame.addWindowStateListener(this.listener.forState());
		this.frame.addWindowListener(this.listener.forWindow());
		this.frame.addWindowListener(new WindowAdapter() { @Override public void windowClosing(WindowEvent e) { source.onClose.consume(source); }});
	}
//	<< INITIALIZERS <<
	
//	>> PRIVATE SECTION >>
	private void closeFrame()
	{
		this.frame.setVisible(false);
		this.frame.dispose();
	}
//	<< PRIVATE SECTION <<
	
//	>> PROTECTED SECTION >>
	protected JComponent useComponent()
	{
		return this.panel;
	}
//	<< PROTECTED SECTION <<
	
//	>> MISC >>
	public void center()
	{
		this.frame.setLocationRelativeTo(null);
	}
	public void redraw()
	{
		this.panel.repaint();
		this.frame.repaint();
	}
	public void setClose(Consumer<ZWindow> consumer)
	{
		this.onClose = consumer;
	}
	public void close()
	{
		this.closeFrame();
	}
//	<< MISC <<
	
//	>> TITLE >>
	public String getTitle()
	{
		return this.frame.getTitle();
	}
	public void setTitle(String title)
	{
		this.frame.setTitle(title);
	}
//	<< TITLE <<
	
//	>> WINDOW STATE >>
	public WinState getState()
	{
		if(!this.frame.isVisible())
		{
			return WinState.HIDDEN;
		}
		else
		{
			switch(this.frame.getExtendedState())
			{
				case JFrame.NORMAL:
				{
					return WinState.WINDOW;
				}
				case JFrame.ICONIFIED:
				{
					return WinState.MINIMIZED;
				}
				case JFrame.MAXIMIZED_BOTH:
				{
					if(this.frame.isUndecorated())
					{
						return WinState.FULLSCREEN;
					}
					return WinState.MAXIMIZED;
				}
			}
		}
		throw new UnexpectedException("unknown state of frame: " + this.frame.getExtendedState());
	}
	public void setState(WinState state)
	{
		WinState current = this.getState();
		if(state != current)
		{
			if(WinState.FULLSCREEN == current)
			{
				//fullscreen exit
				this.closeFrame();
				this.initFrame(this.getTitle(), this.winBounds.getX(), this.winBounds.getY(), this.winBounds.getWidth(), this.winBounds.getHeight());
			}
			
			switch(state)
			{
				case FULLSCREEN:
				{
					this.closeFrame();
					this.winBounds.setBounds(this);
					this.initFrame(this.getTitle(), this.getX(), this.getY(), this.getWidth(), this.getHeight());
					this.frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					this.frame.setUndecorated(true);
					break;
				}
				case HIDDEN:
				{
					this.frame.setVisible(false);
					break;
				}
				case MAXIMIZED:
				{
					this.frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					break;
				}
				case MINIMIZED:
				{
					this.frame.setExtendedState(JFrame.ICONIFIED);
					break;
				}
				case WINDOW:
				{
					this.frame.setExtendedState(JFrame.NORMAL);
					break;
				}
			}
			if(WinState.HIDDEN == current)
			{
				this.frame.setVisible(true);
			}
			this.lastState = current;
		}
	}
	public WinState getLastState()
	{
		return this.lastState;
	}
//	<< WINDOW STATE <<

//	>> GETTERS >>
	public ZBounds getInsets()
	{
		Insets in = this.frame.getInsets();
		return new ZBounds(0, 0, this.getWidth() - in.left - in.right, this.getHeight() - in.top - in.bottom);
	}
//	<< GETTERS <<
	
//	>> ACTIONS >>
	@Override
	public ZAction onEnter(Consumer<MouseState> consumer)
	{
		return this.listener.onEnter(consumer);
	}
	@Override
	public ZAction onLeave(Consumer<MouseState> consumer)
	{
		return this.listener.onLeave(consumer);
	}
	@Override
	public ZAction onStateTo(WinState state, Consumer<ZWindow> consumer)
	{
		return this.listener.onStateTo(state, consumer);
	}
	@Override
	public ZAction onStateFrom(WinState state, Consumer<ZWindow> consumer)
	{
		return this.listener.onStateFrom(state, consumer);
	}
	@Override
	public ZAction onClose(Consumer<ZWindow> consumer)
	{
		return this.listener.onClose(consumer);
	}
	@Override
	public ZAction onOpen(Consumer<ZWindow> consumer)
	{
		return this.listener.onOpen(consumer);
	}
	@Override
	public ZAction onWheelUp(Consumer<WheelState> consumer)
	{
		return this.listener.onWheelUp(consumer);
	}
	@Override
	public ZAction onWheelDown(Consumer<WheelState> consumer)
	{
		return this.listener.onWheelDown(consumer);
	}
	@Override
	public ZAction onMotion(Consumer<MouseState> consumer)
	{
		return this.listener.onMotion(consumer);
	}
	@Override
	public ZAction onDrag(Consumer<MouseState> consumer)
	{
		return this.listener.onDrag(consumer);
	}
	@Override
	public ZAction onKeyPress(Consumer<Unicode> consumer)
	{
		return this.listener.onKeyPress(consumer);
	}
	@Override
	public ZAction onKeyRelease(Consumer<Unicode> consumer)
	{
		return this.listener.onKeyRelease(consumer);
	}
	@Override
	public ZAction onPress(Consumer<MouseState> consumer)
	{
		return this.listener.onPress(consumer);
	}
	@Override
	public ZAction onRelease(Consumer<MouseState> consumer)
	{
		return this.listener.onRelease(consumer);
	}
	@Override
	public ZAction onActivation(Consumer<ZWindow> consumer)
	{
		return this.listener.onActivation(consumer);
	}
	@Override
	public ZAction onDeactivation(Consumer<ZWindow> consumer)
	{
		return this.listener.onDeactivation(consumer);
	}
//	<< ACTIONS <<
	
//	>> SIZE >>
	@Override
	public int getWidth() 
	{
		return this.frame.getWidth();
	}
	public void setWidth(String width)
	{
		this.setSize(width, this.getHeight() + "px");
	}
	@Override
	public int getHeight() 
	{
		return this.frame.getHeight();
	}
	public void setHeight(String height)
	{
		this.setSize(this.getWidth() + "px", height);
	}
//	<< SIZE <<
	
//	>> LOCATION >>
	@Override
	public int getX() 
	{
		return (int) this.frame.getLocation().getX();
	}
	public void setX(String x)
	{
		this.setLocation(x, this.getY() + "px");
	}
	@Override
	public int getY() 
	{
		return (int) this.frame.getLocation().getY();
	}
	public void setY(String y)
	{
		this.setLocation(this.getX() + "px", y);
	}
//	<< LOCATION <<
	
//	>> ENUMS >>
	public static enum WinState
	{
		WINDOW, MINIMIZED, MAXIMIZED, FULLSCREEN, HIDDEN;
	}
//	<< ENUMS <<
}
