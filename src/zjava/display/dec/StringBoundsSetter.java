package zjava.display.dec;

import zjava.lang.types.dec.BoundsGetter;
import zjava.lang.types.dec.BoundsSetter;

public interface StringBoundsSetter extends BoundsGetter
{	
//	>> SIZE >>
	public void setWidth(String width);
	public void setHeight(String height);
	public default void setSize(String width, String height)
	{
		this.setWidth(width);
		this.setHeight(height);
	}
//	<< SIZE <<
	
//	>> LOCATION >>
	public void setX(String x);
	public void setY(String y);
	public default void setLocation(String x, String y)
	{
		this.setX(x);
		this.setY(y);
	}
//	<< LOCATION <<
	
//	>> TRANSFORM >>
	public default BoundsSetter asBoundsSetter()
	{
		StringBoundsSetter source = this;
		return new BoundsSetter()
		{
//			>> PRIVATE SECTION >>
			private String string(int input)
			{
				return input + "px";
			}
//			<< PRIVATE SECTION <<
			
//			>> SIZE >>
			@Override
			public int getWidth() 
			{
				return source.getWidth();
			}
			@Override
			public void setWidth(int width) 
			{
				source.setWidth(string(width));
			}
			@Override
			public int getHeight() 
			{
				return source.getHeight();
			}
			@Override
			public void setHeight(int height) 
			{
				source.setHeight(string(height));
			}
//			<< SIZE <<
			
//			>> LOCATION >>
			@Override
			public int getX() 
			{
				return source.getX();
			}
			@Override
			public void setX(int x) 
			{
				source.setX(string(x));
			}
			@Override
			public int getY() 
			{
				return source.getY();
			}
			@Override
			public void setY(int y) 
			{
				source.setY(string(y));
			}
//			<< LOCATION <<
		};
	}
//	<< TRANSFORM <<
}
