package zjava.display;

import javax.swing.JComponent;
import zjava.ZObject;
import zjava.lang.types.collections.Array;
import zjava.lang.types.collections.ZList;

public abstract class ZContainer extends ZObject
{
	private ZList<ZElement>
		elements = new ZList<>();
	
//	>> CONSTRUCTORS >>
	protected ZContainer()
	{
		
	}
//	<< CONSTRUCTORS <<
	
//	>> PROTECTED SECTION >>
	protected abstract JComponent useComponent();
	protected ZList<ZElement> useElements()
	{
		return this.elements;
	}
//	<< PROTECTED SECTION <<
	
//	>> GET >>
	public Array<ZElement> getElements()
	{
		return this.elements.toArray();
	}
//	<< GET <<
	
//	>> ADD >>
	public void add(ZElement... elements)
	{
		this.elements.add(elements);
		for(ZElement element : elements) this.useComponent().add(element.useComponent());
	}
//	<< ADD <<
	
//	>> REMOVE >>
	public boolean remove(ZElement element)
	{
		return this.elements.remove(element);
	}
//	<< REMOVE <<
}
