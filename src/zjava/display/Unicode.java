package zjava.display;

import zjava.ZObject;
import zjava.auto.annotations.Attribute;
import zjava.errors.ArgumentException;
import zjava.lang.types.collections.ZMap;

public class Unicode extends ZObject
{
	private static final ZMap<Character, Unicode>
		UNICODES = new ZMap<>();
	
	public static Unicode forCode(int code)
	{
		return forChar((char)code);
	}
	public static Unicode forChar(char character)
	{
		if(!UNICODES.containsKey(character))
		{
			Unicode unicode = new Unicode(Character.getName((int)character), (int)character);
			UNICODES.set(character, unicode);
			return unicode;
		}
		return UNICODES.get(character);
	}

	@Attribute
	private final String
		name;
	
	private final int
		code;
	
	private final char
		character;
	
	private final CharCat
		category;
	
	private Unicode(String name, int code) 
	{
		this.name = name;
		this.code = code;
		this.character = (char) code;
		this.category = CharCat.forCode(code);
	}
	
	public String getName()
	{
		return this.name;
	}
	public int getCode()
	{
		return this.code;
	}
	public char getChar()
	{
		return this.character;
	}
	public CharCat getCategory()
	{
		return this.category;
	}
	public CharType getType()
	{
		return this.category.getType();
	}
	
//	>> ENUMS >>
	public static enum CharCat
	{
		OTHER_CONTROL("Cc", "Other, Control", CharType.OTHER, Character.CONTROL),
		OTHER_FORMAT("Cf", "Other, Format", CharType.OTHER, Character.FORMAT),
		OTHER_UNASSIGNED("Cn", "Other, Not Assigned", CharType.NONE, Character.UNASSIGNED),
		OTHER_PRIVATE_USE("Co", "Other, Private Use", CharType.OTHER, Character.PRIVATE_USE),
		OTHER_SURROGATE("Cs", "Other, Surrogate", CharType.OTHER, Character.SURROGATE),
		LETTER_LOWERCASE("Ll", "Letter, Lowercase", CharType.LETTER, Character.LOWERCASE_LETTER),
		LETTER_MODIFIER("Lm", "Letter, Modifier", CharType.LETTER, Character.MODIFIER_LETTER),
		LETTER_OTHER("Lo", "Letter, Other", CharType.LETTER, Character.OTHER_LETTER),
		LETTER_TITLECASE("Lt", "Letter, Titlecase", CharType.LETTER, Character.TITLECASE_LETTER),
		LETTER_UPPERCASE("Lu", "Letter, Uppercase", CharType.LETTER, Character.UPPERCASE_LETTER),
		MARK_SPACING_COMBINING("Mc", "Mark, Spacing Combining", CharType.MARK, Character.COMBINING_SPACING_MARK),
		MARK_ENCLOSING("Me", "Mark, Enclosing", CharType.MARK, Character.ENCLOSING_MARK),
		MARK_NONSPACING("Mn", "Mark, Nonspacing", CharType.MARK, Character.NON_SPACING_MARK),
		NUMBER_DECIMAL_DIGIT("Nd", "Number, Decimal Digit", CharType.NUMBER, Character.DECIMAL_DIGIT_NUMBER),
		NUMBER_LETTER("Nl", "Number, Letter", CharType.NUMBER, Character.LETTER_NUMBER),
		NUMBER_OTHER("No", "Number, Other", CharType.NUMBER, Character.OTHER_NUMBER),
		PUNCTUATION_CONNECTOR("Pc", "Punctuation, Connector", CharType.PUNCTUATION, Character.CONNECTOR_PUNCTUATION),
		PUNCTUATION_DASH("Pd", "Punctuation, Dash", CharType.PUNCTUATION, Character.DASH_PUNCTUATION),
		PUNCTUATION_CLOSE("Pe", "Punctuation, Close", CharType.PUNCTUATION, Character.END_PUNCTUATION),
		PUNCTUATION_FINAL_QUOTE("Pf", "Punctuation, Final quote", CharType.PUNCTUATION, Character.FINAL_QUOTE_PUNCTUATION),
		PUNCTUATION_INITIAL_QUOTE("Pi", "Punctuation, Initial quote", CharType.PUNCTUATION, Character.INITIAL_QUOTE_PUNCTUATION),
		PUNCTUATION_OTHER("Po", "Punctuation, Other", CharType.PUNCTUATION, Character.OTHER_PUNCTUATION),
		PUNCTUATION_OPEN("Ps", "Punctuation, Open", CharType.PUNCTUATION, Character.START_PUNCTUATION),
		SYMBOL_CURRENCY("Sc", "Symbol, Currency", CharType.SYMBOL, Character.CURRENCY_SYMBOL),
		SYMBOL_MODIFIER("Sk", "Symbol, Modifier", CharType.SYMBOL, Character.MODIFIER_SYMBOL),
		SYMBOL_MATH("Sm", "Symbol, Math", CharType.SYMBOL, Character.MATH_SYMBOL),
		SYMBOL_OTHER("So", "Symbol, Other", CharType.SYMBOL, Character.OTHER_SYMBOL),
		SEPARATOR_LINE("Zl", "Separator, Line", CharType.SEPARATOR, Character.LINE_SEPARATOR),
		SEPARATOR_PARAGRAPH("Zp", "Separator, Paragraph", CharType.SEPARATOR, Character.PARAGRAPH_SEPARATOR),
		SEPARATOR_SPACE("Zs", "Separator, Space", CharType.SEPARATOR, Character.SPACE_SEPARATOR),
		;
		
		public static CharCat forChar(char character)
		{
			return forCode((int)character);
		}
		public static CharCat forCode(int code)
		{
			byte type = (byte) Character.getType(code);
			for(CharCat cat : values())
			{
				if(cat.getNativeType() == type)
				{
					return cat;
				}
			}
			throw new ArgumentException("could not resolve category of char:" + "(char=" + (char)code + ", int=" + (int)(code) + ")");
		}
		
		private final String
			code,
			description;
			
		private final CharType
			type;
		
		private final byte
			charByte;
		
		private CharCat(String code, String description, CharType type, byte charByte)
		{
			this.code = code;
			this.description = description;
			this.type = type;
			this.charByte = charByte;
		}
		
		public String getCode()
		{
			return this.code;
		}
		public String getDescription()
		{
			return this.description;
		}
		public CharType getType()
		{
			return this.type;
		}
		public byte getNativeType()
		{
			return this.charByte;
		}
	}
	public static enum CharType
	{
		NONE,
		LETTER,
		MARK,
		NUMBER,
		SEPARATOR,
		OTHER,
		PUNCTUATION,
		SYMBOL,
		;
	}
//	<< ENUMS <<
}
