package zjava.display;

import javax.swing.JPanel;

public class ZPanel extends ZElement
{
	private final JPanel
		panel;
	
	
	@Override
	protected JPanel useComponent()
	{
		return this.panel;
	}
	
}
