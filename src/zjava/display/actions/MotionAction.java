package zjava.display.actions;

import zjava.display.MouseState;
import zjava.display.ZAction;
import zjava.lang.functionals.Consumer;

public interface MotionAction
{
	public ZAction onMotion(Consumer<MouseState> consumer);
	public ZAction onDrag(Consumer<MouseState> consumer);
}
