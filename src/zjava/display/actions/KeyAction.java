package zjava.display.actions;

import zjava.display.Unicode;
import zjava.display.ZAction;
import zjava.lang.functionals.Consumer;

public interface KeyAction
{
	public ZAction onKeyPress(Consumer<Unicode> consumer);
	public ZAction onKeyRelease(Consumer<Unicode> consumer);
}
