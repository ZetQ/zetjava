package zjava.display.actions;

import zjava.display.MouseState;
import zjava.display.ZAction;
import zjava.lang.functionals.Consumer;

public interface ClickAction
{
	public ZAction onPress(Consumer<MouseState> consumer);
	public ZAction onRelease(Consumer<MouseState> consumer);
}
