package zjava.display.actions;

import zjava.display.WheelState;
import zjava.display.ZAction;
import zjava.lang.functionals.Consumer;

public interface WheelAction
{
	public ZAction onWheelUp(Consumer<WheelState> consumer);
	public ZAction onWheelDown(Consumer<WheelState> consumer);
}
