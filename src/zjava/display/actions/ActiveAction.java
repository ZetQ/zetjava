package zjava.display.actions;

import zjava.display.ZAction;
import zjava.lang.functionals.Consumer;

public interface ActiveAction<T>
{	
	public ZAction onActivation(Consumer<T> consumer);
	public ZAction onDeactivation(Consumer<T> consumer);
}
