package zjava.display.actions;

import zjava.display.MouseState;
import zjava.display.ZAction;
import zjava.lang.functionals.Consumer;

public interface HoverAction
{
	public ZAction onEnter(Consumer<MouseState> consumer);
	public ZAction onLeave(Consumer<MouseState> consumer);
}
