package zjava.display.actions;

import zjava.display.ZAction;
import zjava.display.ZWindow.WinState;
import zjava.lang.functionals.Consumer;

public interface WinAction<T>
{
	public ZAction onStateTo(WinState state, Consumer<T> consumer);
	public ZAction onStateFrom(WinState state, Consumer<T> consumer);
	public ZAction onClose(Consumer<T> consumer);
	public ZAction onOpen(Consumer<T> consumer);
}
