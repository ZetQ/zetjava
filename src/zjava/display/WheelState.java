package zjava.display;

import java.awt.event.MouseWheelEvent;

import zjava.lang.utils.DoubleMath;

public class WheelState
{
	public final double
		amount;
	
	public WheelState(MouseWheelEvent event)
	{
		this.amount = DoubleMath.positive(event.getPreciseWheelRotation());
	}
	
	public double getAmount()
	{
		return this.amount;
	}
}
