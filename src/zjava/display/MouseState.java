package zjava.display;

import java.awt.event.MouseEvent;

import zjava.ZObject;
import zjava.lang.types.ZPoint;
import zjava.lang.types.dec.PointGetter;

public class MouseState extends ZObject implements PointGetter
{
	public static final int
		BUTTON_RIGHT = 1,
		BUTTON_LEFT = 2,
		BUTTON_WHEEL = 3;
	
	private final ZPoint
		location,
		locationOnScreen;
	
	private final int
		button,
		clicks;
	
//	>> CONSTRUCTOR >>
	public MouseState(MouseEvent event)
	{
		this.location = new ZPoint(event.getPoint());
		this.locationOnScreen = new ZPoint(event.getLocationOnScreen());
		this.button = event.getButton();
		this.clicks = event.getClickCount();
	}
//	<< CONSTRUCTOR <<

//	>> BUTTONS >>
	public int getButton()
	{
		return this.button;
	}
	public int getClicks()
	{
		return this.clicks;
	}
//	<< BUTTONS <<
	
//	>> LOCATION >>
	@Override
	public int getX()
	{
		return this.location.getX();
	}
	@Override
	public int getY()
	{
		return this.location.getY();
	}
	public ZPoint getLocationOnScreen()
	{
		return this.locationOnScreen.copy();
	}
//	<< LOCATION <<
}
