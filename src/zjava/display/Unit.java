package zjava.display;

import zjava.ZObject;
import zjava.errors.ArgumentException;
import zjava.errors.UnexpectedException;
import zjava.errors.UnsupportedException;
import zjava.lang.types.collections.MapEntry;
import zjava.lang.types.collections.ZMap;
import zjava.system.ZScreen;

public class Unit extends ZObject
{
	private static final ZMap<String, Measure>
		UNITS = new ZMap<>();

	private static final String
		DEFAULT_UNIT = "px",
		REGEX_VALIDNUM = "\\d+.?(\\d.+)?";
	
	static
	{
		define((val, ref) -> (int) val, "px");
		define((val, ref) -> (int)(ref / 100 * val), "%");
		define((value, ref) -> (int)((value * ZScreen.getResolution()) / 2.54), "cm");
		define((value, ref) -> (int)((value * ZScreen.getResolution()) / 25.4), "mm");
	}
	
	public static boolean define(Measure measure, String... symbols)
	{
		boolean success = true;
		for(String symbol : symbols)
		{
			symbol = symbol.trim().toLowerCase();
			if(!(symbol.length() > 0 && !UNITS.containsKey(symbol)))
			{
				UNITS.set(symbol, measure);
				success = false;
			}
		}
		return success;
	}
	public static Value decode(String string)
	{
		string = string.trim();
		if(string.matches("(" + REGEX_VALIDNUM + ").+"))
		{
			for(MapEntry<String, Measure> unit : UNITS)
			{
				if(string.toLowerCase().matches("(" + REGEX_VALIDNUM + ")" + unit.getKey()))
				{
					String[] val = string.split(unit.getKey());
					if(val.length > 1)
					{
						throw new UnexpectedException("error while trying to cast value");
					}
					double value = Double.valueOf(val[0]);
					return new Value()
					{
						@Override
						public int calc(double ref) 
						{
							return unit.getVal().calc(value, ref);
						}
						@Override
						public double getNumber() 
						{
							return value;
						}
						@Override
						public String getSymbol() 
						{
							return unit.getKey();
						}
				
					};
				}
			}
		}
		if(string.matches(REGEX_VALIDNUM))
		{
			return decode(string + DEFAULT_UNIT);
		}
		throw new ArgumentException("unable to parse to value: \"" + string + "\"");
	}
	public static int direct(String string, double ref)
	{
		return decode(string).calc(ref);
	}
	
	@FunctionalInterface
	public static interface Measure
	{
		public int calc(double value, double ref);
	}
	@FunctionalInterface
	public static interface Value
	{
		public int calc(double ref);
		
		public default double getNumber()
		{
			//will be implemented by the decode-method
			throw new UnsupportedException("method not implemented");
		}
		public default String getSymbol()
		{
			//will be implemented by the decode-method
			throw new UnsupportedException("method not implemented");
		}
	}
}
