package zjava.display;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;

import zjava.ZObject;
import zjava.display.ZWindow.WinState;
import zjava.display.actions.ActiveAction;
import zjava.display.actions.ClickAction;
import zjava.display.actions.HoverAction;
import zjava.display.actions.KeyAction;
import zjava.display.actions.MotionAction;
import zjava.display.actions.WheelAction;
import zjava.display.actions.WinAction;
import zjava.lang.functionals.Consumer;
import zjava.lang.functionals.Provider;
import zjava.lang.types.collections.ZList;
import zjava.lang.types.collections.ZMap;

class ListenerProvider<T> extends ZObject implements ActiveAction<T>, ClickAction, KeyAction, MotionAction, WheelAction, WinAction<T>, HoverAction
{
	private T
		source;
	
	private final Provider<WinState>
		getState,
		oldState;
	
	private final ZList<Consumer<WheelState>>
		wheelUp = new ZList<>(),
		wheelDown = new ZList<>();
	
	private final ZList<Consumer<MouseState>>
		mouseMotion = new ZList<>(),
		mouseDrag = new ZList<>(),
		mousePress = new ZList<>(),
		mouseRelease = new ZList<>(),
		mouseEnter = new ZList<>(),
		mouseLeave = new ZList<>();
	
	private final ZList<Consumer<Unicode>>
		keyPress = new ZList<>(),
		keyRelease = new ZList<>();
	
	private final ZList<Consumer<T>>
		activeTrue = new ZList<>(),
		activeFalse = new ZList<>();
	
	private final ZList<Consumer<T>>
		windowClose = new ZList<>(),
		windowOpen = new ZList<>();
	
	private final ZMap<WinState, ZList<Consumer<T>>>
		windowStateTo = create(() -> {
			ZMap<WinState, ZList<Consumer<T>>> map = new ZMap<>();
			for(WinState state : WinState.values()) map.set(state, new ZList<>());
			return map;
		}),
		windowStateFrom = windowStateTo.copy();
	
	public ListenerProvider(T source, Provider<WinState> getState, Provider<WinState> getOldState)
	{
		this.source = source;
		this.getState = getState;
		this.oldState = getOldState;
	}
	
//	>> PRIVATE SECTION >>
	private <P> ZAction add(ZList<Consumer<P>> list, Consumer<P> consumer)
	{
		list.add(consumer);
		return new ZAction(this.source) { @Override public void remove() { list.remove(consumer); } };
	}
	private <P> void dist(ZList<Consumer<P>> list, P value)
	{
		for(Consumer<P> consumer : list)
		{
			thread(() -> consumer.consume(value));
		}
	}
//	<< PRIVATE SECTION <<

//	>> LISTENERS >>
	public KeyListener forKey()
	{
		return this.keyListener;
	}
	public MouseListener forMouse()
	{
		return this.mouseAdapter;
	}
	public MouseMotionListener forMove()
	{
		return this.mouseAdapter;
	}
	public MouseWheelListener forWheel()
	{
		return this.mouseAdapter;
	}
	public WindowListener forWindow()
	{
		return this.windowListener;
	}
	public WindowStateListener forState()
	{
		return this.stateListener;
	}
//	<< LISTENERS <<
	
//	>> ON >>
	@Override
	public ZAction onWheelUp(Consumer<WheelState> consumer)
	{
		return this.add(this.wheelUp, consumer);
	}
	@Override
	public ZAction onWheelDown(Consumer<WheelState> consumer)
	{
		return this.add(this.wheelDown, consumer);
	}
	@Override
	public ZAction onMotion(Consumer<MouseState> consumer)
	{
		return this.add(this.mouseMotion, consumer);
	}
	@Override
	public ZAction onDrag(Consumer<MouseState> consumer)
	{
		return this.add(this.mouseDrag, consumer);
	}
	@Override
	public ZAction onEnter(Consumer<MouseState> consumer)
	{
		return this.add(this.mouseEnter, consumer);
	}
	@Override
	public ZAction onLeave(Consumer<MouseState> consumer)
	{
		return this.add(this.mouseLeave, consumer);
	}
	@Override
	public ZAction onKeyPress(Consumer<Unicode> consumer)
	{
		return this.add(this.keyPress, consumer);
	}
	@Override
	public ZAction onKeyRelease(Consumer<Unicode> consumer)
	{
		return this.add(this.keyRelease, consumer);
	}
	@Override
	public ZAction onPress(Consumer<MouseState> consumer)
	{
		return this.add(this.mousePress, consumer);
	}
	@Override
	public ZAction onRelease(Consumer<MouseState> consumer)
	{
		return this.add(this.mouseRelease, consumer);
	}
	@Override
	public ZAction onActivation(Consumer<T> consumer)
	{
		return this.add(this.activeTrue, consumer);
	}
	@Override
	public ZAction onDeactivation(Consumer<T> consumer)
	{
		return this.add(this.activeFalse, consumer);
	}
	@Override
	public ZAction onStateTo(WinState state, Consumer<T> consumer)
	{
		return this.add(this.windowStateTo.get(state), consumer);
	}
	@Override
	public ZAction onStateFrom(WinState state, Consumer<T> consumer)
	{
		return this.add(this.windowStateFrom.get(state), consumer);
	}
	@Override
	public ZAction onClose(Consumer<T> consumer)
	{
		return this.add(this.windowClose, consumer);
	}
	@Override
	public ZAction onOpen(Consumer<T> consumer)
	{
		return this.add(this.windowOpen, consumer);
	}
//		<< ON <<	
	
//	>> LISTENER CREATION >>
	private final WindowListener
		windowListener = new WindowAdapter()
		{
			@Override
			public void windowOpened(WindowEvent e)
			{
				dist(windowOpen, source);
			}
			@Override
			public void windowClosed(WindowEvent e)
			{
				dist(windowClose, source);
			}
			@Override
			public void windowActivated(WindowEvent e)
			{
				dist(activeTrue, source);
			}
			@Override
			public void windowDeactivated(WindowEvent e)
			{
				dist(activeFalse, source);
			}
		};
	private final MouseAdapter
		mouseAdapter = new MouseAdapter()
		{
		    public void mousePressed(MouseEvent e) 
		    {
		    	dist(mousePress, new MouseState(e));
		    }
		    public void mouseReleased(MouseEvent e) 
		    {
		    	dist(mouseRelease, new MouseState(e));
		    }
		    public void mouseEntered(MouseEvent e) 
		    {
		    	dist(mouseEnter, new MouseState(e));
		    }
		    public void mouseExited(MouseEvent e) 
		    {
		    	dist(mouseLeave, new MouseState(e));
		    }
		    public void mouseWheelMoved(MouseWheelEvent e)
		    {
		    	dist(e.getPreciseWheelRotation() < 0 ? wheelDown : wheelUp, new WheelState(e));
		    }
		    public void mouseDragged(MouseEvent e)
		    {
		    	dist(mouseDrag, new MouseState(e));
		    }
		    public void mouseMoved(MouseEvent e)
		    {
		    	dist(mouseMotion, new MouseState(e));
		    }
		};
	private final KeyListener
		keyListener = new KeyAdapter() 
		{
			@Override
			public void keyPressed(KeyEvent e)
			{
				dist(keyPress, Unicode.forChar(e.getKeyChar()));
			}
			@Override
			public void keyReleased(KeyEvent e)
			{
				dist(keyRelease, Unicode.forChar(e.getKeyChar()));
			}
		};
	private final WindowStateListener
		stateListener = event -> {
			thread(() -> dist(this.windowStateTo.get(this.getState.provide()), source));
			thread(() -> dist(this.windowStateFrom.get(this.oldState.provide()), source));
		};
//		<< LISTENER CREATION <<
}
