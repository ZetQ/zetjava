package zjava.display;

import zjava.ZObject;
import zjava.auto.annotations.Attribute;

public abstract class ZAction extends ZObject
{
	@Attribute
	private final Object
		target;
	
	@Attribute(showName = false)
	private boolean
		isActive;
	
//	>> CONSTRUCTORS >>
	public ZAction(Object target)
	{
		this.target = target;
	}
//	<< CONSTRUCTORS <<
	
//	>> PUBLIC SECTION >>
	public abstract void remove();
//	<< PUBLIC SECTION <<
}
