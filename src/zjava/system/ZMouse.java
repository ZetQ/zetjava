package zjava.system;

import java.awt.AWTException;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.MouseInfo;
import java.awt.Robot;

import zjava.StaticObject;
import zjava.errors.ArgumentException;
import zjava.errors.UnsupportedException;
import zjava.lang.types.ZPoint;
import zjava.lang.utils.ZThread;

public class ZMouse extends StaticObject
{
	private ZMouse() {}
	
//	>> PRIVATE SECTION >>
	private static void checkUsability() throws UnsupportedException
	{
		if(!isSupported())
		{
			throw new UnsupportedException("this System does not support mouse control", ZMouse.class, "isSupported");
		}
	}
	private static void checkButton(int button) throws ArgumentException
	{
		checkUsability();
		if(button >= getButtonAmount())
		{
			throw new ArgumentException("the current mouse does not have the given button");
		}
	}
	private static Robot robot()
	{
		checkUsability();
		try
		{
			return new Robot();
		} 
		catch (AWTException e)
		{
			throw new UnsupportedException("could not move mouse pointer", e);
		}
	}
//	<< PRIVATE SECTION <<
	
//	>> INFO BOOLS >>
	public static boolean isSupported()
	{
		return !GraphicsEnvironment.isHeadless();
	}
	public static boolean isPlugged()
	{
		return MouseInfo.getNumberOfButtons() == -1;
	}
//	<< INFO BOOLS <<
	
//	>> BUTTONS >>
	public static int getButtonAmount()
	{
		checkUsability();
		return MouseInfo.getNumberOfButtons();
	}
//	<< BUTTONS <<
	
//	>> ACTIONS >>
	public static void press(int button)
	{
		checkButton(button);
		robot().mousePress(button);
	}
	public static void release(int button)
	{
		checkButton(button);
		robot().mouseRelease(button);
	}
	public static void click(int button, long delay)
	{
		ZThread.execute(() -> 
		{
			press(button);
			ZThread.sleep(delay);
			release(button);
		});
	}
	public static void scroll(int amount)
	{
		checkUsability();
		robot().mouseWheel(amount);
	}
//	<< ACTIONS <<
	
//	>> LOCATION >>
	public static int getX()
	{
		checkUsability();
		return (int) MouseInfo.getPointerInfo().getLocation().getX();
	}
	public static void setX(int x)
	{
		setLocation(x, getY());
	}
	public static int getY()
	{
		checkUsability();
		return (int) MouseInfo.getPointerInfo().getLocation().getY();
	}
	public static void setY(int y)
	{
		setLocation(getX(), y);
	}
	public static ZPoint getLocation()
	{
		return new ZPoint(getX(), getY());
	}
	public static void setLocation(int x, int y)
	{
		robot().mouseMove(x, y);
	}
//	<< LOCATION <<
}
