package zjava.system;

import java.awt.Toolkit;

import zjava.StaticObject;
import zjava.lang.types.ZSize;

public class ZScreen extends StaticObject
{
	private static final ZSize	
		SIZE = new ZSize(Toolkit.getDefaultToolkit().getScreenSize());
	
	private static final int
		RESOLUTION = Toolkit.getDefaultToolkit().getScreenResolution();
	
	
//	>> RESOLUTION >>
	public static int getResolution()
	{
		return RESOLUTION;
	}
//	<< RESOLUTION <<
	
//	>> SIZE >>
	public static int getWidth()
	{
		return SIZE.getWidth();
	}
	public static int getHeight()
	{
		return SIZE.getHeight();
	}
	public static ZSize getSize()
	{
		return new ZSize(getWidth(), getHeight());
	}
//	<< SIZE <<
}
