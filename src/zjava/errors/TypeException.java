package zjava.errors;

public class TypeException extends ZRuntimeException
{
	public TypeException(String message, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message, fixClass, fixMethod, params);
	}
	public TypeException(String message, Throwable cause, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message, cause, fixClass, fixMethod, params);
	}
	public TypeException(String message, Throwable cause)
	{
		super(message, cause);
	}
	public TypeException(String message)
	{
		super(message);

	}
}
