package zjava.errors;

public class AccessException extends ZException
{
	private static final long serialVersionUID = -4005406090400259980L;

	public AccessException(String message, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message, fixClass, fixMethod, params);
	}
	public AccessException(String message, Throwable cause, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message, cause, fixClass, fixMethod, params);
	}
	public AccessException(String message, Throwable cause)
	{
		super(message, cause);
	}
	public AccessException(String message)
	{
		super(message);
	}
	

}
