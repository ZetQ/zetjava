package zjava.errors;

public class CodeException extends ZRuntimeException
{
	private static final long serialVersionUID = 7999061385137814314L;
	
	public CodeException(String message, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message, fixClass, fixMethod, params);
	}
	public CodeException(String message, Throwable cause, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message, cause, fixClass, fixMethod, params);
	}
	public CodeException(String message, Throwable cause)
	{
		super(message, cause);
	}
	public CodeException(String message)
	{
		super(message);
	}
}
