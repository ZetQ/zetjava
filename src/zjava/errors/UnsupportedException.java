package zjava.errors;

public class UnsupportedException extends ZRuntimeException
{
	private static final long serialVersionUID = 2620944042222238954L;

	public UnsupportedException(String message, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message, fixClass, fixMethod, params);
	}
	public UnsupportedException(String message, Throwable cause, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message, cause, fixClass, fixMethod, params);
	}
	public UnsupportedException(String message, Throwable cause)
	{
		super(message, cause);
	}
	public UnsupportedException(String message)
	{
		super(message);
	}
}
