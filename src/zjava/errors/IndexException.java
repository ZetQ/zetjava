package zjava.errors;

public class IndexException extends ZRuntimeException
{
	private static final long serialVersionUID = -8530724972080268940L;
	
	public IndexException(String message, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message, fixClass, fixMethod, params);
	}
	public IndexException(String message, Throwable cause, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message, cause, fixClass, fixMethod, params);
	}
	public IndexException(String message, Throwable cause)
	{
		super(message, cause);
	}
	public IndexException(String message)
	{
		super(message);
	}
}
