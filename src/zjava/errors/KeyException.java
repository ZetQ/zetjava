package zjava.errors;

public class KeyException extends ZRuntimeException 
{
	private static final long serialVersionUID = -8344617395217201597L;
	
	public KeyException(String message, Class<?> fixClass, String fixMethod, Class<?>... params) 
	{
		super(message, fixClass, fixMethod, params);
	}
	public KeyException(String message, Throwable cause, Class<?> fixClass, String fixMethod, Class<?>... params) 
	{
		super(message, cause, fixClass, fixMethod, params);
	}

	public KeyException(String message, Throwable cause) 
	{
		super(message, cause);
	}
	public KeyException(String message) 
	{
		super(message);
	}

}
