package zjava.errors;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ZException extends Exception
{
	private static final long serialVersionUID = 6611678426458373997L;
	
	private static String createFixMessage(Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		String fix = "\n\tuse \"" + fixClass.getName() + "." + fixMethod + "(";
		
		for(int i = 0; i < params.length; ++i)
		{
			fix += params[i].getSimpleName();
			if(i != params.length - 1)
			{
				fix+= ", ";
			}
		}
		
		return fix + ")\" to prevent this exception";
	}
    @SuppressWarnings("unchecked")
    private static <T extends Throwable> void throwException(Throwable exception) throws T
    {
        throw (T) exception;
    }
    public static void throwUnchecked(Throwable exception)
    {
        ZException.<RuntimeException>throwException(exception);
    }
	
	public ZException(String message)
	{
		super(message);
	}
	public ZException(String message, Throwable cause)
	{
		super(message, cause);
	}
	public ZException(String message, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message + ZException.createFixMessage(fixClass, fixMethod, params));
	}
	public ZException(String message, Throwable cause, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message + ZException.createFixMessage(fixClass, fixMethod, params), cause);
	}
	
	public String getStackTraceString()
	{
		StringWriter sWriter = new StringWriter();
		PrintWriter pWriter = new PrintWriter(sWriter);
		this.printStackTrace(pWriter);
		return sWriter.toString();
	}
}
