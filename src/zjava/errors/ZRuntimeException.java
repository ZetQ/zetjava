package zjava.errors;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ZRuntimeException extends RuntimeException
{
	private static final long serialVersionUID = 537921553909727778L;
	
	private static String createFixMessage(Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		String fix = "\n\tuse \"" + fixClass.getName() + "." + fixMethod + "(";
		
		for(int i = 0; i < params.length; ++i)
		{
			fix += params[i].getSimpleName();
			if(i != params.length - 1)
			{
				fix+= ", ";
			}
		}
		
		return fix + ")\" to prevent this exception";
	}
	
	public ZRuntimeException(String message)
	{
		super(message);
	}
	public ZRuntimeException(String message, Throwable cause)
	{
		super(message, cause);
	}
	public ZRuntimeException(String message, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message + ZRuntimeException.createFixMessage(fixClass, fixMethod, params));
	}
	public ZRuntimeException(String message, Throwable cause, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message + ZRuntimeException.createFixMessage(fixClass, fixMethod, params), cause);
	}
	
	public String getStackTraceString()
	{
		StringWriter sWriter = new StringWriter();
		PrintWriter pWriter = new PrintWriter(sWriter);
		this.printStackTrace(pWriter);
		return sWriter.toString();
	}
}
