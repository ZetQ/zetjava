package zjava.errors;

public class DisplayException extends ZRuntimeException
{
	private static final long serialVersionUID = 4370529346428798880L;

	public DisplayException(String message, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message, fixClass, fixMethod, params);
	}
	public DisplayException(String message, Throwable cause, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message, cause, fixClass, fixMethod, params);
	}
	public DisplayException(String message, Throwable cause)
	{
		super(message, cause);
	}
	public DisplayException(String message)
	{
		super(message);
	}
	


}
