package zjava.errors;

public class UnexpectedException extends ZRuntimeException
{
	private static final long serialVersionUID = 6285203436071663822L;
	
	public UnexpectedException(String message, Throwable cause)
	{
		super(message, cause);
	}
	public UnexpectedException(String message)
	{
		super(message);
	}
}
