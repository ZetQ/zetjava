package zjava.errors;

public class ArgumentException extends ZRuntimeException
{
	private static final long serialVersionUID = -5777492379669313744L;

	public ArgumentException(String message, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message, fixClass, fixMethod, params);
	}
	public ArgumentException(String message, Throwable cause, Class<?> fixClass, String fixMethod, Class<?>... params)
	{
		super(message, cause, fixClass, fixMethod, params);
	}
	public ArgumentException(String message, Throwable cause)
	{
		super(message, cause);
	}
	public ArgumentException(String message)
	{
		super(message);
	}
	

}
