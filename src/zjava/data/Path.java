package zjava.data;

import java.io.IOException;
import java.nio.file.Files;

import zjava.ZObject;
import zjava.auto.annotations.Attribute;
import zjava.errors.AccessException;
import zjava.errors.ArgumentException;
import zjava.lang.tools.Copyable;

public abstract class Path extends ZObject implements Copyable<Path>
{	
//	>> STATIC SECTION >>
	public static Path create(String path)
	{
		java.io.File file = file(path);
		if(file.isDirectory())
		{
			return new Directory(file);
		}
		return new File(file);
	}
	public static boolean exists(String path)
	{
		return file(path).exists();
	}
	public static boolean isDirectory(String path)
	{
		return file(path).isDirectory();
	}
	public static boolean isFile(String path)
	{
		return file(path).isFile();
	}
	private static java.io.File file(String path)
	{
		return new java.io.File(path);
	}
//	<< STATIC SECTION <<
	
	@Attribute(showName=false)
	private final java.io.File
		file;
	
//	>> CONSTRUCTOR >>
	public Path(String path)
	{
		this.file = new java.io.File(path);
	}
//	<< CONSTRUCTOR <<
	
//	>> PRIVATE SECTION >>
	private boolean moveTo(java.io.File location) throws AccessException
	{
		try
		{
			return Files.move(this.file().toPath(), new java.io.File(location.getAbsolutePath()).toPath()).toFile().getParent().equals(location.getAbsolutePath());
		} 
		catch (IOException e)
		{
			throw new AccessException("unable to rename file", e);
		}
		catch(SecurityException e)
		{
			throw new AccessException("no permission to rename file", e);
		}
	}
//	<< PRIVATE SECTION <<
	
//	>> PROTECTED SECTION >>
	protected java.io.File file()
	{
		return this.file;
	}
	protected void checkExistence() throws AccessException
	{	
		if(!this.file.exists())
		{
			throw new AccessException("file does not exist");
		}
	}
	protected void checkWrite() throws AccessException
	{
		if(!file().canWrite())
		{
			throw new AccessException("no permission to change file");
		}
	}
//	<< PROTECTED SECTION <<
	
//	>> MISC >>
	public abstract Object getContents() throws AccessException;
	public abstract boolean clear();
	public long getSize()
	{
		return this.file.length();
	}
//	<< MISC <<
	
//	>> NAME >>
	public String getName()
	{
		return this.file.getName();
	}
	public boolean setName(String name) throws AccessException
	{
		this.checkExistence();
		this.checkWrite();
		java.io.File renamed = new java.io.File(file().getParent() + name.replace("/|\\", " "));
		if(renamed.exists())
		{
			throw new AccessException("file already exists");
		}
		return this.moveTo(renamed);
	}
//	<< NAME <<
	
//	>> STATUS >>
	public boolean isReadable()
	{
		return this.file().canRead();
	}
	public boolean isWritable()
	{
		return this.file().canWrite();
	}
	public boolean isHidden()
	{
		return this.file().isHidden();
	}
//	<< STATUS <<
	
//	>> PATH >>
	public boolean setPath(Directory location) throws AccessException
	{
		checkExistence();
		if(!location.exists())
		{
			if(!location.create())
			{
				throw new AccessException("location does not exist, unable to create it");
			}
		}
		checkWrite();
		
		if(location.getAbsolutePath().contains(this.getAbsolutePath()))
		{
			throw new ArgumentException("cannot move a file in its own folder");
		}
		return this.moveTo(new java.io.File(location.getAbsolutePath() + "/" + this.getName()));
	}
	public String getPath()
	{
		return this.file.getPath();
	}
	public String getAbsolutePath()
	{
		return this.file.getAbsolutePath();
	}
	public String getCanonialPath() throws AccessException
	{
		this.checkExistence();
		try
		{
			return this.file.getCanonicalPath();
		} 
		catch (IOException e)
		{
			throw new AccessException("unable to create path", e);
		}
	}
//	<< PATH <<
	
//	>> EXISTENCE >>
	public boolean exists()
	{
		return this.file.exists();
	}
	public abstract boolean create();
	public void secure()
	{
		if(!this.exists())
		{
			this.create();
		}
	}
	public boolean delete()
	{
		return this.file.delete();
	}
//	<< EXISTENCE <<
	
//	>> PARENT >>
	public Directory getParent()
	{
		if(this.hasParent())
		{
			return new Directory(this.file.getParent());
		}
		return null;
	}
	public boolean hasParent()
	{
		return null != this.file.getParent();
	}
//	<< PARENT <<
	
//	>> TYPE >>
	public boolean isFile()
	{
		return this.file.isFile();
	}
	public boolean isDirectory()
	{
		return this.file.isDirectory();
	}
//	<< TYPE <<
	
//	>> TRANSFORM >>
	public File toFile()
	{
		return new File(this.getPath());
	}
//	<< TRANSFORM <<
}
