package zjava.data;

import zjava.errors.ArgumentException;
import zjava.lang.types.collections.Array;
import zjava.lang.types.collections.ZList;

public class Directory extends Path
{
	public Directory(java.io.File file)
	{
		this(file.getPath());
	}
	public Directory(String path)
	{
		super(path);
		if(!this.isDirectory())
		{
			throw new ArgumentException(path + " is a file");
		}
	}
	
//	>> CONTENT >>
	@Override
	public Array<Path> getContents()
	{
		ZList<Path> list = new ZList<>();
		for(java.io.File file : this.file().listFiles())
		{
			if(file.isDirectory())
			{
				list.add(new Directory(file));
			}
			else
			{
				list.add(new File(file));
			}
		}
		return list.toArray();
	}
	public Array<Directory> getFolders()
	{
		ZList<Directory> list = new ZList<>();
		for(java.io.File file : this.file().listFiles())
		{
			if(file.isDirectory())
			{
				list.add(new Directory(file));
			}
		}
		return list.toArray();
	}
	public Array<File> getFiles()
	{
		ZList<File> list = new ZList<>();
		for(java.io.File file : this.file().listFiles())
		{
			if(!file.isDirectory()) list.add(new File(file));
		}
		return list.toArray();
	}
	public Array<File> getFileWhere(String regex)
	{
		ZList<File> list = new ZList<>();
		for(java.io.File file : this.file().listFiles())
		{
			if(file.getName().matches(regex)) list.add(new File(file));
		}
		return list.toArray();
	}
	public boolean clear()
	{
		boolean clear = true;
		for(Path path : this.getContents())
		{
			if(!path.delete())
			{
				clear = false;
			}
		}
		return clear;
	}
	public long getFreeBytes()
	{
		return this.file().getFreeSpace();
	}
//	<< CONTENT <<
	
//	>> IMPLEMENTATIONS >>
	@Override
	public boolean create()
	{
		return this.file().mkdirs();
	}
	@Override
	public Directory copy()
	{
		return new Directory(this.file());
	}
//	<< IMPLEMENTATIONS <<
}
