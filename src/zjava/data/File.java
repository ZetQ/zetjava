package zjava.data;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import zjava.ZObject;
import zjava.auto.annotations.Attribute;
import zjava.errors.AccessException;
import zjava.errors.ArgumentException;
import zjava.errors.UnexpectedException;
import zjava.lang.functionals.Eval;
import zjava.lang.types.collections.Array;
import zjava.lang.types.collections.ZList;

public class File extends Path
{	
	public File(java.io.File file)
	{
		this(file.getPath());
	}
	public File(String path)
	{
		super(path);
		if(file().isDirectory())
		{
			throw new ArgumentException(path + " is a directory");
		}
	}
	
//	>> PRIVATE SECTION >>
	private String getMimePart(int part) throws AccessException
	{
		String mime = this.getMIMEType();
		return null == mime ? null : mime.split("/")[part];
	}
//	<< PRIVATE SECTION <<
	
//	>> STATUS >>
	public boolean isExecutable()
	{
		return this.file().canExecute();
	}
//	<< STATUS <<
	
//	>> TYPE GET >>
	public String getMIMEType() throws AccessException
	{
		this.checkExistence();
		
		try
		{
			return Files.probeContentType(this.file().toPath());
		} 
		catch (IOException e)
		{
			throw new AccessException("unable to get MIME-type of file", e);
		}
	}
	public FileGroup getGroup() throws AccessException
	{
		return FileGroup.get(this.getMimePart(0));
	}
	public FileType getType() throws AccessException
	{
		return FileType.get(this.getMimePart(1));
	}
//	<< TYPE GET <<
	
//	>> TYPE IS >>
	public boolean isImage() throws AccessException
	{
		return this.exists() && this.getGroup() == FileGroup.IMAGE;
	}
//	<< TYPE IS <<
	
//	>> IMPLEMENTATIONS >>
	@Override
	public Array<String> getContents() throws AccessException
	{
		if(!this.exists())
		{
			throw new AccessException("file does not exist");
		}
		if(!this.isReadable())
		{
			throw new AccessException("denied to read file");
		}
	
		try
		{
			ZList<String> list = new ZList<>();
			BufferedReader reader = new BufferedReader(new FileReader(this.file()));
			String line = reader.readLine();
			while(null != line)
			{
				list.add(line);
				line = reader.readLine();
			}
			reader.close();
			return list.toArray();
		}
		catch (FileNotFoundException e)
		{
			throw new UnexpectedException("unable to read file", e);
		}
		catch (IOException e)
		{
			throw new AccessException("unable to read file", e);
		}
	}
	@Override
	public boolean clear()
	{
		if(this.delete())
		{
			return this.create();
		}
		return false;
	}	
	@Override
	public boolean create()
	{
		try
		{
			return this.file().createNewFile();
		} 
		catch (IOException e)
		{
			return false;
		}
	}
	@Override
	public File copy()
	{
		return new File(this.getPath());
	}
//	<< IMPLEMENTATIONS <<
	
	public static class FileGroup extends ZObject
	{		
		private static final ZList<FileGroup>
			GROUPS = new ZList<>();
		
		public static final FileGroup
			UNDEFINED = get(null),
			IMAGE = get("image");
		
		public static FileGroup get(String identifier)
		{
			Eval<FileGroup> eval = group -> 
			{
				if(null == identifier)
				{
					return null == group.getIdentifier();
				}
				return identifier.equals(group.getIdentifier());
			};
			
			
			if(!GROUPS.containsWhere(eval))
			{
				GROUPS.add(new FileGroup(identifier));
			}
			return GROUPS.getWhere(eval).get(0);
		}
		
		@Attribute
		private final String
			identifier;
		
		private FileGroup(String identifier)
		{
			this.identifier = identifier;
		}
		public String getIdentifier()
		{
			return this.identifier;
		}
	}
	public static class FileType extends ZObject
	{
		private static final ZList<FileType>
			TYPES = new ZList<>();
		
		public static final FileType
			UNDEFINED = get(null),
			PNG = get("png");
	
		public static FileType get(String identifier)
		{	
			Eval<FileType> eval = type -> 
			{
				if(null == identifier)
				{
					return null == type.getIdentifier();
				}
				return identifier.equals(type.getIdentifier());
			};
			
			if(!TYPES.containsWhere(eval))
			{
				TYPES.add(new FileType(identifier));
			}
			return TYPES.getWhere(eval).get(0);
		}
		
		@Attribute
		private final String
			identifier;
		
		private FileType(String identifier)
		{
			this.identifier = identifier;
		}
		public String getIdentifier()
		{
			return this.identifier;
		}
	}
}
