package zjava.aaa_test;

import zjava.ZObject;
import zjava.display.ZPanel;
import zjava.display.ZWindow;
import zjava.display.ZWindow.WinState;

class ZTest_Display extends ZObject
{
	public static void main(String[] args)
	{
		new ZTest_Display();
	}
	public ZTest_Display()
	{	
		ZWindow window = new ZWindow("hey");
		window.add(new ZPanel());
		
		window.setState(WinState.WINDOW);
	}
}
