package zjava;

import java.lang.annotation.Annotation;
import zjava.lang.reflect.ZClass;
import zjava.lang.types.collections.Array;

/**
 * Contains all global constants (eg. enums and final objects).<br>
 * A constant is considered global when it's supposed to be used by multiple classes. 
 */
public class Constant extends StaticObject
{
	private Constant() {}
	
	public static enum Primitive
	{
		BYTE(byte.class, Byte.class),
		SHORT(short.class, Short.class),
		INT(int.class, Integer.class),
		LONG(long.class, Long.class),
		FLOAT(float.class, Float.class),
		DOUBLE(double.class, Double.class),
		CHAR(char.class, Character.class),
		BOOLEAN(boolean.class, Boolean.class),
		VOID(void.class, Void.class)
		;
		
		private final ZClass<?>
			primitive,
			wrapper;
		
		private <P> Primitive(Class<?> primitive, Class<?> wrapper)
		{
			this.primitive = ZClass.get(primitive);
			this.wrapper = ZClass.get(wrapper);
		}
		public ZClass<?> getPrimitive()
		{
			return this.primitive;
		}
		public ZClass<?> getWrapper()
		{
			return this.wrapper;
		}
	}
	public static class Arithmetic
	{
		public static final double
			PI = 3.14159265358979323846,
			E = 2.7182818284590452354;
	}
	
//	>> STRING >>
	public static class SpecialChar extends ZObject
	{
		public static final SpecialChar
			SINGLE_QUOTE = new SpecialChar('\''),
			DOUBLE_QUOTE = new SpecialChar('\"'),
			BACKSLASH = new SpecialChar('\\'),
			TAB = new SpecialChar('\t'),
			BACKSPACE = new SpecialChar('\b'),
			CARRIAGE_RETURN = new SpecialChar('\r'),
			FORMFEED = new SpecialChar('\f'),
			NEWLINE = new SpecialChar('\n');
		
		private final char
			character;
		
	//	>> CONSTRUCTORS >>	
		private SpecialChar(char character)
		{
			this.character = character;
		}
	//	<< CONSTRUCTORS <<
	
	//	>> GETTERS >>
		public char getChar()
		{
			return this.character;
		}
		public String getEscaped()
		{
			return "\\" + this.character;
		}
	//	<< GETTERS <<
		
	//	>> IMPLEMENTATIONS >>
		@Override
		public String toString()
		{
			return "" + this.character;
		}
	//	<< IMPLEMENTATIONS <<
	}
//	<< STRING <<
	
//	>> REFLECTION >>
	public static enum ClassType
	{
		CLASS, INTERFACE, ANNOTATION, ENUM, PRIMITIVE, FUNCTIONAL;
		
		private static final String
			FUNC_REGEX = ".+\\$\\$Lambda\\$\\d+\\/\\d+";
		
		public static ClassType typeOf(Class<?> clazz)
		{
			if(clazz.isPrimitive())
			{
				return PRIMITIVE;
			}
		    else if(java.lang.reflect.Modifier.isInterface(clazz.getModifiers()))
			{
				return INTERFACE;
			}
			else if(Enum.class.isAssignableFrom(clazz))
			{
				return ENUM;
			}
			else if(Annotation.class.isAssignableFrom(clazz))
			{
				return ANNOTATION;
			}
			else if(clazz.getName().matches(FUNC_REGEX))
			{
				return FUNCTIONAL;
			}
			return CLASS;
		}
	}
	public static enum Visibility
	{
		PUBLIC, PROTECTED, PACKAGE, PRIVATE;
		
		public static Visibility getFor(int modifiers)
		{
			if(java.lang.reflect.Modifier.isPublic(modifiers))
			{
				return Visibility.PUBLIC;
			}
			if(java.lang.reflect.Modifier.isPrivate(modifiers))
			{
				return Visibility.PRIVATE;
			}
			if(java.lang.reflect.Modifier.isProtected(modifiers))
			{
				return Visibility.PROTECTED;
			}
			return Visibility.PACKAGE;
		}
	}
	public static enum Modifier
	{
		ABSTRACT
		{
			@Override
			public boolean isFor(int modifiers)
			{
				return java.lang.reflect.Modifier.isAbstract(modifiers);
			}
		}, 
		STATIC 
		{
			@Override
			public boolean isFor(int modifiers)
			{
				return java.lang.reflect.Modifier.isStatic(modifiers);
			}
		}, 
		FINAL 
		{
			@Override
			public boolean isFor(int modifiers)
			{
				return java.lang.reflect.Modifier.isFinal(modifiers);
			}
		}, 
		SYNCHRONIZED
		{
			@Override
			public boolean isFor(int modifiers)
			{
				return java.lang.reflect.Modifier.isSynchronized(modifiers);
			}
			
		}, 
		VOLATILE
		{
			@Override
			public boolean isFor(int modifiers)
			{
				return java.lang.reflect.Modifier.isVolatile(modifiers);
			}
			
		}, 
		TRANSIENT
		{
			@Override
			public boolean isFor(int modifiers)
			{
				return java.lang.reflect.Modifier.isNative(modifiers);
			}
		},	
		NATIVE 
		{
			@Override
			public boolean isFor(int modifiers)
			{
				return java.lang.reflect.Modifier.isNative(modifiers);
			}
		}, 
		STRICT 
		{
			@Override
			public boolean isFor(int modifiers)
			{
				return java.lang.reflect.Modifier.isStrict(modifiers);
			}
		};
		
		public abstract boolean isFor(int modifiers);
		
		public static Array<Modifier> modiferFor(int modifiers)
		{
			return new Array<>(values()).getWhere(modifier -> modifier.isFor(modifiers));
		}
	}
//	<< REFLECTION <<
	
	public static enum UTC
	{
		NEG_1200(-12, "Y"),
		NEG_1100(-11, "X"),
		NEG_1000(-10, "W"),
		NEG_0930(-9.5, "V†"),
		NEG_0900(-9, "V"),
		NEG_0800(-8, "U"),
		NEG_0700(-7, "T"),
		NEG_0600(-6, "S"),
		NEG_0500(-5, "R"),
		NEG_0430(-4.5, "Q†"),
		NEG_0400(-4, "Q"),
		NEG_0330(-3.5, "P†"),
		NEG_0300(-3, "P"),
		NEG_0200(-2, "0"),
		NEG_0100(-1, "N"),
		NONE_0000(0, "Z"),
		POS_0100(+1, "A"),
		POS_0200(+2, "B"),
		POS_0300(+3, "C"),
		POS_0330(+3.5, "C†"),
		POS_0400(+4, "D"),
		POS_0430(+4.5, "D†"),
		POS_0500(+5, "E"),
		POS_0530(+5.5, "E†"),
		POS_0545(+5.75, "E*"),
		POS_0600(+6, "F"),
		POS_0630(+6.5, "F†"),
		POS_0700(+7, "G"),
		POS_0800(+8, "H"),
		POS_0830(+8.5, "H†"),
		POS_0845(+8.75, "H*"),
		POS_0900(+9, "I"),
		POS_0930(+9.5, "I†"),
		POS_1000(+10, "K"),
		POS_1030(+10.5, "K†"),
		POS_1100(+11, "L"),
		POS_1200(+12, "M"),
		POS_1245(+12.75, "M*"),
		POS_1300(+13, "M†"),
		POS_1400(+14, "M†"),
		;
		
		private final double
			offset;
		
		private final String
			abbr;
		
		private UTC(double offset, String abbr)
		{
			this.offset = offset;
			this.abbr = abbr;
		}
		public double getOffset()
		{
			return this.offset;
		}
		public String getAbbreviation()
		{
			return this.abbr;
		}
	}
	public static enum Language
	{
		PASHTO,
		DARI,
		ALBANIAN,
		
		FRENCH,
		GERMAN,
		ITALIAN,
		JAPANESE,
		KOREAN,
		CHINESE_SIMPLIFIED,
		CHINESE_TRADITIONAL,
		ENGLISH;
		;
	}

	public static enum Country
	{
		AFGHANISTAN("AF", "AFG", 4, "+93"/*, Lang.PASHTO, Lang.DARI, Currency.AFGHANI, "af", Timezone.UTC_0430, Timezone.UTC_0430*/),
		ALBANIA("AL", "ALB", 8, "355"/*, Lang.ALBANIAN, Currency.LEK, "al", Timezone.CET, Timezone.CEST*/),
		ALGERIA("DZ", "DZA", 12, "213"),
		AMERICAN_SAMOA("AS", "ASM", 16, "1-684"),
		ANDORRA("AD", "AND", 20, "376"),
		ANGOLA("AO", "AGO", 24, "244"),
		ANGUILLA("AI", "AIA", 660, "1-264"),
		ANTARCTICA("AQ", "ATA", 10, "672"),
		ANTIGUA_AND_BARBUDA("AG", "ATG", 28, "1-268"),
		ARGENTINA("AR", "ARG", 32, "54"),
		ARMENIA("AM", "ARM", 51, "374"),
		ARUBA("AW", "ABW", 533, "297"),
		AUSTRALIA("AU", "AUS", 36, "61"),
		AUSTRIA("AT", "AUT", 40, "43"),
		AZERBAIJAN("AZ", "AZE", 31, "994"),
		BAHAMAS("BS", "BHS", 44, "1-242"),
		BAHRAIN("BH", "BHR", 48, "973"),
		BANGLADESH("BD", "BGD", 50, "880"),
		BARBADOS("BB", "BRB", 52, "1-246"),
		BELARUS("BY", "BLR", 112, "375"),
		BELGIUM("BE", "BEL", 56, "32"),
		BELIZE("BZ", "BLZ", 84, "501"),
		BENIN("BJ", "BEN", 204, "229"),
		BERMUDA("BM", "BMU", 60, "1-441"),
		BHUTAN("BT", "BTN", 64, "975"),
		BOLIVIA("BO", "BOL", 68, "591"),
		BONAIRE("BQ", "BES", 535, "599"),
		BOSNIA_AND_HERZEGOVINA("BA", "BIH", 70, "387"),
		BOTSWANA("BW", "BWA", 72, "267"),
		BOUVET_ISLAND("BV", "BVT", 74, "47"),
		BRAZIL("BR", "BRA", 76, "55"),
		BRITISH_INDIAN_OCEAN_TERRITORY("IO", "IOT", 86, "246"),
		BRUNEI_DARUSSALAM("BN", "BRN", 96, "673"),
		BULGARIA("BG", "BGR", 100, "359"),
		BURKINA_FASO("BF", "BFA", 854, "226"),
		BURUNDI("BI", "BDI", 108, "257"),
		CAMBODIA("KH", "KHM", 116, "855"),
		CAMEROON("CM", "CMR", 120, "237"),
		CANADA("CA", "CAN", 124, "1"),
		CAPE_VERDE("CV", "CPV", 132, "238"),
		CAYMAN_ISLANDS("KY", "CYM", 136, "1-345"),
		CENTRAL_AFRICAN_REPUBLIC("CF", "CAF", 140, "236"),
		CHAD("TD", "TCD", 148, "235"),
		CHILE("CL", "CHL", 152, "56"),
		CHINA("CN", "CHN", 156, "86"),
		CHRISTMAS_ISLAND("CX", "CXR", 162, "61"),
		COCOS_KEELING_ISLANDS("CC", "CCK", 166, "61"),
		COLOMBIA("CO", "COL", 170, "57"),
		COMOROS("KM", "COM", 174, "269"),
		CONGO("CG", "COG", 178, "242"),
		DEMOCRATIC_REPUBLIC_OF_THE_CONGO("CD", "COD", 180, "243"),
		COOK_ISLANDS("CK", "COK", 184, "682"),
		COSTA_RICA("CR", "CRI", 188, "506"),
		CROATIA("HR", "HRV", 191, "385"),
		CUBA("CU", "CUB", 192, "53"),
		CURACAO("CW", "CUW", 531, "599"),
		CYPRUS("CY", "CYP", 196, "357"),
		CZECH_REPUBLIC("CZ", "CZE", 203, "420"),
		COTE_DIVOIRE("CI", "CIV", 384, "225"),
		DENMARK("DK", "DNK", 208, "45"),
		DJIBOUTI("DJ", "DJI", 262, "253"),
		DOMINICA("DM", "DMA", 212, "1-767"),
		DOMINICAN_REPUBLIC("DO", "DOM", 214, "1-809,1-829,1-849"),
		ECUADOR("EC", "ECU", 218, "593"),
		EGYPT("EG", "EGY", 818, "20"),
		EL_SALVADOR("SV", "SLV", 222, "503"),
		EQUATORIAL_GUINEA("GQ", "GNQ", 226, "240"),
		ERITREA("ER", "ERI", 232, "291"),
		ESTONIA("EE", "EST", 233, "372"),
		ETHIOPIA("ET", "ETH", 231, "251"),
		FALKLAND_ISLANDS_MALVINAS("FK", "FLK", 238, "500"),
		FAROE_ISLANDS("FO", "FRO", 234, "298"),
		FIJI("FJ", "FJI", 242, "679"),
		FINLAND("FI", "FIN", 246, "358"),
		FRANCE("FR", "FRA", 250, "33"),
		FRENCH_GUIANA("GF", "GUF", 254, "594"),
		FRENCH_POLYNESIA("PF", "PYF", 258, "689"),
		FRENCH_SOUTHERN_TERRITORIES("TF", "ATF", 260, "262"),
		GABON("GA", "GAB", 266, "241"),
		GAMBIA("GM", "GMB", 270, "220"),
		GEORGIA("GE", "GEO", 268, "995"),
		GERMANY("DE", "DEU", 276, "49"),
		GHANA("GH", "GHA", 288, "233"),
		GIBRALTAR("GI", "GIB", 292, "350"),
		GREECE("GR", "GRC", 300, "30"),
		GREENLAND("GL", "GRL", 304, "299"),
		GRENADA("GD", "GRD", 308, "1-473"),
		GUADELOUPE("GP", "GLP", 312, "590"),
		GUAM("GU", "GUM", 316, "1-671"),
		GUATEMALA("GT", "GTM", 320, "502"),
		GUERNSEY("GG", "GGY", 831, "44"),
		GUINEA("GN", "GIN", 324, "224"),
		GUINEA_BISSAU("GW", "GNB", 624, "245"),
		GUYANA("GY", "GUY", 328, "592"),
		HAITI("HT", "HTI", 332, "509"),
		HEARD_ISLAND_AND_MCDONALD_ISLANDS("HM", "HMD", 334, "672"),
		HOLY_SEE("VA", "VAT", 336, "379"),
		HONDURAS("HN", "HND", 340, "504"),
		HONG_KONG("HK", "HKG", 344, "852"),
		HUNGARY("HU", "HUN", 348, "36"),
		ICELAND("IS", "ISL", 352, "354"),
		INDIA("IN", "IND", 356, "91"),
		INDONESIA("ID", "IDN", 360, "62"),
		IRAN("IR", "IRN", 364, "98"),
		IRAQ("IQ", "IRQ", 368, "964"),
		IRELAND("IE", "IRL", 372, "353"),
		ISLE_OF_MAN("IM", "IMN", 833, "44"),
		ISRAEL("IL", "ISR", 376, "972"),
		ITALY("IT", "ITA", 380, "39"),
		JAMAICA("JM", "JAM", 388, "1-876"),
		JAPAN("JP", "JPN", 392, "81"),
		JERSEY("JE", "JEY", 832, "44"),
		JORDAN("JO", "JOR", 400, "962"),
		KAZAKHSTAN("KZ", "KAZ", 398, "7"),
		KENYA("KE", "KEN", 404, "254"),
		KIRIBATI("KI", "KIR", 296, "686"),
		NORTH_KOREA("KP", "PRK", 408, "850"),
		SOUTH_KOREA("KR", "KOR", 410, "82"),
		KUWAIT("KW", "KWT", 414, "965"),
		KYRGYZSTAN("KG", "KGZ", 417, "996"),
		LAO("LA", "LAO", 418, "856"),
		LATVIA("LV", "LVA", 428, "371"),
		LEBANON("LB", "LBN", 422, "961"),
		LESOTHO("LS", "LSO", 426, "266"),
		LIBERIA("LR", "LBR", 430, "231"),
		LIBYA("LY", "LBY", 434, "218"),
		LIECHTENSTEIN("LI", "LIE", 438, "423"),
		LITHUANIA("LT", "LTU", 440, "370"),
		LUXEMBOURG("LU", "LUX", 442, "352"),
		MACAO("MO", "MAC", 446, "853"),
		MACEDONIA("MK", "MKD", 807, "389"),
		MADAGASCAR("MG", "MDG", 450, "261"),
		MALAWI("MW", "MWI", 454, "265"),
		MALAYSIA("MY", "MYS", 458, "60"),
		MALDIVES("MV", "MDV", 462, "960"),
		MALI("ML", "MLI", 466, "223"),
		MALTA("MT", "MLT", 470, "356"),
		MARSHALL_ISLANDS("MH", "MHL", 584, "692"),
		MARTINIQUE("MQ", "MTQ", 474, "596"),
		MAURITANIA("MR", "MRT", 478, "222"),
		MAURITIUS("MU", "MUS", 480, "230"),
		MAYOTTE("YT", "MYT", 175, "262"),
		MEXICO("MX", "MEX", 484, "52"),
		MICRONESIA("FM", "FSM", 583, "691"),
		MOLDOVA("MD", "MDA", 498, "373"),
		MONACO("MC", "MCO", 492, "377"),
		MONGOLIA("MN", "MNG", 496, "976"),
		MONTENEGRO("ME", "MNE", 499, "382"),
		MONTSERRAT("MS", "MSR", 500, "1-664"),
		MOROCCO("MA", "MAR", 504, "212"),
		MOZAMBIQUE("MZ", "MOZ", 508, "258"),
		MYANMAR("MM", "MMR", 104, "95"),
		NAMIBIA("NA", "NAM", 516, "264"),
		NAURU("NR", "NRU", 520, "674"),
		NEPAL("NP", "NPL", 524, "977"),
		NETHERLANDS("NL", "NLD", 528, "31"),
		NEW_CALEDONIA("NC", "NCL", 540, "687"),
		NEW_ZEALAND("NZ", "NZL", 554, "64"),
		NICARAGUA("NI", "NIC", 558, "505"),
		NIGER("NE", "NER", 562, "227"),
		NIGERIA("NG", "NGA", 566, "234"),
		NIUE("NU", "NIU", 570, "683"),
		NORFOLK_ISLAND("NF", "NFK", 574, "672"),
		NORTHERN_MARIANA_ISLANDS("MP", "MNP", 580, "1-670"),
		NORWAY("NO", "NOR", 578, "47"),
		OMAN("OM", "OMN", 512, "968"),
		PAKISTAN("PK", "PAK", 586, "92"),
		PALAU("PW", "PLW", 585, "680"),
		PALESTINE("PS", "PSE", 275, "970"),
		PANAMA("PA", "PAN", 591, "507"),
		PAPUA_NEW_GUINEA("PG", "PNG", 598, "675"),
		PARAGUAY("PY", "PRY", 600, "595"),
		PERU("PE", "PER", 604, "51"),
		PHILIPPINES("PH", "PHL", 608, "63"),
		PITCAIRN("PN", "PCN", 612, "870"),
		POLAND("PL", "POL", 616, "48"),
		PORTUGAL("PT", "PRT", 620, "351"),
		PUERTO_RICO("PR", "PRI", 630, "1"),
		QATAR("QA", "QAT", 634, "974"),
		ROMANIA("RO", "ROU", 642, "40"),
		RUSSIAN_FEDERATION("RU", "RUS", 643, "7"),
		RWANDA("RW", "RWA", 646, "250"),
		REUNION("RE", "REU", 638, "262"),
		SAINT_BARTHALEMY("BL", "BLM", 652, "590"),
		SAINT_HELENA("SH", "SHN", 654, "290"),
		SAINT_KITTS_AND_NEVIS("KN", "KNA", 659, "1-869"),
		SAINT_LUCIA("LC", "LCA", 662, "1-758"),
		SAINT_MARTIN("MF", "MAF", 663, "590"),
		SAINT_PIERRE_AND_MIQUELON("PM", "SPM", 666, "508"),
		SAINT_VINCENT_AND_THE_GRENADINES("VC", "VCT", 670, "1-784"),
		SAMOA("WS", "WSM", 882, "685"),
		SAN_MARINO("SM", "SMR", 674, "378"),
		SAO_TOME_AND_PRINCIPE("ST", "STP", 678, "239"),
		SAUDI_ARABIA("SA", "SAU", 682, "966"),
		SENEGAL("SN", "SEN", 686, "221"),
		SERBIA("RS", "SRB", 688, "381"),
		SEYCHELLES("SC", "SYC", 690, "248"),
		SIERRA_LEONE("SL", "SLE", 694, "232"),
		SINGAPORE("SG", "SGP", 702, "65"),
		SINT_MAARTEN("SX", "SXM", 534, "1-721"),
		SLOVAKIA("SK", "SVK", 703, "421"),
		SLOVENIA("SI", "SVN", 705, "386"),
		SOLOMON_ISLANDS("SB", "SLB", 90, "677"),
		SOMALIA("SO", "SOM", 706, "252"),
		SOUTH_AFRICA("ZA", "ZAF", 710, "27"),
		SOUTH_GEORGIA_AND_THE_SOUTH_SANDWICH_ISLANDS("GS", "SGS", 239, "500"),
		SOUTH_SUDAN("SS", "SSD", 728, "211"),
		SPAIN("ES", "ESP", 724, "34"),
		SRI_LANKA("LK", "LKA", 144, "94"),
		SUDAN("SD", "SDN", 729, "249"),
		SURINAME("SR", "SUR", 740, "597"),
		SVALBARD_AND_JAN_MAYEN("SJ", "SJM", 744, "47"),
		SWAZILAND("SZ", "SWZ", 748, "268"),
		SWEDEN("SE", "SWE", 752, "46"),
		SWITZERLAND("CH", "CHE", 756, "41"),
		SYRIAN_ARAB_REPUBLIC("SY", "SYR", 760, "963"),
		TAIWAN("TW", "TWN", 158, "886"),
		TAJIKISTAN("TJ", "TJK", 762, "992"),
		TANZANIA("TZ", "TZA", 834, "255"),
		THAILAND("TH", "THA", 764, "66"),
		TIMOR_LESTE("TL", "TLS", 626, "670"),
		TOGO("TG", "TGO", 768, "228"),
		TOKELAU("TK", "TKL", 772, "690"),
		TONGA("TO", "TON", 776, "676"),
		TRINIDAD_AND_TOBAGO("TT", "TTO", 780, "1-868"),
		TUNISIA("TN", "TUN", 788, "216"),
		TURKEY("TR", "TUR", 792, "90"),
		TURKMENISTAN("TM", "TKM", 795, "993"),
		TURKS_AND_CAICOS_ISLANDS("TC", "TCA", 796, "1-649"),
		TUVALU("TV", "TUV", 798, "688"),
		UGANDA("UG", "UGA", 800, "256"),
		UKRAINE("UA", "UKR", 804, "380"),
		UNITED_ARAB_EMIRATES("AE", "ARE", 784, "971"),
		UNITED_KINGDOM("GB", "GBR", 826, "44"),
		UNITED_STATES("US", "USA", 840, "1"),
		UNITED_STATES_MINOR_OUTLYING_ISLANDS("UM", "UMI", 581, "Â"),
		URUGUAY("UY", "URY", 858, "598"),
		UZBEKISTAN("UZ", "UZB", 860, "998"),
		VANUATU("VU", "VUT", 548, "678"),
		VENEZUELA("VE", "VEN", 862, "58"),
		VIET_NAM("VN", "VNM", 704, "84"),
		BRITISH_VIRGIN_ISLANDS("VG", "VGB", 92, "1-284"),
		US_VIRGIN_ISLANDS("VI", "VIR", 850, "1-340"),
		WALLIS_AND_FUTUNA("WF", "WLF", 876, "681"),
		WESTERN_SAHARA("EH", "ESH", 732, "212"),
		YEMEN("YE", "YEM", 887, "967"),
		ZAMBIA("ZM", "ZMB", 894, "260"),
		ZIMBABWE("ZW", "ZWE", 716, "263"),
		ALAND_ISLANDS("AX", "ALA", 248, "358"),
		;
		
		private Country(String isoCode, String uniCode, int uniNum, String dialCode/*, Lang lang, Currency currency, String tld, Timezone standard, Timezone summer*/)
		{
			
		}
	}
}	
