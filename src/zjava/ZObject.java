package zjava;

import zjava.auto.ZAutoString;
import zjava.errors.ArgumentException;
import zjava.lang.functionals.Provider;
import zjava.lang.functionals.Runner;
import zjava.lang.reflect.ZClass;
import zjava.lang.types.collections.Array;
import zjava.lang.types.collections.ZList;
import zjava.lang.utils.ZThread;

/**
 * Superclass for all Objects that are implemented in the ZJava-Library.
 * <br>Amongst other things, it implements an easy way to convert classes to string, and provides an automatic equals-method.
 */
public abstract class ZObject implements ZAutoString
{	
//	>> PROTECTED STATIC >>
	protected static <P> P notNull(P value)
	{
		if(null == value) throw new ArgumentException("argument cannot be null");
		return value;
	}
	protected static void write(String content, Object... objects)
	{
		Console.write(content, objects);
	}
	protected static void echo(Object... objects)
	{
		Console.echo(objects);
	}
	protected static void dump(Object... objects)
	{
		Console.dump(objects);
	}
	protected static <P> ZClass<P> tf(Class<P> clazz)
	{
		return ZClass.get(clazz);
	}
	protected static void thread(Runner sequence)
	{
		ZThread.execute(sequence);
	}
	protected static <P> P create(Provider<P> provider)
	{
		return provider.provide();
	}
	@SafeVarargs
	protected static <P> Array<P> array(P... objects) 
	{
		return new Array<>(objects);
	}
	@SafeVarargs
	protected static <P> ZList<P> list(P... objects)
	{
		return new ZList<P>(objects);
	}
//	<< PROTECTED STATIC <<
	
	public ZObject()
	{
		this.checkConstructor();
	}
	private void checkConstructor()
	{
		
	}
	
	@Override
	public String toString()
	{
		return this.getString();
	}
}
